# 小智报表展现端 API
## 输入
- option选项配置
```shell
const option = {
  mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
  view: {
    width: () => document.documentElement.clientWidth,
    height: () => document.documentElement.clientHeight
  },   // 设置报表的宽高
  renderArrow: true,  // 是否显式 扩展方向图标
  showFreeze: true,  // 是否显式冻结线
  showGrid: true,   // 是否显式网格线
  showPreviewToolbar: true,
  showBottombar: true,
  showSelect: true,
};
```
- config 报表模板json
- data   报表模板需要的数据集
- 实现 onUpdateData回调方法，以外部数据集的方式提供数据
```shell
options.onUpdateData = onUpdateData
// 获取数据表数据
// datasets => [{name:"数据表名称",id:"数据表id"...}]  参与获取数据的数据集
// params => [{name:"参数名称",value:"参数值"}]  value的类型是字符串或者数组
// 该方法的返回值是一个Promise
const onUpdateData = (datasets,params)=>{
  return new Promise((resolve, reject)=>{
    // resolve的数据格式
    let results = [
      {
        name:"数据表名称",
        response_data:[] 
        // 支持两种数据格式  1.{columns:[a,b],data:[[1,2]]} 2. [{a:1,b:2}]
      }
    ]
    resolve(results)
  })
}
```
- 实例构建
```shell
  spreadSheet = XZReport("#reportPreview", option);
  spreadSheet.update(config, data);
  //触发 onUpdateData回调,到后台获取数据表的记录信息
```
# 样例参考 
## /src/views/report/XzReport1.vue