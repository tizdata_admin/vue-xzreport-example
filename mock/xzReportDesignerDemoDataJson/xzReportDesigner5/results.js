//第三方外部数据集类型的具体的数据集

let results = [
    {
        name:"商品销售",
        id:"商品销售id",
        columns:[
                    {name: "商品名称", data_type: 3 },
                    { name: "区域名称",data_type: 3 },
                    {name: "销售额",data_type: 2},
                ],
        params:[
                    {name: "商品名称", data_type: 3},
                    {name: "区域名称",data_type: 3},
                ],
        response_data:[],
    }]

export {results}