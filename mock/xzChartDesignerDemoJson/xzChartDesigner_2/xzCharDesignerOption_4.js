// 数据来源部分
// 数据来源目前支持选择Json数据、http请求数据、API服务数据三种数据集类型。可以支持接收外部Json数据。可在小智BI初始化方法时设置option字段来选择数据集的可选择方式。
// 以Json数据为实例来演示。
let option = { //参数配置说明 根据您的需求填写option
    uploadImage: { //上传图片相关信息 如果不传该字段时，设计器用到的所有上传图片配置项均不显示。
        server: "url",//上传图片地址
        method: "post",//上传图片请求方式
        headers: { //上传图片请求头
            "Authorization": "Token XXXXXXXX"
        },
        fieldName: "file",
        resultField: "file"
    },
    pageData: { //画布初始化时的宽高
        width: '1920px',
        height: '1080px'
    },
    dataSet: [ //数据集按钮的方式
        { //Json数据
            id: 1,
            value: "新建Json数据集"
        },
        { //http请求数据
            id: 2,
            value: "新建Http接口数据集"
        },
        { //API服务数据
            id: 3,
            value: "API服务数据集"
        },
        {
            id: 4,
            value: "外部数据集"  //  新建一个新的报表实例，如果希望可以预先动态从后台提供几个数据集供其使用，这个配置必须有；
            // 然后只要动态的从后台拿到n个数据集，全部用 results格式装配，onUpdateDatasetList方法来触发
        }
    ]

}
export {option}