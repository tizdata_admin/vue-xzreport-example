const reportTemplate_mainSub_1 = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}, {"Row": 3}, {"Row": 4}, {"Row": 5}, {"Row": 6}, {"Row": 7}, {"Row": 8}, {"Row": 9}, {"Row": 10}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "type": 6, "value": "客户编号", "column": 0, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "公司名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地址", "column": 2, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "城市",
            "column": 3,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地区", "column": 4, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "电话",
            "column": 5,
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CustomerID",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CustomerID", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CompanyName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CompanyName", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Address",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Address", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.City",
            "column": 3,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.City", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Region",
            "column": 4,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Region", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Phone",
            "column": 5,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Phone", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "编号", "column": 0, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "单价", "column": 2, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "库存数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "订购数量", "column": 4, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "订购级别",
            "column": 5,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "使用中", "column": 6, "styleIndex": 0}, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductID",
            "column": 0,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitPrice",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitPrice",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsInStock",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsInStock",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsOnOrder",
            "column": 4,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsOnOrder",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ReorderLevel",
            "column": 5,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ReorderLevel",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.Discontinued",
            "column": 6,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.Discontinued",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 5,
            "type": 6,
            "value": "最有价值客户",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 5, "column": 1, "styleIndex": 7}, {"row": 5, "column": 2, "styleIndex": 7}, {
            "row": 5,
            "column": 3,
            "styleIndex": 7
        }, {"row": 6, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "客户编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 6, "type": 6, "value": "公司名称", "column": 2, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "销售总金额",
            "column": 3,
            "styleIndex": 0
        }, {"row": 7, "type": 5, "value": "=ROW()-17", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CustomerID",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CustomerID",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CompanyName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CompanyName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.SumAmt",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.SumAmt",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 8,
            "type": 6,
            "value": "最畅销产品",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 8, "column": 1, "styleIndex": 7}, {"row": 8, "column": 2, "styleIndex": 7}, {
            "row": 8,
            "column": 3,
            "styleIndex": 7
        }, {"row": 9, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "产品编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 9, "type": 6, "value": "产品名称", "column": 2, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "销售数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 10, "type": 5, "value": "=ROW()-29", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductID",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.SumQty",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.SumQty",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Column": 3}, {"Column": 4}, {"Column": 5}, {"Column": 6}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 7,
            "sci": 0,
            "sri": 6,
            "label": "A7-D8",
            "filter": null,
            "bgColor": "rgba(255, 212, 80,0.5)",
            "borderColor": "rgba(255, 212, 80)"
        }, {
            "eci": 3,
            "eri": 10,
            "sci": 0,
            "sri": 9,
            "label": "A10-D11",
            "filter": null,
            "bgColor": "rgba(115, 187, 255,0.5)",
            "borderColor": "rgba(115, 187, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }, {"align": "center", "valign": "middle"}, {
            "font": {"size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"bold": true, "size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle",
            "bgcolor": "#BDD7EE"
        }, {"font": {"bold": true, "size": 15}, "bgcolor": "#BDD7EE"}],
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}]
export const sheetConfig_mainSub_1=[{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}, {"Row": 3}, {"Row": 4}, {"Row": 5}, {"Row": 6}, {"Row": 7}, {"Row": 8}, {"Row": 9}, {"Row": 10}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "type": 6, "value": "客户编号", "column": 0, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "公司名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地址", "column": 2, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "城市",
            "column": 3,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地区", "column": 4, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "电话",
            "column": 5,
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CustomerID",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CustomerID", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CompanyName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CompanyName", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Address",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Address", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.City",
            "column": 3,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.City", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Region",
            "column": 4,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Region", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Phone",
            "column": 5,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Phone", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "编号", "column": 0, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "单价", "column": 2, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "库存数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "订购数量", "column": 4, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "订购级别",
            "column": 5,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "使用中", "column": 6, "styleIndex": 0}, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductID",
            "column": 0,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitPrice",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitPrice",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsInStock",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsInStock",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsOnOrder",
            "column": 4,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsOnOrder",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ReorderLevel",
            "column": 5,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ReorderLevel",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.Discontinued",
            "column": 6,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.Discontinued",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 5,
            "type": 6,
            "value": "最有价值客户",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 5, "column": 1, "styleIndex": 7}, {"row": 5, "column": 2, "styleIndex": 7}, {
            "row": 5,
            "column": 3,
            "styleIndex": 7
        }, {"row": 6, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "客户编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 6, "type": 6, "value": "公司名称", "column": 2, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "销售总金额",
            "column": 3,
            "styleIndex": 0
        }, {"row": 7, "type": 5, "value": "=ROW()-17", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CustomerID",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CustomerID",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CompanyName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CompanyName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.SumAmt",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.SumAmt",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 8,
            "type": 6,
            "value": "最畅销产品",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 8, "column": 1, "styleIndex": 7}, {"row": 8, "column": 2, "styleIndex": 7}, {
            "row": 8,
            "column": 3,
            "styleIndex": 7
        }, {"row": 9, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "产品编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 9, "type": 6, "value": "产品名称", "column": 2, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "销售数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 10, "type": 5, "value": "=ROW()-29", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductID",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.SumQty",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.SumQty",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Column": 3}, {"Column": 4}, {"Column": 5}, {"Column": 6}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 7,
            "sci": 0,
            "sri": 6,
            "label": "A7-D8",
            "filter": null,
            "bgColor": "rgba(255, 212, 80,0.5)",
            "borderColor": "rgba(255, 212, 80)"
        }, {
            "eci": 3,
            "eri": 10,
            "sci": 0,
            "sri": 9,
            "label": "A10-D11",
            "filter": null,
            "bgColor": "rgba(115, 187, 255,0.5)",
            "borderColor": "rgba(115, 187, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }, {"align": "center", "valign": "middle"}, {
            "font": {"size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"bold": true, "size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle",
            "bgcolor": "#BDD7EE"
        }, {"font": {"bold": true, "size": 15}, "bgcolor": "#BDD7EE"}],
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}];
export const response_data_Customer_mainSub_1 = [{"CustomerID":"ALFKI","CompanyName":"三川实业有限公司","ContactName":"刘小姐","ContactTitle":"销售代表","Address":"大崇明路 50 号","City":"天津","Region":"华北","PostalCode":"343567","Country":"中国","Phone":"(030) 30074321","Fax":"(030) 30765452"},
    {"CustomerID":"ANATR","CompanyName":"东南实业","ContactName":"王先生","ContactTitle":"物主","Address":"承德西路 80 号","City":"天津","Region":"华北","PostalCode":"234575","Country":"中国","Phone":"(030) 35554729","Fax":"(030) 35553744"},
    {"CustomerID":"ANTON","CompanyName":"坦森行贸易","ContactName":"王炫皓","ContactTitle":"物主","Address":"黄台北路 780 号","City":"石家庄","Region":"华北","PostalCode":"985060","Country":"中国","Phone":"(0321) 5553932","Fax":""},
    {"CustomerID":"AROUT","CompanyName":"国顶有限公司","ContactName":"方先生","ContactTitle":"销售代表","Address":"天府东街 30 号","City":"深圳","Region":"华南","PostalCode":"890879","Country":"中国","Phone":"(0571) 45557788","Fax":"(0571) 45556750"},
    {"CustomerID":"BERGS","CompanyName":"通恒机械","ContactName":"黄小姐","ContactTitle":"采购员","Address":"东园西甲 30 号","City":"南京","Region":"华东","PostalCode":"798089","Country":"中国","Phone":"(0921) 9123465","Fax":"(0921) 55123467"},
    {"CustomerID":"BLAUS","CompanyName":"森通","ContactName":"王先生","ContactTitle":"销售代表","Address":"常保阁东 80 号","City":"天津","Region":"华北","PostalCode":"787045","Country":"中国","Phone":"(030) 30058460","Fax":"(030)  33008924"}
]
export const response_data_Product_mainSub_1=
[
    {"ProductID":56,"ProductName":"白米","SupplierID":26,"CategoryID":5,"QuantityPerUnit":"每袋3公斤","UnitPrice":38,"UnitsInStock":21,"UnitsOnOrder":10,"ReorderLevel":30,"Discontinued":false},
    {"ProductID":32,"ProductName":"白奶酪","SupplierID":14,"CategoryID":4,"QuantityPerUnit":"每箱12瓶","UnitPrice":32,"UnitsInStock":9,"UnitsOnOrder":40,"ReorderLevel":25,"Discontinued":false},
    {"ProductID":16,"ProductName":"饼干","SupplierID":7,"CategoryID":3,"QuantityPerUnit":"每箱30盒","UnitPrice":17.45,"UnitsInStock":29,"UnitsOnOrder":0,"ReorderLevel":10,"Discontinued":false},
    {"ProductID":42,"ProductName":"糙米","SupplierID":20,"CategoryID":5,"QuantityPerUnit":"每袋3公斤","UnitPrice":14,"UnitsInStock":26,"UnitsOnOrder":0,"ReorderLevel":0,"Discontinued":true},
    {"ProductID":47,"ProductName":"蛋糕","SupplierID":22,"CategoryID":3,"QuantityPerUnit":"每箱24个","UnitPrice":9.5,"UnitsInStock":36,"UnitsOnOrder":0,"ReorderLevel":0,"Discontinued":false},
    {"ProductID":12,"ProductName":"德国奶酪","SupplierID":5,"CategoryID":4,"QuantityPerUnit":"每箱12瓶","UnitPrice":38,"UnitsInStock":86,"UnitsOnOrder":0,"ReorderLevel":0,"Discontinued":false}
]
export const response_data_Top10Customer_mainSub_1=
[
    {"CustomerID":"QUICK","CompanyName":"高上补习班","SumAmt":117483.39},
    {"CustomerID":"SAVEA","CompanyName":"大钰贸易","SumAmt":115673.39},
    {"CustomerID":"ERNSH","CompanyName":"正人资源","SumAmt":113236.68},
    {"CustomerID":"HUNGO","CompanyName":"师大贸易","SumAmt":57317.39},
    {"CustomerID":"RATTC","CompanyName":"学仁贸易","SumAmt":52245.9},
    {"CustomerID":"HANAR","CompanyName":"实翼","SumAmt":34101.15},
    {"CustomerID":"FOLKO","CompanyName":"五洲信托","SumAmt":32555.55},
    {"CustomerID":"MEREP","CompanyName":"华科","SumAmt":32203.9},
    {"CustomerID":"KOENE","CompanyName":"永业房屋","SumAmt":31745.75},
    {"CustomerID":"QUEEN","CompanyName":"留学服务中心","SumAmt":30226.1}
]
export const response_data_Top10Product_mainSub_1=
[
    {"ProductID":38,"ProductName":"绿茶","SumQty":149984.2},
    {"ProductID":29,"ProductName":"鸭肉","SumQty":87736.4},
    {"ProductID":59,"ProductName":"苏澳奶酪","SumQty":76296},
    {"ProductID":60,"ProductName":"花奶酪","SumQty":50286},
    {"ProductID":62,"ProductName":"山渣片","SumQty":49827.9},
    {"ProductID":56,"ProductName":"白米","SumQty":45159.2},
    {"ProductID":51,"ProductName":"猪肉干","SumQty":44742.6},
    {"ProductID":17,"ProductName":"猪肉","SumQty":35650.2},
    {"ProductID":18,"ProductName":"墨鱼","SumQty":31987.5},
    {"ProductID":28,"ProductName":"烤肉酱","SumQty":26865.6}
]
export const data_mainSub_1=[
    {
        name:"Customer",
        id:"Customer",
        type:4,
        response_data:response_data_Customer_mainSub_1
    },
    {
        name:"Product",
        id:"Product",
        type:4,
        response_data:response_data_Product_mainSub_1
    },
    {
        name:"Top10Customer",
        id:"Top10Customer",
        type:4,
        response_data:response_data_Top10Customer_mainSub_1
    },
    {
        name:"Top10Product",
        id:"Top10Product",
        type:4,
        response_data:response_data_Top10Product_mainSub_1
    },
]
export const onUpdateDataSetList_mainSub_1=[
    {

    }
]
export const  option_mainSub_1 = {
    mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
    view: {
        width: () => document.documentElement.clientWidth,
        height: () => document.documentElement.clientHeight
    },   // 设置报表的宽高
    renderArrow: false,  // 是否显式 扩展方向图标
    showFreeze: false,  // 是否显式冻结线
    showGrid: false   // 是否显式网格线
};

export     const reportPrintOptions_mainSub_1 = [
    {
        name: "组内排序",
        //print: sheetConfig
    }
]