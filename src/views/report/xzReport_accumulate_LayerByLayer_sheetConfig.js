export const reportTemplate_accumulate_layerByLayer = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "columnHead": [{"Width": 72, "Column": 0}, {"Width": 68, "Column": 1}, {"Width": 71, "Column": 2}, {
            "Width": 74,
            "Column": 3
        }, {"Width": 86, "Column": 4}],
        "rowHead": [{"Row": 0}, {"Row": 1}],
        "stylesConfig": [{"align": "center"}, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "bgcolor": "#01B0F1"
        }, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "bgcolor": "#93D051"
        }],
        "cellConfig": [{"column": 0, "row": 0, "value": "商品", "styleIndex": 1}, {
            "column": 1,
            "row": 0,
            "value": "年份",
            "styleIndex": 1
        }, {"column": 2, "row": 0, "value": "季度", "styleIndex": 1}, {
            "column": 3,
            "row": 0,
            "value": "销售额",
            "styleIndex": 1
        }, {"column": 4, "row": 0, "value": "逐层累计", "styleIndex": 1}, {
            "column": 0,
            "row": 1,
            "value": "sale.商品",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.商品", "data_type": "3", "merge": 1, "axisType": 1, "order": 0},
            "type": 2,
            "dataType": "3"
        }, {
            "column": 1,
            "row": 1,
            "value": "sale.年份",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.年份", "data_type": "2", "axisType": 1, "order": 0, "merge": 1},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 2,
            "row": 1,
            "value": "sale.季度",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.季度", "data_type": "2", "axisType": 1, "order": 0, "merge": 1},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 3,
            "row": 1,
            "value": "sale.销售额",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 3,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.销售额", "data_type": "2", "aggregate": 1, "axisType": 2, "order": 0},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 4,
            "row": 1,
            "value": "=IF(B2>1,D2 + E2[C2:-1],D2)",
            "styleIndex": 2,
            "warn": [{
                "name": "条件1",
                "act_on": 1,
                "cond_str": "((字段：sale.商品) 不为空 )",
                "filter": {
                    "logic": 1,
                    "params": [{
                        "logic": 1,
                        "params": [{
                            "column": "1:0",
                            "data_type": 3,
                            "logic": 1,
                            "type": 8,
                            "value": "",
                            "is_req": false
                        }]
                    }]
                },
                "style_str": "小数位：2位； ",
                "stylesConfig": {"format": {"decimal": 2}}
            }],
            "type": 5
        }],
        "groupConfig": [{
            "label": "A1-E2",
            "borderColor": "rgba(94, 255, 215)",
            "bgColor": "rgba(94, 255, 215,0.5)",
            "noagg": false,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "position": "A1:E2"
        }],
        "collapse": [],
        "toolbarButtonList": [{
            "showIcon": true,
            "showText": true,
            "text": "首页",
            "icon": "fa fa-step-backward",
            "color": "#2c3e50",
            "show": true,
            "type": 1
        }, {
            "showIcon": true,
            "showText": true,
            "text": "上一页",
            "icon": "xzreport-icon-arrow-left-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 2
        }, {
            "showIcon": true,
            "showText": true,
            "text": "当前页/总页数",
            "icon": "",
            "color": "#2c3e50",
            "show": true,
            "type": 3
        }, {
            "showIcon": true,
            "showText": true,
            "text": "下一页",
            "icon": "xzreport-icon-arrow-right-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 4
        }, {
            "showIcon": true,
            "showText": true,
            "text": "末页",
            "icon": "fa fa-step-forward",
            "color": "#2c3e50",
            "show": true,
            "type": 5
        }, {
            "showIcon": true,
            "showText": true,
            "text": "打印",
            "color": "#2c3e50",
            "icon": "fa fa-print",
            "show": true,
            "type": 6
        }, {
            "showIcon": true,
            "showText": true,
            "text": "导出",
            "icon": "fa fa-download",
            "color": "#2c3e50",
            "show": true,
            "type": 7
        }]
    }
}];

export const sheetConfig_accumulate_layerByLayer = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "columnHead": [{"Width": 72, "Column": 0}, {"Width": 68, "Column": 1}, {"Width": 71, "Column": 2}, {
            "Width": 74,
            "Column": 3
        }, {"Width": 86, "Column": 4}],
        "rowHead": [{"Row": 0}, {"Row": 1}],
        "stylesConfig": [{"align": "center"}, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "bgcolor": "#01B0F1"
        }, {
            "align": "center",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "bgcolor": "#93D051"
        }],
        "cellConfig": [{"column": 0, "row": 0, "value": "商品", "styleIndex": 1}, {
            "column": 1,
            "row": 0,
            "value": "年份",
            "styleIndex": 1
        }, {"column": 2, "row": 0, "value": "季度", "styleIndex": 1}, {
            "column": 3,
            "row": 0,
            "value": "销售额",
            "styleIndex": 1
        }, {"column": 4, "row": 0, "value": "逐层累计", "styleIndex": 1}, {
            "column": 0,
            "row": 1,
            "value": "sale.商品",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.商品", "data_type": "3", "merge": 1, "axisType": 1, "order": 0},
            "type": 2,
            "dataType": "3"
        }, {
            "column": 1,
            "row": 1,
            "value": "sale.年份",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.年份", "data_type": "2", "axisType": 1, "order": 0, "merge": 1},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 2,
            "row": 1,
            "value": "sale.季度",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 1,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.季度", "data_type": "2", "axisType": 1, "order": 0, "merge": 1},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 3,
            "row": 1,
            "value": "sale.销售额",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 3,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {"value": "sale.销售额", "data_type": "2", "aggregate": 1, "axisType": 2, "order": 0},
            "type": 2,
            "dataType": "2"
        }, {
            "column": 4,
            "row": 1,
            "value": "=IF(B2>1,D2 + E2[C2:-1],D2)",
            "styleIndex": 2,
            "warn": [{
                "name": "条件1",
                "act_on": 1,
                "cond_str": "((字段：sale.商品) 不为空 )",
                "filter": {
                    "logic": 1,
                    "params": [{
                        "logic": 1,
                        "params": [{
                            "column": "1:0",
                            "data_type": 3,
                            "logic": 1,
                            "type": 8,
                            "value": "",
                            "is_req": false
                        }]
                    }]
                },
                "style_str": "小数位：2位； ",
                "stylesConfig": {"format": {"decimal": 2}}
            }],
            "type": 5
        }],
        "groupConfig": [{
            "label": "A1-E2",
            "borderColor": "rgba(94, 255, 215)",
            "bgColor": "rgba(94, 255, 215,0.5)",
            "noagg": false,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "position": "A1:E2"
        }],
        "collapse": [],
        "toolbarButtonList": [{
            "showIcon": true,
            "showText": true,
            "text": "首页",
            "icon": "fa fa-step-backward",
            "color": "#2c3e50",
            "show": true,
            "type": 1
        }, {
            "showIcon": true,
            "showText": true,
            "text": "上一页",
            "icon": "xzreport-icon-arrow-left-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 2
        }, {
            "showIcon": true,
            "showText": true,
            "text": "当前页/总页数",
            "icon": "",
            "color": "#2c3e50",
            "show": true,
            "type": 3
        }, {
            "showIcon": true,
            "showText": true,
            "text": "下一页",
            "icon": "xzreport-icon-arrow-right-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 4
        }, {
            "showIcon": true,
            "showText": true,
            "text": "末页",
            "icon": "fa fa-step-forward",
            "color": "#2c3e50",
            "show": true,
            "type": 5
        }, {
            "showIcon": true,
            "showText": true,
            "text": "打印",
            "color": "#2c3e50",
            "icon": "fa fa-print",
            "show": true,
            "type": 6
        }, {
            "showIcon": true,
            "showText": true,
            "text": "导出",
            "icon": "fa fa-download",
            "color": "#2c3e50",
            "show": true,
            "type": 7
        }]
    }
}]

export const response_data_accumulate_layerByLayer =
    [{"商品": "A产品", "年份": 2020, "季度": 1, "销售额": 5890.56}, {
        "商品": "A产品",
        "年份": 2020,
        "季度": 2,
        "销售额": 4566.26
    }, {"商品": "A产品", "年份": 2020, "季度": 3, "销售额": 7861.56}, {
        "商品": "A产品",
        "年份": 2020,
        "季度": 4,
        "销售额": 5654.56
    }, {"商品": "A产品", "年份": 2021, "季度": 1, "销售额": 5820.96}, {
        "商品": "A产品",
        "年份": 2021,
        "季度": 2,
        "销售额": 5666.26
    }, {"商品": "A产品", "年份": 2021, "季度": 3, "销售额": 8661.56}, {
        "商品": "A产品",
        "年份": 2021,
        "季度": 4,
        "销售额": 5254.56
    }, {"商品": "B产品", "年份": 2020, "季度": 1, "销售额": 5890.56}, {
        "商品": "B产品",
        "年份": 2020,
        "季度": 2,
        "销售额": 4456.16
    }, {"商品": "B产品", "年份": 2020, "季度": 3, "销售额": 7561.56}, {
        "商品": "B产品",
        "年份": 2020,
        "季度": 4,
        "销售额": 3554.56
    }, {"商品": "B产品", "年份": 2021, "季度": 1, "销售额": 4620.96}, {
        "商品": "B产品",
        "年份": 2021,
        "季度": 2,
        "销售额": 5456.26
    }, {"商品": "B产品", "年份": 2021, "季度": 3, "销售额": 8891.56}, {
        "商品": "B产品",
        "年份": 2021,
        "季度": 4,
        "销售额": 5623
    }];

export const formula_accumulate_layerByLayer = "=IF(B2>1,D2 + E2[C2:-1],D2)";
export const data_accumulate_layerByLayer = [
    {
        "type": 4,
        "name": "sale",
        "response_data": response_data_accumulate_layerByLayer
    }
]
export const option_accumulate_layerByLayer = {
    mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
    view: {
        width: () => document.documentElement.clientWidth,
        height: () => document.documentElement.clientHeight
    },   // 设置报表的宽高
    renderArrow: false,  // 是否显式 扩展方向图标
    showFreeze: false,  // 是否显式冻结线
    showGrid: false   // 是否显式网格线
};

export const reportPrintOptions_accumulate_layerByLayer = [
    {
        name: "逐层累计报表",
        //print: sheetConfig
    }
]

