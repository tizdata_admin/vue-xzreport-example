

export const get_onupdatedatamaps =()=>{
    let response_data =
        [{"商品": "A产品", "季度": 1, "销售额": 5890.56}, {"商品": "A产品", "季度": 2, "销售额": 4566.26}, {
            "商品": "A产品",
            "季度": 3,
            "销售额": 7861.56
        }, {"商品": "A产品", "季度": 4, "销售额": 5654.56}, {"商品": "B产品", "季度": 1, "销售额": 5890.56}, {
            "商品": "B产品",
            "季度": 2,
            "销售额": 4456.16
        }, {"商品": "B产品", "季度": 3, "销售额": 7561.56}, {"商品": "B产品", "季度": 4, "销售额": 3554.56}, {
            "商品": "C产品",
            "季度": 1,
            "销售额": 4600.78
        }, {"商品": "C产品", "季度": 2, "销售额": 5246}, {"商品": "C产品", "季度": 3, "销售额": 7761.56}, {
            "商品": "C产品",
            "季度": 4,
            "销售额": 8164
        }, {"商品": "D产品", "季度": 1, "销售额": 4678.56}, {"商品": "D产品", "季度": 2, "销售额": 6659}, {
            "商品": "D产品",
            "季度": 3,
            "销售额": 7567.56
        }, {"商品": "D产品", "季度": 4, "销售额": 7498}]

    const OnUpdateDataSortInGroup = {
        "name": "组内排序",
        "id": "组内排序",
        "type": 4,
        response_data,
    }

    let onupdatedatamaps = new Map();
    onupdatedatamaps.set(OnUpdateDataSortInGroup.name, {
        name:OnUpdateDataSortInGroup.name,
        id:OnUpdateDataSortInGroup.id,
        type:OnUpdateDataSortInGroup.type,
        response_data:Array.from(response_data),
    });
    return onupdatedatamaps;
}


