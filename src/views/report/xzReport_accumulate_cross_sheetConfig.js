export const reportTemplate_acc_cross = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "value": "商品", "column": 0, "styleIndex": 1}, {
            "row": 0,
            "value": "季度",
            "column": 1,
            "styleIndex": 1
        }, {"row": 0, "value": "销售额", "column": 2, "styleIndex": 1}, {
            "row": 0,
            "value": "分组排名",
            "column": 3,
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.商品",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.商品", "axisType": 1, "data_type": "3"},
            "styleIndex": 4
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.季度",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.季度", "axisType": 1, "data_type": "2"},
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.销售额",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"order": 0, "value": "sale.销售额", "axisType": 2, "aggregate": 1, "data_type": "2"},
            "styleIndex": 5
        }, {"row": 1, "type": 5, "value": "=COUNT(C2[!0]{A2 = $A2 && C2 > $C2}) + 1", "column": 3, "styleIndex": 4}],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Width": 159, "Column": 3}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 1,
            "sci": 0,
            "sri": 0,
            "label": "A1-D2",
            "noagg": false,
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(255, 123, 88,0.5)",
            "borderColor": "rgba(255, 123, 88)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#93D051"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#01B0F1"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#BFBFBF"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#FFFFFF"
        }],
        "pageFreezeConfig": "A1",
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}];

export const sheetConfig_acc_cross = [{"name":"sheet1","SheetData":{"freeze":"A1","columnHead":[{"Width":127,"Column":0},{"Width":68,"Column":1},{"Column":2}],"rowHead":[{"Row":0,"height":42},{"Row":1}],"stylesConfig":[{"align":"center","valign":"middle"},{"border":{"bottom":["thin","#666"],"top":["thin","#666"],"left":["thin","#666"],"right":["thin","#666"]}},{"align":"center","valign":"middle","border":{"bottom":["thin","#666"],"top":["thin","#666"],"left":["thin","#666"],"right":["thin","#666"]}},{"border":{"bottom":["thin","#666"],"top":["thin","#666"],"left":["thin","#666"],"right":["thin","#666"]},"bgcolor":"#93D051"},{"border":{"bottom":["thin","#666"],"top":["thin","#666"],"left":["thin","#666"],"right":["thin","#666"]},"bgcolor":"#01B0F1"},{"border":{"bottom":["thin","#666"],"top":["thin","#666"],"left":["thin","#666"],"right":["thin","#666"]},"bgcolor":"#fff"}],"cellConfig":[{"column":0,"row":0,"value":"","styleIndex":1,"editable":true,"slash":{"text":"季度|销售额|商品","type":0,"lineStyle":"thin","lineColor":"#000"},"dataConfig":{},"type":6},{"column":1,"row":0,"value":"sale.季度","valueType":1,"expansion":2,"styleIndex":2,"editable":false,"scaleType":1,"dataConfig":{"value":"sale.季度","data_type":"2","axisType":1,"order":0,"merge":1},"type":2,"dataType":"2","columnSpan":2,"rowSpan":1},{"column":2,"row":0,"expansion":2},{"column":0,"row":1,"value":"sale.商品","valueType":1,"expansion":1,"styleIndex":1,"editable":false,"scaleType":1,"dataConfig":{"value":"sale.商品","data_type":"3","merge":1,"axisType":1,"order":0},"type":2,"dataType":"3"},{"column":1,"row":1,"value":"sale.销售额","valueType":1,"expansion":3,"styleIndex":3,"editable":false,"scaleType":1,"dataConfig":{"value":"sale.销售额","data_type":"2","aggregate":1,"axisType":2,"order":0},"type":2,"dataType":"2"},{"column":2,"row":1,"value":"=B2+C2[;B1:-1]","styleIndex":4,"type":5}],"groupConfig":[{"children":[],"noagg":false,"no_agg":false,"repeat":{"isRepeat":false,"repeatKey":"","repeatValue":""},"borderColor":"rgba(255, 123, 88)","bgColor":"rgba(255, 123, 88,0.5)","position":"A1:C2"}],"collapse":[]}}]
export const response_data_acc_cross = [{"商品": "A产品", "季度": 1, "销售额": 5890.56}, {
    "商品": "A产品",
    "季度": 2,
    "销售额": 4566.26
}, {"商品": "A产品", "季度": 3, "销售额": 7861.56}, {"商品": "A产品", "季度": 4, "销售额": 5654.56}, {
    "商品": "A产品",
    "季度": 1,
    "销售额": 5820.96
}, {"商品": "A产品", "季度": 2, "销售额": 5666.26}, {"商品": "A产品", "季度": 3, "销售额": 8661.56}, {
    "商品": "A产品",
    "季度": 4,
    "销售额": 5254.56
}, {"商品": "B产品", "季度": 1, "销售额": 5890.56}, {"商品": "B产品", "季度": 2, "销售额": 4456.16}, {
    "商品": "B产品",
    "季度": 3,
    "销售额": 7561.56
}, {"商品": "B产品", "季度": 4, "销售额": 3554.56}, {"商品": "B产品", "季度": 1, "销售额": 4620.96}, {
    "商品": "B产品",
    "季度": 2,
    "销售额": 5456.26
}, {"商品": "B产品", "季度": 3, "销售额": 8891.56}, {"商品": "B产品", "季度": 4, "销售额": 5623}, {
    "商品": "C产品",
    "季度": 1,
    "销售额": 4600.78
}, {"商品": "C产品", "季度": 2, "销售额": 5246}, {"商品": "C产品", "季度": 3, "销售额": 7761.56}, {
    "商品": "C产品",
    "季度": 4,
    "销售额": 8164
}, {"商品": "C产品", "季度": 1, "销售额": 4645.56}, {"商品": "C产品", "季度": 2, "销售额": 6579}, {
    "商品": "C产品",
    "季度": 3,
    "销售额": 7591.56
}, {"商品": "C产品", "季度": 4, "销售额": 7168}, {"商品": "D产品", "季度": 1, "销售额": 4678.56}, {
    "商品": "D产品",
    "季度": 2,
    "销售额": 6659
}, {"商品": "D产品", "季度": 3, "销售额": 7567.56}, {"商品": "D产品", "季度": 4, "销售额": 7498}, {
    "商品": "D产品",
    "季度": 1,
    "销售额": 5246
}, {"商品": "D产品", "季度": 2, "销售额": 4645}, {"商品": "D产品", "季度": 3, "销售额": 7561.8}, {
    "商品": "D产品",
    "季度": 4,
    "销售额": 7941
}, {"商品": "E产品", "季度": 1, "销售额": 5890.56}, {"商品": "E产品", "季度": 2, "销售额": 4566.26}, {
    "商品": "E产品",
    "季度": 3,
    "销售额": 7861.56
}, {"商品": "E产品", "季度": 4, "销售额": 5654.56}, {"商品": "E产品", "季度": 1, "销售额": 5820.96}, {
    "商品": "E产品",
    "季度": 2,
    "销售额": 5666.26
}, {"商品": "E产品", "季度": 3, "销售额": 8661.56}, {"商品": "E产品", "季度": 4, "销售额": 5254.56}, {
    "商品": "F产品",
    "季度": 1,
    "销售额": 5890.56
}, {"商品": "F产品", "季度": 2, "销售额": 4456.16}, {"商品": "F产品", "季度": 3, "销售额": 7561.56}, {
    "商品": "F产品",
    "季度": 4,
    "销售额": 3554.56
}, {"商品": "F产品", "季度": 1, "销售额": 4620.96}, {"商品": "F产品", "季度": 2, "销售额": 5456.26}, {
    "商品": "F产品",
    "季度": 3,
    "销售额": 8891.56
}, {"商品": "F产品", "季度": 4, "销售额": 5623}, {"商品": "G产品", "季度": 1, "销售额": 4600.78}, {
    "商品": "G产品",
    "季度": 2,
    "销售额": 5246
}, {"商品": "G产品", "季度": 3, "销售额": 7761.56}, {"商品": "G产品", "季度": 4, "销售额": 8164}, {
    "商品": "G产品",
    "季度": 1,
    "销售额": 4645.56
}, {"商品": "G产品", "季度": 2, "销售额": 6579}, {"商品": "G产品", "季度": 3, "销售额": 7591.56}, {
    "商品": "G产品",
    "季度": 4,
    "销售额": 7168
}, {"商品": "H产品", "季度": 1, "销售额": 4678.56}, {"商品": "H产品", "季度": 2, "销售额": 6659}, {
    "商品": "H产品",
    "季度": 3,
    "销售额": 7567.56
}, {"商品": "H产品", "季度": 4, "销售额": 7498}, {"商品": "H产品", "季度": 1, "销售额": 5246}, {
    "商品": "H产品",
    "季度": 2,
    "销售额": 4645
}, {"商品": "H产品", "季度": 3, "销售额": 7561.8}, {"商品": "H产品", "季度": 4, "销售额": 7941}]
export const _formula_acc_cross = "=B2+C2[;B1:-1]";
export const data_acc_cross = [
    {
        "type": 4,
        "name": "sale",
        "response_data": response_data_acc_cross
    }
]
export const option_acc_cross = {
    mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
    view: {
        width: () => document.documentElement.clientWidth,
        height: () => document.documentElement.clientHeight
    },   // 设置报表的宽高
    renderArrow: false,  // 是否显式 扩展方向图标
    showFreeze: false,  // 是否显式冻结线
    showGrid: false   // 是否显式网格线
};

export const reportPrintOptions_acc_cross = [
    {
        name: "交叉累计",
        //print: sheetConfig
    }
]

