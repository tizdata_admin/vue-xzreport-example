//遍历map1
for(  const key of  get_onUpdateDatasetlist().keys()){
    console.log(key)
}
//遍历map2
for(const value of get_onupdatedatamaps.values()){
    console.log(value)
}
//遍历map3
for(const entry of get_onupdatedatamaps.entries()){
    console.log(entry)
}
//遍历map4
for(const [key,value] of get_onupdatedatamaps.entries()){
    console.log(key,value)
}
//遍历map5
for(const {value,key} of get_onupdatedatamaps.entries()){
    console.log(key,value)
}

//类型转换
console.log(...get_onUpdateDatasetlist())

console.log([...get_onUpdateDatasetlist()])

let newArr = [...get_onupdatedatamaps()].filter(item=>{
    return true;
})
console.log(newArr)

get_onUpdateDatasetlist().forEach((value,index,key) => {
    console.log(key)
    console.log(index)
    console.log(value)
})


