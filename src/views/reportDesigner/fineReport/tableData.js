import { ref } from 'vue'

const tableData = ref([{
    name: 'finereport3801_abs_v',
    title: '绝对坐标-纵向扩展',
    routeName: 'finereport3801_abs_v',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_abs_v',
}, {
    name: 'finereport3801_abs_hori',
    title: '绝对坐标-横向扩展',
    routeName: 'finereport3801_abs_hori',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_abs_h'
},{
    name: 'finereport3801_rel_v',
    title: '相对坐标-纵向扩展',
    routeName: 'finereport3801_rel_v',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_rel_v',
},{
    name: 'finereport3801_rel_hori',
    title: '相对坐标-横向扩展',
    routeName: 'finereport3801_rel_hori',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_rel_h',

},{
    name: 'finereport3801_two_abs',
    title: '绝对坐标-双向扩展',
    routeName: 'finereport3801_two_abs',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_two_abs',

},{
    name: 'finereport3801_two_rel',
    title: '相对坐标-双向扩展',
    routeName: 'finereport3801_two_rel',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_two_rel',

},{
    name: 'finereport3801_two_other',
    title: '单行单列-双向扩展',
    routeName: 'finereport3801_two_other',
    vueSrc: '/views/reportDesigner/fineReport/fi3801_two_other',
}])
 export  {tableData}
