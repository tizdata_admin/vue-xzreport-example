import {defineStore} from "pinia";
export const usePreviewStore =defineStore('xzPreviewStore',{
    state:()=>({
            xzReportDesigners:new Map(),//key=designerId,value:{config,data}  //const {config,data} = sheetDesign.getData()
            xzChartDesigners:new Map(),
    }),
    actions:{
        saveReportDesignerPreviewData(reportDesignerId,config,data,__updateDatasetList=[],__updateData=[]){
            console.log("saveReportDesignerPreviewData:reportDesignerId:",reportDesignerId)
            console.log("saveReportDesignerPreviewData:config:",config)
            console.log("saveReportDesignerPreviewData:data:",data)
            console.log("saveReportDesignerPreviewData:__updateDatasetList:",__updateDatasetList)
            console.log("saveReportDesignerPreviewData:__updateData:",__updateData)
            this.xzReportDesigners.set(reportDesignerId,{config,data,__updateDatasetList,__updateData});
        },
        saveChartDesignerPreviewData(chartDesignerId,config,dataConfig,__updateDatasetList=[],__updateData=[]){
            console.log("saveChartDesignerPreviewData:chartDesignerId:",chartDesignerId)
            console.log("saveChartDesignerPreviewData:config:",config)
            console.log("saveChartDesignerPreviewData:dataConfig:",dataConfig)
            console.log("saveChartDesignerPreviewData:__updateDatasetList:",__updateDatasetList)
            console.log("saveChartDesignerPreviewData:__updateData:",__updateData)
            this.xzChartDesigners.set(chartDesignerId,{config,dataConfig,__updateDatasetList,__updateData});
        },
        getReportDesignerPreviewData(reportDesignerId){
            return this.xzReportDesigners.get(reportDesignerId);
        },
        getChartDesignerPreviewData(chartDesignerId){
            return this.xzChartDesigners.get(chartDesignerId);
        },

        cleanReportDesignerPreviewData(reportDesignerId){
           // console.log("before deleted:",this.xzReportDesigners.get(reportDesignerId));
            this.xzReportDesigners.delete(reportDesignerId);
           // console.log("after deleted:",this.xzReportDesigners.get(reportDesignerId));

        },
        cleanChartDesignerPreviewData(chartDesignerId){
           // console.log("before deleted:",this.xzChartDesigners.get(chartDesignerId));
            this.xzChartDesigners.delete(chartDesignerId);
           // console.log("after deleted:",this.xzChartDesigners.get(chartDesignerId));
        }
    }
})
