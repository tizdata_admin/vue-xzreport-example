import * as THREE from 'three';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { RoomEnvironment } from 'three/examples/jsm/environments/RoomEnvironment.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';
import $ from 'jquery';

let defaultOption = {}; // 控件绘图需要的属性
let jsonConfig = {
  // 配置json
  chartConfig: [],
  dataConfig: {},
};
let staticPath = "../bi/static"
export class Keyframes extends XZChart {
  constructor(selectors, config) {
    config.definitions = {
      ...XZChart.definitions,
      defaultOption,
      jsonConfig,
      type: 'frames',
      indexCount: 0,
      columnCount: 0,
      useXSelector: false,
      showDataSheet: false,
      showDataConfig: false
    }
    super(selectors, config);
    // this.chart = null;
    // this.init();
  }

  init() {
    this.chart = this.controlEl.el;
    // this.mixer = null;
    let mixer = null
    const clock = new THREE.Clock();
    this.clock = clock
    const container = this.chart;
    const stats = new Stats();
    this.stats = stats
    // container.appendChild(stats.dom);

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    this.renderer = renderer

    const pmremGenerator = new THREE.PMREMGenerator(renderer);
    this.pmremGenerator = pmremGenerator;

    const scene = new THREE.Scene();
    this.scene = scene;

    scene.background = new THREE.Color(0xbfe3dd);
    scene.environment = pmremGenerator.fromScene(new RoomEnvironment(renderer), 0.04).texture;

    const camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 100);
    this.camera = camera;
    camera.position.set(5, 2, 8);


    const controls = new OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0.5, 0);
    controls.update();
    controls.enablePan = false;
    controls.enableDamping = true;

    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath(`${staticPath}/jsm/libs/draco/gltf/`);
    this.dracoLoader = dracoLoader

    const loader = new GLTFLoader();
    loader.setDRACOLoader(dracoLoader);

    function animate() {
      requestAnimationFrame(animate);
      const delta = clock.getDelta();
      mixer.update(delta);
      controls.update();
      stats.update();
      renderer.render(scene, camera);
    }

    loader.load(`${staticPath}/models/gltf/LittlestTokyo.glb`, function (gltf) {
      const model = gltf.scene;
      model.position.set(1, 1, 0);
      model.scale.set(0.01, 0.01, 0.01);
      scene.add(model);
      mixer = new THREE.AnimationMixer(model);
      mixer.clipAction(gltf.animations[0]).play();
      animate();
    }, undefined, function (e) {
      console.error(e);
    });
    this.loader = loader;
    this.mixer = mixer

    this.render();
  }

  _transData() { }
  _transOption() { }
  resize() {
    const w = $(this.controlEl.el).width();
    const h = $(this.controlEl.el).height();
    this.camera.aspect = w / h;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(w, h);
  }
  render() {
    // this.renderer.render(this.scene, this.camera);
  }
}