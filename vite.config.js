import {fileURLToPath, URL} from 'node:url'
import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {viteMockServe} from "vite-plugin-mock";
import {createSvgIconsPlugin} from "vite-plugin-svg-icons";
import  path from "path";
const root  = process.cwd();
function pathResolve(dir) {
    return path.resolve(root, dir);
}



// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        viteMockServe({
            mockPath: 'mock',
        }),
        createSvgIconsPlugin({
            iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
            symbolId: 'icon-[dir]-[name]',
        })
    ],
    css: {
        preprocessorOptions: {
            scss: {
                javascriptEnabled: true,
                additionalData: `@import "./src/styles/variable.scss";`,
            }
        }
    },
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    define:{
        __VUE_PROD_DEVTOOLS__: true,
    },
    // cacheDir: '.cache',
})
