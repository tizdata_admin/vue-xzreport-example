import {useRouter} from 'vue-router'
import finereport3801_abs_hori from "@/views/reportDesigner/fineReport/fr3801_abs_h/FineReport3801_abs_H.vue"
import finereport3801_abs_v from "@/views/reportDesigner/fineReport/fr3801_abs_v/FineReport3801_abs_V.vue"
import finereport3801_rel_hori from "@/views/reportDesigner/fineReport/fr3801_rel_h/FineReport3801_rel_H.vue"
import finereport3801_rel_v from "@/views/reportDesigner/fineReport/fr3801_rel_v/FineReport3801_rel_V.vue"
import finereport3801_two_abs from "@/views/reportDesigner/fineReport/fr3801_two_abs/FineReport3801_two_abs.vue"
import finereport3801_two_rel from "@/views/reportDesigner/fineReport/fr3801_two_rel/FineReport3801_two_rel.vue"
import finereport3801_two_other from "@/views/reportDesigner/fineReport/fr3801_two_other/FineReport3801_two_other.vue"

const GlobalComponentsPlugin = {
  install(app) {
    // 全局注册组件
    app.component('finereport3801_abs_hori', finereport3801_abs_hori);
    app.component('finereport3801_abs_v', finereport3801_abs_v);
    app.component('finereport3801_rel_hori', finereport3801_rel_hori);
    app.component('finereport3801_rel_v', finereport3801_rel_v);
    app.component('finereport3801_two_abs', finereport3801_two_abs);
    app.component('finereport3801_two_rel', finereport3801_two_rel);
    app.component('finereport3801_two_other', finereport3801_two_other);

  }
};

// 自动安装插件
export  {GlobalComponentsPlugin};

