import router from '@/router'
import nprogress from 'nprogress';
import "nprogress/nprogress.css";
import {useUserStore} from "./stores/modules/userStore";

import pinia from "./stores";

let userStore = useUserStore(pinia);
console.log(JSON.stringify(userStore));

router.beforeEach(async (to: any, from: any, next: any) => {
    //if (1 == 1) next();
    nprogress.start();
    let token = userStore.token;
    let username = userStore.username;
    if (token) {
        if (to.path == '/login') {
            next({path: '/'});
        } else  {
            if (username){
                next();
            } else {
                try{
                    await userStore.userInfo();
                    next({...to});
                }catch(e){
                    //await userStore.userLogout();
                    //next({path:'/login',query:{redirect:to.path}});
                    next();
                }

            }

        }

    } else {
        if (to.path === '/login') {
            next();
        } else {
            next({path: '/login', query: {redirect: to.path}});
        }
    }

});
router.afterEach((to: any, from: any) => {

    nprogress.done();
});
