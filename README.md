# XZReport - 小智BI+小智报表

v3.7.2 | 2023-05-11


一款原生JS的免费纯前端报表控件，帮助产品高效解决各种报表绘制与导出打印问题。功能涵盖：报表样式绘制、打印格式设置、数据处理函数、交互操作功能等！

- 原生JS开发包形态，可快速应用于各种开发框架。
- 类Excel操作界面，拖拽式报表设计交互，内置大量数据处理函数。
- 丰富的报表样式支持，如：动态行列交叉报表、标签报表、预警报表等。
- 支持报表导出、列表批量打印、低分辨率打印、格式套打打印等交互操作。
- 可直接调用API方法，快速搭建报表场景，如：内置报表工具、搭建报表中心等。

```
开源协议：`功能永久免费、可以商用、代码不开放`。
```


快速体验： [https://xzreport.com](https://xzreport.com "xzreport.com")

# xzreportAnddesigner150_example_vue3

小智报表设计器端，小智报表展现端，vue3的嵌入演示样例

## 操作步骤

## Project Setup

```sh
pnpm install
```

### Compile and Hot-Reload for Development

```sh
pnpm run dev
```


### xzReport展现效果（小智报表纯展现）
<img src="./images/411-xzReport1.png" style="zoom:50%;" alt=""/>

### xzReportDesigner展现效果（小智报表设计器）
<img src="./images/511-xzReportDesigner-1.png" style="zoom:50%;" alt=""/>



反馈问题
-----------------------------------

> 发现bug 请在github上[发issue](https://github.com/tianzhidata/XZReport/issues/new)



开发文档
-----------------------------------

- 官方网站： https://www.xzreport.com/
- 在线服务 ： [小智报表 (xzreport.com)](https://designer.xzreport.com/login)
- 小智BI官方文档： https://doc.tizdata.com/xiaozhi/651
- 小智报表官方文档：https://doc.tizdata.com/xiaozhi/650
- QQ交流群：894709724
- 小智数据产品使用手册：https://doc.tizdata.com/xiaozhi/




产生背景
-----------------------------------

在软件开发中，需要解决用户对报表方面的一系列需求：

1.复杂的报表样式需求。
软件系统在开发报表时，会遇到各种复杂的报表需求和挑战，如：动态行列报表、多重聚合计算报表、交叉透视报表、自由布局报表、平铺报表、分栏报表、多源报表等。

2.自定义导出打印需求。
基于软件产生的发票、合同、标签、订单等单据和报表，用户可以根据自己的需求和喜好，对单据的内容、布局、样式等进行自定义和调整，以满足不同场景下的打印需求。

3.自己设计报表的需求。
用户期待软件系统中能提供自己设计报表的功能，无需二次开发，通过简单的拖拽和配置，即可灵活地设计出想要的报表样式和内容，从而灵活满足企业的个性化报表需求。




为什么选择 XZReport?
-----------------------------------

>    借助小智报表来实现报表开发，可以降低开发成本，提高开发效率，以及为用户创造了更好的产品体验。

- 轻松实现各种复杂报表样式
- 内置大量数据处理函数
- 拖拽式操作实现报表样式调整
- 制作的报表自动适配和兼容
- 支持按模板打印/自定义打印等
- 支持分组、交叉，合计、表达式等复杂报表
- 灵活打印设置轻松实现格式套打
- 对低分辨率打印做了很好地支持
- 对低分辨率打印做了很好地支持
- 提供类Excel交互的报表设计器

# api--小智BI设计器
## 嵌入脚本
```shell
<div id="chartDesigner" class="chartDesigner"></div>

import "./XZChartDesigner/report.js";
import "./XZChartDesigner/report.css";
```
## 新建模板
### xzChartDesigner
```shell
...
  <div id="container_cd8_main" style="width: 100%; height: 1024px"></div>
...
XZChartDesigner.setKey('授权码')
let option = { //参数配置说明 根据您的需求填写option
  uploadImage: { //上传图片相关信息 如果不传该字段时，设计器用到的所有上传图片配置项均不显示。
    server: "url",//上传图片地址
    method: "post",//上传图片请求方式
    headers: { //上传图片请求头
      "Authorization": "Token XXXXXXXX"
    },
    fieldName: "file",
    resultField: "file"
  },
  pageData: { //画布初始化时的宽高
    width: '1920px',
    height: '1080px'
  },
  dataSet: [ //数据集按钮的方式
    { //Json数据
      id: 1,
      value: "新建Json数据集"
    },
    { //http请求数据
      id: 2,
      value: "新建Http接口数据集"
    },
    { //API服务数据
      id: 3,
      value: "API服务数据集"
    },
    {
      id: 4,
      value: "外部数据集"  //  新建一个新的报表实例，如果希望可以预先动态从后台提供几个数据集供其使用，这个配置必须有；
      // 然后只要动态的从后台拿到n个数据集，全部用 results格式装配，onUpdateDatasetList方法来触发
    }
  ]
}
...
onMounted(() => {
    designer = new XZChartDesigner("#container_cd8_main", option);
})
```
## 数据保存
### xzChartDesigner
```shell
...
  let report = designer.getSaveData();
...
  //保存到业务系统的后台
  axios({
    method: "post",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      report:report
    }
  }).then((response)=>{
    ....
  })
```
## 模板修改/模板回显
### xzChartDesigner
```shell
...画布容器
  <div id="container_cd8_main" style="width: 100%; height: 1024px"></div>
...
let option = { //参数配置说明 根据您的需求填写option
  uploadImage: { //上传图片相关信息 如果不传该字段时，设计器用到的所有上传图片配置项均不显示。
    server: "url",//上传图片地址
    method: "post",//上传图片请求方式
    headers: { //上传图片请求头
      "Authorization": "Token XXXXXXXX"
    },
    fieldName: "file",
    resultField: "file"
  },
  pageData: { //画布初始化时的宽高
    width: '1920px',
    height: '1080px'
  },
  dataSet: [ //数据集按钮的方式
    { //Json数据
      id: 1,
      value: "新建Json数据集"
    },
    { //http请求数据
      id: 2,
      value: "新建Http接口数据集"
    },
    { //API服务数据
      id: 3,
      value: "API服务数据集"
    },
    {
      id: 4,
      value: "外部数据集"  //  新建一个新的报表实例,如果希望可以预先动态从后台提供几个数据集供其使用,这个配置必须有;
      // 然后只要动态的从后台拿到n个数据集,全部用 results格式装配,onUpdateDatasetList方法来触发
    }
  ]
}
....
onMounted(() => {
   axios({
    method: "post",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      reportType: "chartDesigner",
      reportId: 'report1',
    }
  }).then((response) => {
    report = response.data.data.report;//保存时候:let report = designer.getSaveData();
    designer = new XZChartDesigner("#chartDesigner2", option, report);
  }).catch((error) => {
  ...
  })
})
```
## onUpdateDatasetList 外部数据集
### xzChartDesigner 到后台取数据集（表结构）
```shell
...
let option={......}
const onUpdateDatasetList = () => {
 axios({
    method: "get",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.onUpdateDatasetList
  })
}
option.onUpdateDatasetList=onUpdateDatasetList
```
## onUpdateData 外部数据集记录
### xzChartDesigner 到后台取数据集的记录（表记录）
```shell
...
let option={......}
const onUpdateData = (datasets, params) => {
    //根据 入口参数 datasets,params的信息,业务层面拼接查询
 axios({
    method: "get",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.records
  })
}
option.onUpdateData=onUpdateData
```
## 清除设计器画布
### xzChartDesigner
```shell
    //option 参考前面的配置
    document.getElementById("container_cd8_main").innerHTML = "";
    designer = new XZChartDesigner("#container_cd8_main", option);
```
## 自定义BI控件 toDo  
### 参考小智数据官方文档 https://doc.tizdata.com/xiaozhi/199/

# api--小智BI展示器
## 小智BI展现端 嵌入脚本
```shell
<div id="chartContainer" style="width:100%;height:100%;"></div>

import "./XZChart/xzchart.js";
import "./XZChart/xzchart.css";
```
## 渲染
### xzChart
```shell
.... 首先获取BI模板
let data;
   axios({
    method: "get",
    url: "/api/........",
    data: {
      ....
    }
  }).then((response) => {
    report = response.data.data.report;
  //提示一下 当初保存的方法：report= designer.getSaveData();
    config=report.config//展现端需要的配置信息
    dataConfig=report.data.dataConfig //BI展现端需要的初始数据，
  }).catch((error) => {
    ......
  })
....
XZDashboard.setKey('授权码')

let option = {reportConfig:config,reportData:[],paramsConfig:[]}
const reportInstance = new XZDashboard("#container_cd8_Echo", option);
reportInstance.updateData(dataConfig);//在合适的时候会触发下面的onUpdateData方法

onUpdateData = (datasets, params) => {//展现端的数据记录动态获取
  //根据 入口参数 datasets,params的信息,业务层面拼接查询
 axios({
    method: "get",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.records
  })
}
option.onUpdateData=onUpdateData

```
## 查询参数 onUpdateData = (datasets, params) => {...}
```shell
// datasets => [{name:"数据表名称",id:"数据表id"...}]  参与获取数据的数据集
// params => [{name:"参数名称",value:"参数值"}]  value的类型是字符串或者数组
// 该方法的返回值是一个Promise,根据业务需求,后台拼接服务/sql
const onUpdateData = (datasets,params)=>{
  ...
  return new Promise((resolve, reject)=>{
    // resolve的数据格式
    let results = [
      {
        name:"测试", //必填项
        id:"测试" //必填项
        response_data:[] // [{a:1,b:2}]
      }
    ]
    resolve(results)
  })
let option = {
  onUpdateData
}
```
# api--小智报表设计器
## 小智报表设计器 嵌入脚本
```shell
<div id="chartContainer" style="width:100%;height:100%;"></div>
import "./XZReportDesigner/xzreportdesigner.js";
import "./XZReportDesigner/xzreportdesigner.css";
```
## 新建模板
### xzReportDesigner
```shell
  <div id="container_indentorMain" style="width: 100%; height: 100%"></div>

const dataset = [ //  添加数据集按钮
  { //Json数据
    id: 1,
    value: "新建Json数据集"
  },
  { //http请求数据
    id: 2,
    value: "新建Http接口数据集"
  },
  { //API服务数据
    id: 3,
    value: "API服务数据集"
  },
  {
    id: 4,
    value: "外部数据集"
  }
];
const options = {
  row: 100,
  col: 25,
  width:()=>width
  height:()=>height
  renderArrow: true,  // 是否显式 扩展方向图标
  showFreeze: true,  // 是否显式冻结线
  showGrid: true,   // 是否显式网格线
  showPreviewToolbar:true,
  showBottombar:true,
  showSelect:true,
  dataset
}
const onUpdateDatasetList = () => {
  return new Promise((resolve, reject) => {
    // resolve  一般axios获取，参考onUpdateDatasetList接口
    resolve(results)  /
  })
}
options.onUpdateDatasetList = onUpdateDatasetList
let sheetDesign;
onMounted(() => {
  sheetDesign = XZReportDesigner("#container_indentorMain", options);
})
```
## 数据保存
### xzReportDesigner
```shell
const {config, data} = sheetDesign.getData()
  //保存到业务系统的后台
  axios({
    method: "post",
    url: "/api/xzReportDesigner/公司远程api",
    data: {
      config:config,
      data:data
    }
  }).then((response)=>{
    ....
  })
```
## 模板修改/模板回显
### xzReportDesigner
```shell
//option,onUpdateDatasetList,onUpdateData 配置略
  axios.post拿到报表设计器的数据
//参考设计器当时的保存方法:const {config, data} = sheetDesign.getData())
  ...
  sheetDesign = XZReportDesigner("#container", option);
  sheetDesign.loadData({config: config, data: data});
  //这里的data是保存报表模板时候提供的信息,和 onUpdateData带参数查询的方法不同
```
## 模板数据更新
### xzReportDesigner？？？？
```shell
  document.getElementById(div_id).innerHTML = "";//酌情可选
  sheetDesign = XZReportDesigner("#containerTabs1", options);

  try {
    sheetDesign.loadData({config: sheetConfig, data: data});//和bi设计器的不同,这里的data必须是初始的data(保存时候的data)
  } catch (e) {
    console.log(e);
  }
```
## 数据集类型配置
### xzReportDesigner 
数据来源目前支持选择Json数据、http请求数据、API服务数据,外部数据集四种数据集类型。可以支持接收外部Json数据。
```shell
let option={......参考新增/修改}
....
 dataSet: [ //数据集按钮的方式
    { //Json数据
      id: 1,
      value: "新建Json数据集"
    },
    { //http请求数据
      id: 2,
      value: "新建Http接口数据集"
    },
    { //API服务数据
      id: 3,
      value: "API服务数据集"
    },
    {
      id: 4,
      value: "外部数据集"  //  新建一个新的报表实例，如果希望可以预先动态从后台提供几个数据集供其使用，这个配置必须有；
      // 然后只要动态的从后台拿到n个数据集，全部用 results格式装配，onUpdateDatasetList方法来触发
    }
  ]
option.dataSet:dataSet;
```
## 外部数据集 onUpdateDatasetList
### xzReportDesigner到后台取数据集（表结构）
```shell
...
let option={......}
const onUpdateDatasetList = () => {
 axios({
    method: "get",
    url: "/api/xzReportDesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.onUpdateDatasetList
  })
}
option.onUpdateDatasetList=onUpdateDatasetList
```
## 外部数据集记录 onUpdateData
### xzReportDesigner到后台取数据集的记录（表记录）
```shell
...
let option={......}
const onUpdateData = (datasets, params) => {
    //根据 入口参数 datasets,params的信息,业务层面拼接查询
    // datasets => [{name:"数据表名称",id:"数据表id"...}]  参与获取数据的数据集
    // params => [{name:"参数名称",value:"参数值"}]  value的类型是字符串或者数组
 axios({
    method: "get",
    url: "/api/xzReportDesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.records
  })
}
option.onUpdateData=onUpdateData
```

# api--小智报表展示器
## 小智报表展现端 嵌入脚本
```shell
<div id="chartContainer"></div>
import "./XZReport/xzreport.js";
import "./XZReport/xzreport.css";
```
## 渲染
### xzReport
```shell
报表渲染需要提供：报表模板json（保存时候的json），报表模板需要的数据集记录
//参考报表模板保存 const {config, data} = sheetDesign.getData();
// option配置略
onMounted(() => {
  try{
    spreadSheet = XZReport("#containerTabs1_echo", option);
    spreadSheet.update(config,data);
  }catch (error){
    console.log(error);
  }
});
spreadSheet.update(config, data);

```
## 外部数据集记录 onUpdateData
### xzReport到后台取数据集的记录（表记录）
```shell
...
let option={......}
const onUpdateData = (datasets, params) => {
    //根据 入口参数 datasets,params的信息,业务层面拼接查询
    // datasets => [{name:"数据表名称",id:"数据表id"...}]  参与获取数据的数据集
    // params => [{name:"参数名称",value:"参数值"}]  value的类型是字符串或者数组
 axios({
    method: "get",
    url: "/api/xzchartdesigner/公司远程api",
    data: {
      ....
    }
  }).then((response)=>{
    return response.data.data.records
  })
}
option.onUpdateData=onUpdateData
```
## 查询参数 onUpdateData = (datasets, params) => {...}
```shell
// datasets => [{name:"数据表名称",id:"数据表id"...}]  参与获取数据的数据集
// params => [{name:"参数名称",value:"参数值"}]  value的类型是字符串或者数组
// 该方法的返回值是一个Promise,根据业务需求,后台拼接服务/sql
const onUpdateData = (datasets,params)=>{
  ...
  return new Promise((resolve, reject)=>{
    // resolve的数据格式
    let results = [
      {
        name:"测试", //必填项
        id:"测试" //必填项
        response_data:[] // [{a:1,b:2}]
      }
    ]
    resolve(results)
  })
let option = {
  onUpdateData
}
```
## 关于vue2.x嵌入(下面以小智报表设计器为例,其余类推)
### @/api/xzrd.js 
```shell   
import request from '@/utils/request'
// reqType='onUpdateDatasetList/onUpdateData/sheetconfig/sheetconfigdata'
// dataCode = 1/2/3...
// ext扩展属性
export function getxzdata(reqType, dataCode, ext) {
  return request({
    url: '/vue-admin-template/xzrd/datainfo',
    method: 'post',
    data: { reqType, dataCode, ext }
  })
}
```
### @/views/xereportdesigner/xzrd_1.vue
```shell
<template>
  <div>
    <el-button @click="saveData('updateServiceMain')">保存数据</el-button>
    <div id="updateServiceMain"></div>
  </div>
</template>
<script>
import '@/assets/XZReportDesigner/xzreportdesigner.js'
import '@/assets/XZReportDesigner/xzreportdesigner.css'
import { getxzdata } from '@/api/xzrd'

export default {
  data() {
    return {
      sheetDesign: null,
      options: {
        row: 100,
        col: 25,
        width: () => 1900,
        // width:()=>width
        // height:()=>height
        renderArrow: true,
        showFreeze: true,
        showGrid: true,
        showPreviewToolbar: true,
        showBottombar: true,
        showSelect: true,
        dataset: [
          {
            'id': 1,
            'value': '新建Json数据集'
          },
          {
            'id': 2,
            'value': '新建Http接口数据集'
          },
          {
            'id': 3,
            'value': 'API服务数据集'
          },
          {
            'id': 4,
            'value': '外部数据集'
          }
        ]
      }
    }
  },
  mounted() {
    this.options.onUpdateDatasetList = this.onUpdateDatasetList
    try {
      // eslint-disable-next-line no-undef
      this.sheetDesign = XZReportDesigner('#updateServiceMain', this.options)
    } catch (error) {
      console.log('..........created,throw errors:', error)
    }
  },
  methods: {
    onUpdateDatasetList() {
      return new Promise((resolve, reject) => {
        const rr = getxzdata('onUpdateDatasetList', '1', 'ext').then((res) => {
          return res.data
        })
        return resolve(rr)
      })
    },
    onUpdateData(datasets, params) {
      return new Promise((resolve, reject) => {
        const rr = getxzdata('onUpdateData', '1', 'ext').then((res) => {
          return res.data
        })
        return resolve(rr)
      })
    },
    saveData(reportDesignerPreviewId) {
      // config 为报表配置 ， data为 数据配置
      const { config, data } = this.sheetDesign.getData() // 报表设计器的保存事件
      // usePreStore.saveReportDesignerPreviewData(reportDesignerPreviewId, config, data)
      console.log(config)
      console.log(data)
    }

  }
}
</script>
```
## 工具栏  
### 参考小智数据官方文档 https://doc.tizdata.com/xiaozhi/199/
## 打印  
### 参考小智数据官方文档 https://doc.tizdata.com/xiaozhi/199/
## 打印预览 
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 导出EXCEL  
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 导出PDF    
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 画布       
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 目录       
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 组件图层       
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/
## 数据结构  
### 参考小智数据官方文档  https://doc.tizdata.com/xiaozhi/199/

# BI设计器VUE3样例
## 样例1
<img src="./images/xzchartdesigner/小智BI设计器样例1-1.png" style="zoom:50%;" alt=""/>

<img src="./images/xzchartdesigner/小智BI设计器样例1-2.png" style="zoom:50%;" alt=""/>

<img src="./images/xzchartdesigner/小智BI设计器样例1-3.png" style="zoom:50%;" alt=""/>

## 样例2
<img src="./images/xzchartdesigner/小智BI设计器样例2-1.png" style="zoom:50%;" alt=""/>

<img src="./images/xzchartdesigner/小智BI设计器样例2-2.png" style="zoom:50%;" alt=""/>

### 样例3
<img src="./images/xzchartdesigner/小智BI设计器样例3.png" style="zoom:50%;" alt=""/>

# BI展示器样例 toDo

# 报表设计器样例 toDo
## 样例1
<img src="./images/xzreportDesigner/小智报表设计器样例1-1.png"  style="zoom:50%;" alt=""/>

## 样例2
<img src="./images/xzreportDesigner/小智报表设计器样例2-1.png"  style="zoom:50%;" alt=""/>

## 样例3
<img src="./images/xzreportDesigner/小智报表设计器样例3-1.png"  style="zoom:50%;" alt=""/>


# 报表展示器样例  toDo



报表设计与预览效果
-----------------------------------

- 同比报表

<img src="images/报表样例图/同比报表图.webp" style="zoom:50%;" alt=""/>

- 比较报表

<img src="images/报表样例图/比较报表.webp" style="zoom:50%;" alt=""/>


- 跨层累计报表

<img src="images/报表样例图/跨层累计报表.webp" style="zoom:50%;"  alt=""/>

- 双向分组小计

<img src="images/报表样例图/双向分组小计报表.webp" style="zoom:50%;"  alt=""/>

- 条件汇总报表

<img src="images/报表样例图/条件汇总报表.webp" style="zoom:50%;"  alt=""/>

- 自由布局报表

<img src="images/报表样例图/自由布局报表.webp" style="zoom:50%;"  alt=""/>

- 组内占比报表

<img src="images/报表样例图/组内占比报表.webp" style="zoom:50%;"  alt=""/>

- 分片重复报表

<img src="images/报表样例图/分片重复报表.webp" style="zoom:50%;"  alt=""/>

- 交叉累计报表

<img src="images/报表样例图/交叉累计报表.webp" style="zoom:50%;"  alt=""/>

- 绝对坐标-纵向扩展

<img src="images/xzreportDesigner/绝对坐标-纵向扩展.png" alt="">

- 绝对坐标-横向扩展

<img src="images/xzreportDesigner/绝对坐标-横向扩展.png" alt="">

- 相对坐标-纵向扩展

<img src="images/xzreportDesigner/相对坐标-纵向扩展.png" alt="">

- 相对坐标-横向扩展

<img src="images/xzreportDesigner/相对坐标-横向扩展.png" alt="">

- 绝对坐标-双向扩展

<img src="images/xzreportDesigner/绝对坐标-双向扩展.png" alt="">

- 相对坐标-双向扩展

<img src="images/xzreportDesigner/相对坐标-双向扩展.png" alt="">

- 单行单列-双向扩展

<img src="images/xzreportDesigner/单行单列-双向扩展.png" alt="">

# 常见问题解答
## 需要大量的数据集 数据记录的开发吗？
```shell
前端js 回调方法
onUpdateDatasetList

场景1 关系型数据库
业务系统添加2张表  crud的功能
databaseConn   数据库连接表(dcId,host/ip,port,user,password,...)
datasetList    数据集表   (Id,dcId, categoryId,tablefields_sql...,);categoryId是分类

后台发布的通用方法
restfull api: getOnUpdateDatasetList(userToken:string,dataSetIds:string[]));
    该方法可以获取为当前用户配置的全部数据集(带查询参数的描述json格式),
    或者:dataSetIds是本次请求需要的数据集ids; 
```

授权协议
-----------------------------------

开源版本遵循 LGPL 条款发布，如果您不能接受 LGPL，并且需要将该项目用于商业用途且不想保留版权信息，需购买[商业授权]([报表工具,小智报表,报表控件,内嵌式报表工具,OEM，表格控件,开源报表工具-产品定价 (xzreport.com)](https://www.xzreport.com/vip))。

