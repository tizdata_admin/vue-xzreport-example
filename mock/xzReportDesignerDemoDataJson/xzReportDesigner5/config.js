let config = [{
    "name": "sheet1",
    "SheetData": {
        "freeze": "A1",
        "columnHead": [
            {
                "Column": 0
            },
            {
                "Column": 1
            },
            {
                "Column": 2
            }
        ],
        "rowHead": [
            {
                "Row": 0
            },
            {
                "Row": 1
            }
        ],
        "stylesConfig": [
            {
                "border": {
                    "bottom": [
                        "thin",
                        "#000"
                    ],
                    "top": [
                        "thin",
                        "#000"
                    ],
                    "left": [
                        "thin",
                        "#000"
                    ],
                    "right": [
                        "thin",
                        "#000"
                    ]
                }
            }
        ],
        "cellConfig": [
            {
                "column": 0,
                "row": 0,
                "value": "商品名称",
                "styleIndex": 0,
                "type": 6
            },
            {
                "column": 1,
                "row": 0,
                "value": "区域名称",
                "styleIndex": 0,
                "type": 6
            },
            {
                "column": 2,
                "row": 0,
                "value": "销售额",
                "styleIndex": 0,
                "type": 6
            },
            {
                "column": 0,
                "row": 1,
                "value": "商品销售.商品名称",
                "valueType": 1,
                "expansion": 1,
                "styleIndex": 0,
                "editable": false,
                "scaleType": 1,
                "dataConfig": {
                    "value": "商品销售.商品名称",
                    "type": "3",
                    "merge": 1,
                    "axisType": 1,
                    "data_type": "3",
                    "order": 0
                },
                "type": 2,
                "dataType": "3"
            },
            {
                "column": 1,
                "row": 1,
                "value": "商品销售.区域名称",
                "valueType": 1,
                "expansion": 1,
                "styleIndex": 0,
                "editable": false,
                "scaleType": 1,
                "dataConfig": {
                    "value": "商品销售.区域名称",
                    "type": "3",
                    "merge": 1,
                    "axisType": 1,
                    "data_type": "3",
                    "order": 0
                },
                "type": 2,
                "dataType": "3"
            },
            {
                "column": 2,
                "row": 1,
                "value": "商品销售.销售额",
                "valueType": 1,
                "expansion": 1,
                "styleIndex": 0,
                "editable": false,
                "scaleType": 1,
                "dataConfig": {
                    "value": "商品销售.销售额",
                    "type": "2",
                    "aggregate": 1,
                    "axisType": 2,
                    "data_type": "2",
                    "order": 0
                },
                "type": 2,
                "dataType": "2"
            }
        ],
        "groupConfig": [],
        "collapse": [],
        "waterMask": null,
        "toolbarButtonList": [
            {
                "showIcon": true,
                "showText": true,
                "text": "首页",
                "icon": "fa fa-step-backward",
                "color": "#2c3e50",
                "show": true,
                "type": 1
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "上一页",
                "icon": "xzreport-icon-arrow-left-filling",
                "color": "#2c3e50",
                "show": true,
                "type": 2
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "当前页/总页数",
                "icon": "fa fa-file-text-o",
                "color": "#2c3e50",
                "show": true,
                "type": 3
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "下一页",
                "icon": "xzreport-icon-arrow-right-filling",
                "color": "#2c3e50",
                "show": true,
                "type": 4
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "末页",
                "icon": "fa fa-step-forward",
                "color": "#2c3e50",
                "show": true,
                "type": 5
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "打印",
                "color": "#2c3e50",
                "icon": "fa fa-print",
                "show": true,
                "type": 6
            },
            {
                "showIcon": true,
                "showText": true,
                "text": "导出",
                "icon": "fa fa-download",
                "color": "#2c3e50",
                "show": true,
                "type": 7
            }
        ],
        "paramsConfig": [],
        "dataConfig": [
            {
                "name": "商品销售",
                "type": 4,
                "response_data": [],
                "columns": [
                    {
                        "name": "商品名称",
                        "data_type": 3,
                        "axis_type": 1,
                        "sheetname": "商品销售",
                        "value": "商品名称"
                    },
                    {
                        "name": "区域名称",
                        "data_type": 3,
                        "axis_type": 1,
                        "sheetname": "商品销售",
                        "value": "区域名称"
                    },
                    {
                        "name": "销售额",
                        "data_type": 2,
                        "axis_type": 1,
                        "sheetname": "商品销售",
                        "value": "销售额"
                    }
                ],
                "params": [
                    {
                        "name": "商品名称",
                        "data_type": 3,
                        "axis_type": 1,
                        "sheetname": "商品销售",
                        "value": "商品名称"
                    },
                    {
                        "name": "区域名称",
                        "data_type": 3,
                        "axis_type": 1,
                        "sheetname": "商品销售",
                        "value": "区域名称"
                    }
                ],
                "path": "商品销售id",
                "id": "商品销售id"
            }
        ],
        "apiConfig": {
            "url": "",
            "key": "",
            "secret": ""
        }
    }
}]
export {config}