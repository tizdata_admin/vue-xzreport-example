//登录接口参数ts类型
export interface loginForm {
    username: string,
    password: string
}

interface dataType {
    token?: string,
    message?:string
}

//登录接口返回
export interface loginResponseData {
    code: number,
    data: dataType
}

interface user{
    checkUser:{
        userId:number,
        avatar:string,
        username:string,
        password:string,
        desc:string,
        roles:string[],
        buttons:string[],
        routes:string[],
        token:string
    }
}
//服务返回用户信息的相关的数据类型
export interface userResponseData{
    code:number,
    data:user,
}