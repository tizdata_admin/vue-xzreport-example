let echarts_option = {
    legend: {},
    tooltip: {},
    dataset: {
        // 提供一份数据。
        source: [
            ['product', '2015', '2016', '2017'],
            ['Matcha Latte', 43.3, 85.8, 93.7],
            ['Milk Tea', 83.1, 73.4, 55.1],
            ['Cheese Cocoa', 86.4, 65.2, 82.5],
            ['Walnut Brownie', 72.4, 53.9, 39.1]
        ]
    },
    // 声明一个 X 轴，类目轴（category）。默认情况下，类目轴对应到 dataset 第一列。
    xAxis: { type: 'category' },
    // 声明一个 Y 轴，数值轴。
    yAxis: {},
    // 声明多个 bar 系列，默认情况下，每个系列会自动对应到 dataset 的每一列。
    series: [{ type: 'bar' }, { type: 'bar' }, { type: 'bar' }]
};




let jsonConfig = {
    // 配置json
    chartConfig: [
        {
            "title": "echartsBar", //样式分组的名称
            "key": "chartStyle",	// 样式分组的类型
            "children": [	//子样式组
                {
                    "title": "字体颜色", //样式配置项名称
                    "key": "color", //样式配置项对应的控件所需要的属性名称
                    "widget": 5, //样式配置项类型  在本代码中的最后有对应类型说明
                    "default": 'rgba(0, 0, 0, 1)' //样式配置项默认值
                }
            ]
        }
    ],
    dataConfig: {},
};

// let staticPath = "../bi/static" //静态资源路径
class Keyframes extends XZChart { // XZChart 为控件基础类，基于这个类可以做数据的显示等控件事件的实现。
    constructor(selectors, config) {
        super(selectors, config);
    }

    definition() {
        this.definitions.type = 'frames';
        this.definitions.jsonConfig = _.cloneDeep(jsonConfig);
        this.definitions.columnCount = [0, 1];
        /**
         config.definitions 的说明
         type, //控件类型 String  例 "InputFilter"   default:''
         jsonConfig, //json配置  Object  default:{}
         width, //控件宽度 String  例  '200px'  default:'960px'
         height, //控件高度 String  例  '200px'  default:'540px'
         defaultOption, //控件默认配置项  Object  default:{}
         indexCount, //需要维度字段数 Array | number  例  0  -1  [0,-1]   default:0
         columnCount, //需要度量字段数 Array | number  例  0  -1  [0,-1]      default:0
         indexName, //维度实际显示名字 String  例  维度  字段   显示值值  指标  default:'维度'
         columnName, //度量实际显示名字 String  例  度量  字段   实际值   指标  default:'度量'
         designerScale, //控件显示类型  Number  例  1  2   1 -> 缩放填充 2 -> 保持控件的宽高  default:1
         useXSelector,   //是否需要X选择器  Boolean  例  true  false  default:true
         showDataSheet,  //是否显示数据集  Boolean    default:true
         showDataConfig  //是否显示控件选择器 数据过滤器 标签提示字段  default:true
         noDataRender  // 没有数据时是否渲染
         isFilterChart  // 是否是过滤器控件
         multiIndex   // 多个维度
         */
    }

    init() {
        let controlEl = this.controlEl.el;
        this.el = document.createElement('div');
        this.el.innerHTML = `这是一个自定义控件`;
        controlEl.appendChild(this.el);
        this.echartsBar= echarts.init(this.el);


    }

    _transData() {
        this.render();
    }

    _transOption(chartConfig) {
        console.log(chartConfig, 'chartConfig')
        this.chartOptions = chartConfig;
        this.render();
    }

    resize() {
    }

    render() {
        console.log(this.chartOptions);
        if (this.el) {
            this.el.style.color = this.chartOptions?.chartStyle?.color;
            const {dataHandler} = this; //整理数据的数据集
            const formatData = dataHandler.getFormatData(); //获取格式化后的数据
            const {columns, data} = formatData; //columns 字段组 、data 格式化后数据
            this.el && data[0] && (this.el.innerHTML = `这是一个自定义控件 ${columns[0]}=${data[0][0]}`)

            this.echartsBar.setOption(echarts_option);
        }
    }
}

window.Keyframes = Keyframes;
