let onUpdateDatasetlist = [
    {
        name: "订货商订单信息",
        id: "订货商订单信息",
        type:4,
        columns:
            [
                {
                    name: "订单编号",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "订单地址",
                    data_type: 3
                },
                {
                    name: "订单姓名",
                    data_type: 3
                },
                {
                    name: "订单日期",
                    data_type: 3
                },
                {
                    name: "到货日期",
                    data_type: 3
                },
            ],

        params:
            [
                {
                    name: "订单编号",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "订单姓名",
                    data_type: 3
                },

            ],

    },
    {
        name: "订单详情",
        id: "订单详情",
        type:4,
        columns: [
            {
                name: "订单编号",                           //这些name：可以是中文？
                data_type: 3
            },
            {
                name: "商品编码",
                data_type: 3
            }, {
                name: "商品名称",
                data_type: 3

            }, {
                name: "定价",
                data_type: 2
            }, {
                name: "销售数据量",
                data_type: 3

            }, {
                name: "付款金额",
                data_type: 2
            },{
                name:"销售时间",
                data_type: 1
            }

        ],
        params:
            [
                {
                    name: "订单编号",
                    data_type: 3
                },
            ],
    }
]
import {deepCopy} from "@/utils/xzToolsUtil.js";
export const get_onUpdateDatasetlist= ()=>{

    return deepCopy(onUpdateDatasetlist);//深拷贝
}