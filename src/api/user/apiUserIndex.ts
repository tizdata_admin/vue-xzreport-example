//统一管理用户相关的接口
import request from "../../utils/request";
import type {loginForm, loginResponseData, userResponseData} from "./apiUserType";

enum API {
    LOGIN_URL = "/user/login",
    USERINFO_URL = "/user/info",
    LOGOUT_URL = '/user/logout',
}

const reqLogin = (data: loginForm) => request.post<any, loginResponseData>(API.LOGIN_URL, data);
const reqUserInfo = () => {
    console.log('in apiUserIndex.ts reqUserInfo,before request.get...,: ');
    request.get<any, userResponseData>(API.USERINFO_URL);
    console.log('in apiUserIndex.ts reqUserInfo,after request.get...,: ');

}
const reqLogout = () => {
    request.post(API.LOGOUT_URL);
}
//暴露请求函数
export {reqLogin, reqUserInfo, reqLogout}



