export const reportTemplate_cross_mul_column = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{
            "row": 0,
            "type": 6,
            "slash": {"text": "商品|公司", "type": 0, "lineColor": "#000", "lineStyle": "thin"},
            "value": "",
            "column": 0,
            "rowSpan": 2,
            "editable": true,
            "columnSpan": 1,
            "dataConfig": {},
            "styleIndex": 1
        }, {
            "row": 0,
            "type": 2,
            "value": "test.ProductName",
            "column": 1,
            "rowSpan": 1,
            "dataType": "3",
            "editable": false,
            "expansion": 2,
            "scaleType": 1,
            "valueType": 1,
            "columnSpan": 2,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.ProductName", "axisType": 1, "data_type": "3"},
            "styleIndex": 1
        }, {"row": 0, "column": 2, "expansion": 2, "styleIndex": 3}, {
            "row": 1,
            "slash": {"text": "商品|公司", "type": 0, "lineColor": "#000", "lineStyle": "thin"},
            "value": "",
            "column": 0,
            "editable": true,
            "dataConfig": {},
            "styleIndex": 3
        }, {"row": 1, "type": 6, "value": "数量", "column": 1, "styleIndex": 4}, {
            "row": 1,
            "type": 6,
            "value": "单价",
            "column": 2,
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.CompanyName",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.CompanyName", "axisType": 1, "data_type": "3"},
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.Amount",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.Amount", "axisType": 1, "data_type": "2"},
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.Quantity",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.Quantity", "axisType": 1, "data_type": "2"},
            "styleIndex": 4
        }],
        "columnHead": [{"Column": 0}, {"Width": 42, "Column": 1}, {"Width": 87, "Column": 2}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 2,
            "eri": 2,
            "sci": 0,
            "sri": 0,
            "label": "A1-C3",
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(115, 234, 255,0.5)",
            "children": [{
                "eci": 2,
                "eri": 2,
                "sci": 1,
                "sri": 0,
                "label": "B1-C3",
                "bgColor": "rgba(255, 115, 191,0.5)",
                "borderColor": "rgba(255, 115, 191)"
            }],
            "borderColor": "rgba(115, 234, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center", "valign": "middle"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            },
            "valign": "middle"
        }, {
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }, {"align": "center"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }],
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}];

export const sheetConfig_cross_mul_column = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{
            "row": 0,
            "type": 6,
            "slash": {"text": "商品|公司", "type": 0, "lineColor": "#000", "lineStyle": "thin"},
            "value": "",
            "column": 0,
            "rowSpan": 2,
            "editable": true,
            "columnSpan": 1,
            "dataConfig": {},
            "styleIndex": 1
        }, {
            "row": 0,
            "type": 2,
            "value": "test.ProductName",
            "column": 1,
            "rowSpan": 1,
            "dataType": "3",
            "editable": false,
            "expansion": 2,
            "scaleType": 1,
            "valueType": 1,
            "columnSpan": 2,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.ProductName", "axisType": 1, "data_type": "3"},
            "styleIndex": 1
        }, {"row": 0, "column": 2, "expansion": 2, "styleIndex": 3}, {
            "row": 1,
            "slash": {"text": "商品|公司", "type": 0, "lineColor": "#000", "lineStyle": "thin"},
            "value": "",
            "column": 0,
            "editable": true,
            "dataConfig": {},
            "styleIndex": 3
        }, {"row": 1, "type": 6, "value": "数量", "column": 1, "styleIndex": 4}, {
            "row": 1,
            "type": 6,
            "value": "单价",
            "column": 2,
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.CompanyName",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.CompanyName", "axisType": 1, "data_type": "3"},
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.Amount",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.Amount", "axisType": 1, "data_type": "2"},
            "styleIndex": 4
        }, {
            "row": 2,
            "type": 2,
            "value": "test.Quantity",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.Quantity", "axisType": 1, "data_type": "2"},
            "styleIndex": 4
        }],
        "columnHead": [{"Column": 0}, {"Width": 42, "Column": 1}, {"Width": 87, "Column": 2}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 2,
            "eri": 2,
            "sci": 0,
            "sri": 0,
            "label": "A1-C3",
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(115, 234, 255,0.5)",
            "children": [{
                "eci": 2,
                "eri": 2,
                "sci": 1,
                "sri": 0,
                "label": "B1-C3",
                "bgColor": "rgba(255, 115, 191,0.5)",
                "borderColor": "rgba(255, 115, 191)"
            }],
            "borderColor": "rgba(115, 234, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center", "valign": "middle"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            },
            "valign": "middle"
        }, {
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }, {"align": "center"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }],
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}];

export const response_data_cross_mul_column =
    [{
        "City": "北京",
        "Amount": 144,
        "Quantity": 8,
        "ProductID": 1,
        "CompanyName": "仪和贸易",
        "ProductName": "苹果汁"
    }, {
        "City": "北京",
        "Amount": 1000,
        "Quantity": 40,
        "ProductID": 6,
        "CompanyName": "兰格英语",
        "ProductName": "酱油"
    }, {
        "City": "北京",
        "Amount": 144,
        "Quantity": 10,
        "ProductID": 1,
        "CompanyName": "留学服务中心",
        "ProductName": "苹果汁"
    }, {
        "City": "北京",
        "Amount": 660,
        "Quantity": 30,
        "ProductID": 4,
        "CompanyName": "留学服务中心",
        "ProductName": "盐"
    }, {
        "City": "北京",
        "Amount": 132,
        "Quantity": 6,
        "ProductID": 4,
        "CompanyName": "留学服务中心",
        "ProductName": "盐"
    }, {
        "City": "常州",
        "Amount": 54,
        "Quantity": 3,
        "ProductID": 1,
        "CompanyName": "迈策船舶",
        "ProductName": "苹果汁"
    }, {
        "City": "常州",
        "Amount": 228,
        "Quantity": 12,
        "ProductID": 2,
        "CompanyName": "椅天文化事业",
        "ProductName": "牛奶"
    }, {
        "City": "常州",
        "Amount": 608,
        "Quantity": 40,
        "ProductID": 2,
        "CompanyName": "椅天文化事业",
        "ProductName": "牛奶"
    }, {
        "City": "常州",
        "Amount": 616,
        "Quantity": 35,
        "ProductID": 4,
        "CompanyName": "椅天文化事业",
        "ProductName": "盐"
    }, {
        "City": "成都",
        "Amount": 72,
        "Quantity": 4,
        "ProductID": 1,
        "CompanyName": "新巨企业",
        "ProductName": "苹果汁"
    }, {
        "City": "大连",
        "Amount": 360,
        "Quantity": 20,
        "ProductID": 1,
        "CompanyName": "华科",
        "ProductName": "苹果汁"
    }, {
        "City": "大连",
        "Amount": 176,
        "Quantity": 10,
        "ProductID": 4,
        "CompanyName": "华科",
        "ProductName": "盐"
    }, {
        "City": "大连",
        "Amount": 1750,
        "Quantity": 70,
        "ProductID": 6,
        "CompanyName": "华科",
        "ProductName": "酱油"
    }, {
        "City": "海口",
        "Amount": 900,
        "Quantity": 50,
        "ProductID": 1,
        "CompanyName": "保信人寿",
        "ProductName": "苹果汁"
    }, {
        "City": "海口",
        "Amount": 72,
        "Quantity": 4,
        "ProductID": 1,
        "CompanyName": "保信人寿",
        "ProductName": "苹果汁"
    }, {
        "City": "海口",
        "Amount": 304,
        "Quantity": 20,
        "ProductID": 2,
        "CompanyName": "保信人寿",
        "ProductName": "牛奶"
    }, {
        "City": "海口",
        "Amount": 400,
        "Quantity": 50,
        "ProductID": 3,
        "CompanyName": "保信人寿",
        "ProductName": "蕃茄酱"
    }, {
        "City": "海口",
        "Amount": 160,
        "Quantity": 20,
        "ProductID": 3,
        "CompanyName": "保信人寿",
        "ProductName": "蕃茄酱"
    }, {
        "City": "海口",
        "Amount": 85.4,
        "Quantity": 4,
        "ProductID": 5,
        "CompanyName": "上河工业",
        "ProductName": "麻油"
    }, {
        "City": "南京",
        "Amount": 630,
        "Quantity": 35,
        "ProductID": 1,
        "CompanyName": "通恒机械",
        "ProductName": "苹果汁"
    }, {
        "City": "南京",
        "Amount": 399,
        "Quantity": 21,
        "ProductID": 2,
        "CompanyName": "通恒机械",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 300,
        "Quantity": 30,
        "ProductID": 3,
        "CompanyName": "通恒机械",
        "ProductName": "蕃茄酱"
    }, {
        "City": "南京",
        "Amount": 264,
        "Quantity": 12,
        "ProductID": 4,
        "CompanyName": "通恒机械",
        "ProductName": "盐"
    }, {
        "City": "南京",
        "Amount": 380,
        "Quantity": 25,
        "ProductID": 2,
        "CompanyName": "五洲信托",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 532,
        "Quantity": 35,
        "ProductID": 2,
        "CompanyName": "五洲信托",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 95,
        "Quantity": 5,
        "ProductID": 2,
        "CompanyName": "五洲信托",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 1100,
        "Quantity": 50,
        "ProductID": 4,
        "CompanyName": "五洲信托",
        "ProductName": "盐"
    }, {
        "City": "南京",
        "Amount": 1800,
        "Quantity": 60,
        "ProductID": 7,
        "CompanyName": "五洲信托",
        "ProductName": "海鲜粉"
    }, {
        "City": "南京",
        "Amount": 720,
        "Quantity": 40,
        "ProductID": 1,
        "CompanyName": "幸义房屋",
        "ProductName": "苹果汁"
    }, {
        "City": "南京",
        "Amount": 720,
        "Quantity": 40,
        "ProductID": 1,
        "CompanyName": "幸义房屋",
        "ProductName": "苹果汁"
    }, {
        "City": "南京",
        "Amount": 380,
        "Quantity": 20,
        "ProductID": 2,
        "CompanyName": "幸义房屋",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 500,
        "Quantity": 20,
        "ProductID": 6,
        "CompanyName": "幸义房屋",
        "ProductName": "酱油"
    }, {
        "City": "南京",
        "Amount": 190,
        "Quantity": 10,
        "ProductID": 2,
        "CompanyName": "永大企业",
        "ProductName": "牛奶"
    }, {
        "City": "南京",
        "Amount": 304,
        "Quantity": 20,
        "ProductID": 2,
        "CompanyName": "永大企业",
        "ProductName": "牛奶"
    }, {
        "City": "秦皇岛",
        "Amount": 364.8,
        "Quantity": 24,
        "ProductID": 2,
        "CompanyName": "友恒信托",
        "ProductName": "牛奶"
    }, {
        "City": "秦皇岛",
        "Amount": 427,
        "Quantity": 20,
        "ProductID": 5,
        "CompanyName": "利合材料",
        "ProductName": "麻油"
    }, {
        "City": "秦皇岛",
        "Amount": 180,
        "Quantity": 6,
        "ProductID": 7,
        "CompanyName": "利合材料",
        "ProductName": "海鲜粉"
    }, {
        "City": "秦皇岛",
        "Amount": 380,
        "Quantity": 20,
        "ProductID": 2,
        "CompanyName": "千固",
        "ProductName": "牛奶"
    }, {
        "City": "秦皇岛",
        "Amount": 300,
        "Quantity": 10,
        "ProductID": 7,
        "CompanyName": "千固",
        "ProductName": "海鲜粉"
    }, {
        "City": "秦皇岛",
        "Amount": 384,
        "Quantity": 16,
        "ProductID": 7,
        "CompanyName": "千固",
        "ProductName": "海鲜粉"
    }, {
        "City": "上海",
        "Amount": 190,
        "Quantity": 10,
        "ProductID": 2,
        "CompanyName": "业兴",
        "ProductName": "牛奶"
    }, {
        "City": "上海",
        "Amount": 750,
        "Quantity": 30,
        "ProductID": 6,
        "CompanyName": "业兴",
        "ProductName": "酱油"
    }, {
        "City": "上海",
        "Amount": 90,
        "Quantity": 3,
        "ProductID": 7,
        "CompanyName": "业兴",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 285,
        "Quantity": 15,
        "ProductID": 2,
        "CompanyName": "国顶有限公司",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 240,
        "Quantity": 30,
        "ProductID": 3,
        "CompanyName": "光明杂志",
        "ProductName": "蕃茄酱"
    }, {
        "City": "深圳",
        "Amount": 120,
        "Quantity": 4,
        "ProductID": 7,
        "CompanyName": "光明杂志",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 720,
        "Quantity": 30,
        "ProductID": 7,
        "CompanyName": "光明杂志",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 760,
        "Quantity": 50,
        "ProductID": 2,
        "CompanyName": "正人资源",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 152,
        "Quantity": 8,
        "ProductID": 2,
        "CompanyName": "正人资源",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 200,
        "Quantity": 20,
        "ProductID": 3,
        "CompanyName": "正人资源",
        "ProductName": "蕃茄酱"
    }, {
        "City": "深圳",
        "Amount": 250,
        "Quantity": 25,
        "ProductID": 3,
        "CompanyName": "正人资源",
        "ProductName": "蕃茄酱"
    }, {
        "City": "深圳",
        "Amount": 544,
        "Quantity": 32,
        "ProductID": 5,
        "CompanyName": "正人资源",
        "ProductName": "麻油"
    }, {
        "City": "深圳",
        "Amount": 1105,
        "Quantity": 65,
        "ProductID": 5,
        "CompanyName": "正人资源",
        "ProductName": "麻油"
    }, {
        "City": "深圳",
        "Amount": 540,
        "Quantity": 18,
        "ProductID": 7,
        "CompanyName": "正人资源",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 360,
        "Quantity": 20,
        "ProductID": 1,
        "CompanyName": "远东开发",
        "ProductName": "苹果汁"
    }, {
        "City": "深圳",
        "Amount": 950,
        "Quantity": 50,
        "ProductID": 2,
        "CompanyName": "远东开发",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 190,
        "Quantity": 10,
        "ProductID": 2,
        "CompanyName": "阳林",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 110,
        "Quantity": 5,
        "ProductID": 4,
        "CompanyName": "阳林",
        "ProductName": "盐"
    }, {
        "City": "深圳",
        "Amount": 220,
        "Quantity": 10,
        "ProductID": 4,
        "CompanyName": "阳林",
        "ProductName": "盐"
    }, {
        "City": "深圳",
        "Amount": 320.25,
        "Quantity": 15,
        "ProductID": 5,
        "CompanyName": "阳林",
        "ProductName": "麻油"
    }, {
        "City": "深圳",
        "Amount": 760,
        "Quantity": 40,
        "ProductID": 2,
        "CompanyName": "一诠精密工业",
        "ProductName": "牛奶"
    }, {
        "City": "深圳",
        "Amount": 1050,
        "Quantity": 35,
        "ProductID": 7,
        "CompanyName": "一诠精密工业",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 600,
        "Quantity": 20,
        "ProductID": 7,
        "CompanyName": "一诠精密工业",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 240,
        "Quantity": 10,
        "ProductID": 7,
        "CompanyName": "昇昕股份有限公司",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 140,
        "Quantity": 14,
        "ProductID": 3,
        "CompanyName": "中硕贸易",
        "ProductName": "蕃茄酱"
    }, {
        "City": "深圳",
        "Amount": 300,
        "Quantity": 10,
        "ProductID": 7,
        "CompanyName": "中硕贸易",
        "ProductName": "海鲜粉"
    }, {
        "City": "深圳",
        "Amount": 378,
        "Quantity": 21,
        "ProductID": 1,
        "CompanyName": "凯诚国际顾问公司",
        "ProductName": "苹果汁"
    }, {
        "City": "深圳",
        "Amount": 360,
        "Quantity": 20,
        "ProductID": 1,
        "CompanyName": "凯诚国际顾问公司",
        "ProductName": "苹果汁"
    }]
export const sortInGroup_formula_cross_mul_column = "=COUNT(C2[!0]{A2 = $A2 && C2 > $C2}) + 1";
export const data_cross_mul_column = [
    {
        "type": 4,
        "name": "test",
        "response_data": response_data_cross_mul_column
    }
]
export const option_cross_mul_column = {
    mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
    view: {
        width: () => document.documentElement.clientWidth,
        height: () => document.documentElement.clientHeight
    },   // 设置报表的宽高
    renderArrow: false,  // 是否显式 扩展方向图标
    showFreeze: false,  // 是否显式冻结线
    showGrid: false   // 是否显式网格线
};

export const reportPrintOptions_cross_mul_column = [
    {
        name: "交叉表多数据列",
        //print: sheetConfig
    }
]

