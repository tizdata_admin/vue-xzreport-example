function createUserList() {
    return [
        {
            userId: 1,
            username: 'admin',
            password: '111111',
            token: 'Admin Token',
            avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            desc: '平台管理员',
            roles: ['平台管理员'],
            buttons: ['cuser.detail'],
            routes: ['home'],

        },
        {
            userId: 2,
            username: 'system',
            password: '111111',
            token: 'System Token',
            avatar:
                'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            desc: '系统管理员',
            roles: ['系统管理员'],
            buttons: ['cuser.detail', 'cuser.user'],
            routes: ['home'],
        }
    ]
}

export default [{
    url: '/api/user/login',
    method: 'POST',
    response: ({body}) => {
        const {username, password} = body;
        const checkUser = createUserList().find(user => user.username === username && user.password === password);
        if (!checkUser) {
            return {code: 201, data: {message: '账号或者密码不正确'}}
        }
        const {token} = checkUser;
        console.log("in mock.userStore.ts,token:", token)
        return {code: 200, data: {token: token}};
    }
}, {
    url: '/api/user/info',
    method: 'get',
    response: (request) => {
        //response:({request})=>{......会报错，必须改成 response:(request)=>{.....
        //console.log("userMock.js  ,url=/api/user/info, request:", request);
        const token = request.headers.token;
        const checkUser = createUserList().find(user => user.token === token);
        if (!checkUser) {
            return {
                code: 200,
                data: {
                    message: '获取用户信息失败',
                }
            }

        }
        return {
            code: 200,
            data: {checkUser}
        }
    }
}]