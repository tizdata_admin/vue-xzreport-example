# xzChartDesigner API(小智BI设计器)
## 设计器需要： 数据集(表结构，查询参数)，以及数据集对应的数据；
## 输入
- option选项配置
```shell
let option = { //参数配置说明 根据您的需求填写option
  uploadImage: { //上传图片相关信息 如果不传该字段时，设计器用到的所有上传图片配置项均不显示。
    server: "url",//上传图片地址
    method: "post",//上传图片请求方式
    headers: { //上传图片请求头
      "Authorization": "Token XXXXXXXX"
    },
    fieldName: "file",
    resultField: "file"
  },
  pageData: { //画布初始化时的宽高
    width: '1920px',
    height: '1080px'
  },
  dataSet: [ //数据集按钮的方式
    { //Json数据
      id: 1,
      value: "新建Json数据集"
    },
    { //http请求数据
      id: 2,
      value: "新建Http接口数据集"
    },
    { //API服务数据
      id: 3,
      value: "API服务数据集"
    },
    {
      id: 4,
      value: "外部数据集"  //  新建一个新的报表实例，如果希望可以预先动态从后台提供几个数据集供其使用，这个配置必须有；
      // 然后只要动态的从后台拿到n个数据集，全部用 results格式装配，onUpdateDatasetList方法来触发
    }
  ]

}
```
- 数据集类型（提供表结构的方式：目前4种）
```shell
const dataset= [ 
    { //Json数据
        id: 1,
        value: "新建Json数据集"     
    },
    { //http请求数据
        id: 2,
        value: "新建Http接口数据集"
    },
    { //API服务数据
        id: 3,
        value: "API服务数据集"
    },
    {
        id: 4,
        value: "外部数据集"  
    }
];
option.dataSet=dataset    //注意大小写
```
- 准备外部数据集(表结构，是数组,一次可以提供多个表结构)
```shell
let results = [
    {
        name:"商品销售",
        id:"商品销售id",
        columns:[
                    {name: "商品名称", data_type: 3 },
                    { name: "区域名称",data_type: 3 },
                    {name: "销售额",data_type: 2},
                ],
        params:[
                    {name: "商品名称", data_type: 3},
                    {name: "区域名称",data_type: 3},
                ],  
        response_data:[],
    },
    .....
]
```
- 实现方法 onUpdateDatasetList
```shell
const onUpdateDatasetList = () => {
  return new Promise((resolve, reject) => {    //异步方法
          resolve(results)
          })
}
option.onUpdateDatasetList = onUpdateDatasetList;
```
- 实例构建
```shell
  sheetDesign = XZReportDesigner("#container", option);
  sheetDesign.loadData({config: config, data: data});

```
## 输出  sheetDesign.getData()
```shell
let sheetDesign = XZReportDesigner("#container", option);
... ...
let {config,data} =sheetDesign.getData()
.....
axios.post(url, data, [config]) 将config,data 发到后台,保存入库
```
# 样例参考 /src/views/reportDesigner/XzReportDesignerEdit5.vue
## 报表模板数据config,data两个json，以mock的方式，模拟保存在后台，
## 演示了‘加载一个已经存在的报表模板’,可以继续修改
