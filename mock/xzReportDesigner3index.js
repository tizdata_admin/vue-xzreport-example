import {results as results3} from "./xzReportDesignerDemoDataJson/xzReportDesigner3/results.js";
import {results as results5} from "./xzReportDesignerDemoDataJson/xzReportDesigner5/results.js";
import {config as config5} from "./xzReportDesignerDemoDataJson/xzReportDesigner5/config.js";
import {data as data5} from "./xzReportDesignerDemoDataJson/xzReportDesigner5/data.js";
import {onupdataOutsourceEcho} from "./xzReportDemoDataJson/xzreportdesignertabs4/onupdate_echo.js"
import {
    data as reportDesigner3_instance_data_echo
} from "./xzReportDesignerDemoDataJson/xzReportDesigner3/xzReportDesigner3_Instance_data_echo.js";

function getReportDesigner3Datas() {
    console.log("getReportDesigner3Datas:", results3);
    return {
        results3
    }

}

function getReportDesigner3_instance_data_echo() {
    console.log("getReportDesigner3_instance_data_echo", reportDesigner3_instance_data_echo);
    return {
        reportDesigner3_instance_data_echo
    }
}

function getReportDesigner5ConfigAndData() {
    console.log("getReportDesigner5ConfigAndData:", config5, data5);
    return {
        config5, data5
    }
}

function getReportDesigner5Outdatasets() {
    console.log("getReportDesigner5Outdatasets:", results5);
    return {
        results5
    }
}

function  getReport4OnupdataOutsourceEcho(){
    return {
        onupdataOutsourceEcho
    }
}


import {data as xzReport1_data} from "./xzReportDemoDataJson/xzReport1/data_1";
import {sheetConfig as xzReport1_sheetConfig} from "./xzReportDemoDataJson/xzReport1/sheet_config1";

function getXzReport1Datas() {
    return {
        xzReport1_data, xzReport1_sheetConfig
    }
}

export default [{
    url: '/api/xzreportdesigner/report3',
    method: 'post',
    response: ({body}) => {
        let results = getReportDesigner3Datas().results3;
        return {
            code: 200, data: {
                results
            }
        }

    }
}, {
    url: '/api/xzreportdesigner/echo_report3/instanceDataEcho',//根据现场报表设计器3的表结构，做一个报表，然后用这个数据来实现回显,
    method: 'post',
    response: ({body}) => {
        let reportDesigner3_instance_data_echo = getReportDesigner3_instance_data_echo().reportDesigner3_instance_data_echo;
        return {
            data:reportDesigner3_instance_data_echo
        }
    }
}, {
    url: '/api/xzreportdesigner/report5/configanddata',
    method: 'post',
    response: ({body}) => {

        let rtn = getReportDesigner5ConfigAndData();

        return {
            code: 200, data: {
                config: rtn.config5, data: rtn.data5
            }
        }

    }
}, {
    //获取报表设计器5的外部数据集列表
    url: '/api/xzreportdesigner/report5/outdatasets',
    method: 'post',
    response: ({body}) => {

        let rtn = getReportDesigner5Outdatasets();
        return {
            code: 200, data: {
                results: rtn.results5
            }
        }

    }
},
    {
        url: '/api/xzreport/report1',
        method: 'post',
        response: ({body}) => {
            console.log("/api/xzreport/report1 is called!");
            let rtn = getXzReport1Datas();

            return {
                code: 200, data: {
                    sheetConfig: rtn.xzReport1_sheetConfig,
                    data: rtn.xzReport1_data
                }
            }

        }
    },
    {
        url: '/api/xzreportdesigner/report4',
        method: 'post',
        response: ({body}) => {
            let rtn = getReport4OnupdataOutsourceEcho().onupdataOutsourceEcho;
          //  console.log("/api/xzreportdesigner/report4 报表设计器->回显->onupdate is called!,",rtn);

            return {
                code: 200, data: {
                    data: rtn
                }
            }

        }
    }]