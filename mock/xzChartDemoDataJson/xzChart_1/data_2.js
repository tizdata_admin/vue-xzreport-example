// 现在报表里数据配置 type 1 json数据
// json 数据 在使用 updateData方法更新
// 一个完整的数据集配置；  下一步，添加带参数的
import {response_data_rtn as  xzchart_1_response_dat_rtn} from "./response_data_rtn_1.js";
let data = [ //等同于 4.1设计器保存时调用方法返回数据中的 data.dataConfig字段
    {
        name:"数据表名称",
        id:"数据表id",
        type: 1, // 1 Json数据 3 API数据
        response_data: xzchart_1_response_dat_rtn
    }
]
export {data} // vue3 在纯html嵌入脚本中，必须注释掉