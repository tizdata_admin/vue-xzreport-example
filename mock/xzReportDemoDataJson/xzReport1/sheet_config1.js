const sheetConfig = [{
    "name": "sheet1",
    "SheetData": {
        "freeze": "A1",
        "columnHead": [{
            "Column": 0
        }, {
            "Column": 1
        }, {
            "Column": 2
        }],
        "rowHead": [{
            "Row": 0
        }, {
            "Row": 1
        }, {
            "Row": 2
        }],
        "dataConfig":[],
        "apiConfig":[],
        "paramsConfig":[],
        "stylesConfig": [{
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "valign": "middle"
        }, {
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "align": "center"
        }, {
            "align": "center"
        }, {
            "align": "center",
            "valign": "middle",
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            }
        }, {
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "align": "left"
        }, {
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "align": "center",
            "bgcolor": "#01B0F1"
        }, {
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "align": "center",
            "bgcolor": "#93D051"
        }, {
            "border": {
                "bottom": ["thin", "#666"],
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"]
            },
            "align": "center",
            "bgcolor": "#FFC001"
        }],
        "cellConfig": [{
            "column": 0,
            "row": 0,
            "value": "",
            "styleIndex": 5,
            "editable": true,
            "slash": {
                "text": "商品|季度",
                "type": 0,
                "lineStyle": "thin",
                "lineColor": "#000"
            },
            "dataConfig": {}
        }, {
            "column": 1,
            "row": 0,
            "value": "test.商品",
            "valueType": 1,
            "expansion": 2,
            "styleIndex": 2,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {
                "value": "test.商品",
                "data_type": "3",
                "merge": 1,
                "axisType": 1,
                "order": 0
            },
            "type": 2,
            "dataType": "3"
        }, {
            "column": 2,
            "row": 0,
            "value": "合计",
            "styleIndex": 6,
            "type": 6
        }, {
            "column": 0,
            "row": 1,
            "value": "test.季度",
            "valueType": 1,
            "expansion": 1,
            "styleIndex": 2,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {
                "value": "test.季度",
                "data_type": "2",
                "axisType": 1,
                "order": 0,
                "merge": 1
            },
            "type": 2,
            "dataType": "2"
        }, {
            "column": 1,
            "row": 1,
            "value": "test.销售额",
            "valueType": 1,
            "expansion": 3,
            "styleIndex": 2,
            "editable": false,
            "scaleType": 1,
            "dataConfig": {
                "value": "test.销售额",
                "data_type": "2",
                "aggregate": 1,
                "axisType": 2,
                "order": 0
            },
            "type": 2,
            "dataType": "2"
        }, {
            "column": 2,
            "row": 1,
            "value": "=SUM(B2)",
            "styleIndex": 6,
            "type": 5
        }, {
            "column": 0,
            "row": 2,
            "value": "合计",
            "styleIndex": 7,
            "type": 6
        }, {
            "column": 1,
            "row": 2,
            "value": "=SUM(B2)",
            "styleIndex": 7,
            "warn": [{
                "name": "条件1",
                "act_on": 1,
                "cond_str": "((字段：test.商品) 不为空 )",
                "filter": {
                    "logic": 1,
                    "params": [{
                        "logic": 1,
                        "params": [{
                            "column": "0:1",
                            "data_type": 3,
                            "logic": 1,
                            "type": 8,
                            "value": "",
                            "cname": "test.商品",
                            "is_req": false
                        }]
                    }]
                },
                "style_str": "小数位：2位； ",
                "stylesConfig": {
                    "format": {
                        "decimal": 2
                    }
                }
            }],
            "type": 5
        }, {
            "column": 2,
            "row": 2,
            "value": "=SUM(B2)",
            "styleIndex": 8,
            "warn": [{
                "name": "条件1",
                "act_on": 1,
                "cond_str": "((字段：test.商品) 不为空 )",
                "filter": {
                    "logic": 1,
                    "params": [{
                        "logic": 1,
                        "params": [{
                            "column": "0:1",
                            "data_type": 3,
                            "logic": 1,
                            "type": 8,
                            "value": "",
                            "cname": "test.商品",
                            "is_req": false
                        }]
                    }]
                },
                "style_str": "小数位：2位； ",
                "stylesConfig": {
                    "format": {
                        "decimal": 2
                    }
                }
            }],
            "type": 5
        }],
        "groupConfig": [{
            "label": "A1-C3",
            "children": [{
                "label": "A2-C3",
                "children": [{
                    "label": "A2-B3",
                    "borderColor": "rgba(255, 115, 191)",
                    "bgColor": "rgba(255, 115, 191,0.5)",
                    "noagg": false,
                    "no_agg": false,
                    "repeat": {
                        "isRepeat": false,
                        "repeatKey": "",
                        "repeatValue": ""
                    },
                    "position": "A2:B3"
                }],
                "borderColor": "rgba(115, 187, 255)",
                "bgColor": "rgba(115, 187, 255,0.5)",
                "noagg": false,
                "no_agg": false,
                "repeat": {
                    "isRepeat": false,
                    "repeatKey": "",
                    "repeatValue": ""
                },
                "position": "A2:C3"
            }],
            "borderColor": "rgba(94, 255, 215)",
            "bgColor": "rgba(94, 255, 215,0.5)",
            "noagg": false,
            "no_agg": false,
            "repeat": {
                "isRepeat": false,
                "repeatKey": "",
                "repeatValue": ""
            },
            "position": "A1:C3"
        }],
        "collapse": [],
        "toolbarButtonList": [{
            "showIcon": true,
            "showText": true,
            "text": "首页",
            "icon": "fa fa-step-backward",
            "color": "#2c3e50",
            "show": true,
            "type": 1
        }, {
            "showIcon": true,
            "showText": true,
            "text": "上一页",
            "icon": "xzreport-icon-arrow-left-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 2
        }, {
            "showIcon": true,
            "showText": true,
            "text": "当前页/总页数",
            "icon": "",
            "color": "#2c3e50",
            "show": true,
            "type": 3
        }, {
            "showIcon": true,
            "showText": true,
            "text": "下一页",
            "icon": "xzreport-icon-arrow-right-filling",
            "color": "#2c3e50",
            "show": true,
            "type": 4
        }, {
            "showIcon": true,
            "showText": true,
            "text": "末页",
            "icon": "fa fa-step-forward",
            "color": "#2c3e50",
            "show": true,
            "type": 5
        }, {
            "showIcon": true,
            "showText": true,
            "text": "打印",
            "color": "#2c3e50",
            "icon": "fa fa-print",
            "show": true,
            "type": 6
        }, {
            "showIcon": true,
            "showText": true,
            "text": "导出",
            "icon": "fa fa-download",
            "color": "#2c3e50",
            "show": true,
            "type": 7
        }]
    }
}]
export {sheetConfig}