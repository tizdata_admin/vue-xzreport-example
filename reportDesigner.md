# xzReportDesigner API(小智报表设计器)
## 输入
- option选项配置
```shell
const option = {
    row: 100,
    col: 25,
     width:()=>1600,
    // height:()=>height
  renderArrow: true,  // 是否显式 扩展方向图标
  showFreeze: true,  // 是否显式冻结线
  showGrid: true,   // 是否显式网格线
  showPreviewToolbar:true,
  showBottombar:true,
  showSelect:true,    
    ......
}
```
- 数据集类型（提供表结构的方式：目前4种）
```shell
const dataset= [ 
    { //Json数据
        id: 1,
        value: "新建Json数据集"     
    },
    { //http请求数据
        id: 2,
        value: "新建Http接口数据集"
    },
    { //API服务数据
        id: 3,
        value: "API服务数据集"
    },
    {
        id: 4,
        value: "外部数据集"  
    }
];
option.dataset=dataset
```
- 准备外部数据集(表结构，是数组,一次可以提供多个表结构)
```shell
let results = [
    {
        name:"商品销售",
        id:"商品销售id",
        columns:[
                    {name: "商品名称", data_type: 3 },
                    { name: "区域名称",data_type: 3 },
                    {name: "销售额",data_type: 2},
                ],
        params:[
                    {name: "商品名称", data_type: 3},
                    {name: "区域名称",data_type: 3},
                ],  
        response_data:[],
    },
    .....
]
```
- 实现方法 onUpdateDatasetList
```shell
const onUpdateDatasetList = () => {
  return new Promise((resolve, reject) => {    
          resolve(results)
          })
}
option.onUpdateDatasetList = onUpdateDatasetList;
```
- 实例构建
```shell
  sheetDesign = XZReportDesigner("#container", option);
  sheetDesign.loadData({config: config, data: data});

```
## 输出  sheetDesign.getData()
```shell
let sheetDesign = XZReportDesigner("#container", option);
... ...
let {config,data} =sheetDesign.getData()
.....
axios.post(url, data, [config]) 将config,data 发到后台,保存入库
```
# 样例参考 /src/views/reportDesigner/XzReportDesignerEdit5.vue
## 报表模板数据config,data两个json，以mock的方式，模拟保存在后台，
## 演示了‘加载一个已经存在的报表模板’,可以继续修改
