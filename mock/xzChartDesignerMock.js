import {report as chartDesigner_2_report}               from "./xzChartDesignerDemoJson/xzChartDesigner_2/chartDesigner_lsyjkb.js";
import {results as chartDesigner_2_results}             from "./xzChartDesignerDemoJson/xzChartDesigner_2/results.js";
import {response_data as chartDesigner_2_response_data} from "./xzChartDesignerDemoJson/xzChartDesigner_2/response_data.js";
function getChartDesigner2_report(){
    return {
        report:chartDesigner_2_report
    }
}
function getChartDesigner2_results(){
    return {
        results: chartDesigner_2_results
    }
}
function getChartDesigner2_response_data(){
    return{
        response_data:chartDesigner_2_response_data
    }
}


export default [

    {  //----xzchartdesigner
        url: '/api/xzchartdesigner/report2/report',
        method: 'post',
        type:'xzchartdisigner',
        reportid:'report2',
        response: ({body}) => {
            let report = getChartDesigner2_report().report;
           // console.log('/api/xzchartdesigner/report2/report:report->:', report);

            return {
                code: 200, data: {
                    report
                }
            }
        }
    },
    {
        url: '/api/xzchartdesigner/report2/results',
        method: 'post',
        type:'xzchartdisigner',
        reportid:'report2',
        response: ({body}) => {
            let results = getChartDesigner2_results().results ;
            //console.log('/api/xzchartdesigner/report2/report:results>:', results);
            return{
                code: 200, data: {
                    results
                }
            }
        }
    },
    {
        url: '/api/xzchartdesigner/report2/response_data',
        method: 'post',
        type:'xzchartdisigner',
        reportid:'report2',
        response: ({body}) => {
            let response_data = getChartDesigner2_response_data().response_data ;
            //console.log('/api/xzchartdesigner/report2/report:results>:', results);
            return{
                code: 200, data: {
                    response_data
                }
            }
        }
    }
]