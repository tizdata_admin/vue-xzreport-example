//第三方外部数据集类型的具体的数据集
//import {response_data} from "./response_data.js";
let results = [
    {
        name:"商品销售",
        id:"商品销售id",
        columns:
            [
                {
                    name: "商品名称",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "区域名称",
                    data_type: 3
                },
                {
                    name: "销售额",
                    data_type: 2
                },
            ],

        params:
            [
                {
                    name: "商品名称",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "区域名称",
                    data_type: 3
                },


            ],  // 数据表中参数 参数耳朵数据结构 {name:"参数名称",data_type:"参数类型"} data_type => 1日期 2数字 3字符串;必须和上面的columns:匹配？
        response_data:[],//当数据集的类型是外部数据源，这个数据没有价值，
        //何时有用？ 只有添加json数据集类型的时候，粘贴的是具体是数据记录，在sheetDesign.getData()时候可以回显
        //每一条记录信息如何配置？#########################
    }
]

export  {results}