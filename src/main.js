import {createApp} from 'vue'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import App from './App.vue'
import 'virtual:svg-icons-register';

const app = createApp(App);
app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
import {GlobalComponentsPlugin}  from '@/components/MyComponent.js'
app.use(GlobalComponentsPlugin);
// import {createPinia} from "pinia";
// const pinia = createPinia()

import pinia from "./stores";
app.use(pinia);

import router from './router'
app.use(router);
import   '@/permisstions.ts'

import '@/styles/index.scss';
// import VueDevtools from 'vue-devtools';
// app.use(VueDevtools);
app.mount('#app')
