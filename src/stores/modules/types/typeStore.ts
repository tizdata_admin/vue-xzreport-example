import type{RouteRecordRaw} from "vue-router";

export interface UserState{
    token:string|'123456';
    menuRoutes:RouteRecordRaw[];
    username:string|null;
    avatar:string|null;

}
