import {reportTemplate} from "@/views/report/XzReport_sortInGroup_sheetConfig.js";

const reportTemplate = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}, {"Row": 3}, {"Row": 4}, {"Row": 5}, {"Row": 6}, {"Row": 7}, {"Row": 8}, {"Row": 9}, {"Row": 10}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "type": 6, "value": "客户编号", "column": 0, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "公司名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地址", "column": 2, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "城市",
            "column": 3,
            "styleIndex": 0
        }, {"row": 0, "type": 6, "value": "地区", "column": 4, "styleIndex": 0}, {
            "row": 0,
            "type": 6,
            "value": "电话",
            "column": 5,
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CustomerID",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CustomerID", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.CompanyName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.CompanyName", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Address",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Address", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.City",
            "column": 3,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.City", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Region",
            "column": 4,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Region", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {
            "row": 1,
            "type": 2,
            "value": "Customer.Phone",
            "column": 5,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 0, "order": 0, "value": "Customer.Phone", "axisType": 1, "data_type": "3"},
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "编号", "column": 0, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "名称",
            "column": 1,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "单价", "column": 2, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "库存数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "订购数量", "column": 4, "styleIndex": 0}, {
            "row": 3,
            "type": 6,
            "value": "订购级别",
            "column": 5,
            "styleIndex": 0
        }, {"row": 3, "type": 6, "value": "使用中", "column": 6, "styleIndex": 0}, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductID",
            "column": 0,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ProductName",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitPrice",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitPrice",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsInStock",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsInStock",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.UnitsOnOrder",
            "column": 4,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.UnitsOnOrder",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.ReorderLevel",
            "column": 5,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.ReorderLevel",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 4,
            "type": 2,
            "value": "Product.Discontinued",
            "column": 6,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Product.Discontinued",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 5,
            "type": 6,
            "value": "最有价值客户",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 5, "column": 1, "styleIndex": 7}, {"row": 5, "column": 2, "styleIndex": 7}, {
            "row": 5,
            "column": 3,
            "styleIndex": 7
        }, {"row": 6, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "客户编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 6, "type": 6, "value": "公司名称", "column": 2, "styleIndex": 0}, {
            "row": 6,
            "type": 6,
            "value": "销售总金额",
            "column": 3,
            "styleIndex": 0
        }, {"row": 7, "type": 5, "value": "=ROW()-17", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CustomerID",
            "column": 1,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CustomerID",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.CompanyName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.CompanyName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 7,
            "type": 2,
            "value": "Top10Customer.SumAmt",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 1,
                "order": 0,
                "value": "Top10Customer.SumAmt",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 8,
            "type": 6,
            "value": "最畅销产品",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 4,
            "styleIndex": 6
        }, {"row": 8, "column": 1, "styleIndex": 7}, {"row": 8, "column": 2, "styleIndex": 7}, {
            "row": 8,
            "column": 3,
            "styleIndex": 7
        }, {"row": 9, "type": 6, "value": "序号", "column": 0, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "产品编号",
            "column": 1,
            "styleIndex": 0
        }, {"row": 9, "type": 6, "value": "产品名称", "column": 2, "styleIndex": 0}, {
            "row": 9,
            "type": 6,
            "value": "销售数量",
            "column": 3,
            "styleIndex": 0
        }, {"row": 10, "type": 5, "value": "=ROW()-29", "column": 0, "expansion": 1, "styleIndex": 0}, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductID",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductID",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.ProductName",
            "column": 2,
            "dataType": "3",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "3",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.ProductName",
                "axisType": 1,
                "data_type": "3"
            },
            "styleIndex": 0
        }, {
            "row": 10,
            "type": 2,
            "value": "Top10Product.SumQty",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "wrapType": 1,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {
                "type": "2",
                "merge": 0,
                "order": 0,
                "value": "Top10Product.SumQty",
                "axisType": 1,
                "data_type": "2"
            },
            "styleIndex": 0
        }],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Column": 3}, {"Column": 4}, {"Column": 5}, {"Column": 6}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 7,
            "sci": 0,
            "sri": 6,
            "label": "A7-D8",
            "filter": null,
            "bgColor": "rgba(255, 212, 80,0.5)",
            "borderColor": "rgba(255, 212, 80)"
        }, {
            "eci": 3,
            "eri": 10,
            "sci": 0,
            "sri": 9,
            "label": "A10-D11",
            "filter": null,
            "bgColor": "rgba(115, 187, 255,0.5)",
            "borderColor": "rgba(115, 187, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{
            "border": {
                "top": ["thin", "#000"],
                "left": ["thin", "#000"],
                "right": ["thin", "#000"],
                "bottom": ["thin", "#000"]
            }
        }, {"align": "center", "valign": "middle"}, {
            "font": {"size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle"
        }, {"font": {"bold": true, "size": 15}}, {
            "font": {"bold": true, "size": 15},
            "align": "center",
            "valign": "middle",
            "bgcolor": "#BDD7EE"
        }, {"font": {"bold": true, "size": 15}, "bgcolor": "#BDD7EE"}],
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}]