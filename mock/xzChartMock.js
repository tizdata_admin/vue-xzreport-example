//---- xcChart.js
import {data as xzchart_1_data}         from "./xzChartDemoDataJson/xzChart_1/data_2.js";
import { config as xzchart_1_config}    from "./xzChartDemoDataJson/xzChart_1/reportConfig_3.js";
function getChart1Data(){
    return {
        data:xzchart_1_data,config:xzchart_1_config
    }
}

export default [

    {  //----xcchart
        url: '/api/xzchart/report1/configanddata',
        method: 'post',
        type:'xzchart',
        reportid:'report1',
        response: ({body}) => {
            console.log(".mock mock mock ....xzChartMock............................/api/xzchart/report1/configanddata........................")
            let data = getChart1Data().data;
            let config=getChart1Data().config
            console.log('/api/xzchart/report1/configanddata:data->:', data);
            console.log('/api/xzchart/report1/configanddata:config->:', config);
            return {
                code: 200, data: {
                    config, data
                }
            }
        }
    },

]