//import {get_onupdatedatamaps} from "@/views/chartDesigner/chartDesignerEltabs3/get_onUpdateDataMaps_ForEcho.js";
let isPrintinfo=true;
function deepClone(obj) {//好好测试一下；源对象内部嵌套了数组，map，到目标对象里，这些嵌套的数组/map 是否还保留了迭代，filter的特性方法？
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    if (obj instanceof Date) {
        return new Date(obj.getTime());
    }

    if (obj instanceof Array) {
        return obj.reduce((arr, item, i) => {
            arr[i] = deepClone(item);
            return arr;
        }, []);
    }

    if (obj instanceof Object) {
        return Object.keys(obj).reduce((newObj, key) => {
            newObj[key] = deepClone(obj[key]);
            return newObj;
        }, {});
    }
}

// 使用例子
// const originalArray = [1, 2, { a: 1, b: [1, 2, 3] }];
// const clonedArray = deepClone(originalArray);
//
// clonedArray[2].b.push(4);
// console.log(originalArray); // 不会被修改，依然是[1, 2, { a: 1, b: [1, 2, 3] }]
// console.log(clonedArray);   // 修改后的结果，是[1, 2, { a: 1, b: [1, 2, 3, 4] }]


export const deepCopy = (obj) => {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    if (obj instanceof Date) {
        return new Date(obj.getTime());
    }

    if (obj instanceof Array) {
        return obj.reduce((arr, item, i) => {
            arr[i] = deepCopy(item);
            return arr;
        }, []);
    }

    if (obj instanceof Object) {
        return Object.keys(obj).reduce((newObj, key) => {
            newObj[key] = deepCopy(obj[key]);
            return newObj;
        }, {});
    }
}
//输入：dataSetList 全部候选表结构   ，当前需要数据的表名，查询参数列表；因为不知道tableName中，到底否包含了params中几个参数（0，1，2.）
//输出：,从dataSetList全部表结构，拿到tableName的数据集信息，看该表tableName元数据信息是否包含了‘查询参数列表’，如果包括，，就把tablename的表结构信息+参数信息带出来
export const getParamsFrom_DataSet = (dataSetList, tableName, params) => {
    let _rtn = [];
    for (let i = 0; i < dataSetList.length; i++) {
        if (dataSetList[i].name === tableName && dataSetList[i].params.length > 0) {
            params.forEach((param, param_index) => {
                dataSetList[i].params.forEach((item, index) => {
                    if (param.name === item.name) {
                        _rtn.push(param);
                    }
                })
            })
        }
    }
    if(isPrintinfo)
    console.log(".......xzToolsUtil.js.getParamsFrom_dataSet:", dataSetList, "...tableName:", tableName, ",params:", params, ",tableName有几个参数出现在params中:", _rtn);
    return _rtn;
}
//datasets 查询数据集信息
// praams  查询参数信息
//datasetList全部的外部数据集（上传的，没有数据库）
//get_onUpdateDataMaps 全部的外部数据记录（上传的，没有数据库）
// 当前需要提取记录的表结构列表，当前发生的参数选择信息
export const filterDatasetsFrom_params = ((datasets, params, datasetList, get_onUpdateDataMaps, _condition = '_or_') => {
    let _ok_datasets = [];//假设根据参数从外部数据集查询到的结果是[]
    console.log("datasets.length:", datasets.length);
    console.log("params.length::", params.length);
    console.log("datasetList.length:", datasetList.length);
    console.log("get_onUpdateDataMaps().size:",get_onUpdateDataMaps().size)
    if(get_onUpdateDataMaps().size === 0)
        return _ok_datasets;//如果外部数据集记录本身为0，返回空记录
    if(params.length === 0){
        //如果有外部数据集记录，但是没有查询参数，那么直接返回外部数据集记录
        for(const value of get_onUpdateDataMaps().values()){
            _ok_datasets.push(value);
        }
        return _ok_datasets;
    }
    //下面代表：有外部数据集记录，也有查询参数，需要过滤的流程
    datasets.forEach((_dataset, _index) => {
            let _includeParams = getParamsFrom_DataSet(datasetList, _dataset.name, params);
            if(isPrintinfo)
            console.log("_dataset.name:", _dataset.name);
            let _table_withRecores = get_onUpdateDataMaps().get(_dataset.name);
            if(isPrintinfo)
            console.log("...._table_withRecores:", _table_withRecores);
            if (_includeParams.length > 0) {//如果当前dataSets对应的表结构中，此时，有参数传入
                let _final_rtn = [];
                let _records = _table_withRecores.response_data;//相当于从后台取到dataset对应的表结构的全部记录，再对记录做参数过滤
                //下面只对 _condition ==  and ,or,not;三种场景，满足条件就返回该记录
                if (_condition === '_or_') {
                    _records.forEach((_record, index) => {
                        console.log("....1...._record.index:", index);
                        let isFind = false;
                        for (let _i = 0; _i < _includeParams.length; _i++) {//记录1的多个参数循环
                            //举例：产品销售表；商品名称，区域名称，销售额三个字段；  查询参数1：区域选择：北京/上海，查询参数2：商品名称：笔记本电脑/台式机电脑
                            //记录1，  如果区域名称是上海，那么商品名称就不再查询了，，跳到记录2
                            //记录1，如果区域名称是北京，那么查询参数1的上海就不需要查了，同时，查询参数2都不要再查了，跳到记录2
                            let _includeparam = _includeParams[_i];

                            if (Array.isArray(_includeparam.value)) {//参数x的值，是一个列表---------------#
                                for (let _j = 0; _j < _includeparam.value.length; _j++) {//记录1 的参数1的多值循环
                                    let _paramValue = _includeparam.value[_j];
                                    if(isPrintinfo)
                                    console.log("....2...参数名._includeparam:", _includeparam.name, "，有多个值，当前值：", _paramValue, '，当前记录值：', _record[_includeparam.name]);
                                    if (_record[_includeparam.name] === _paramValue) {
                                        _final_rtn.push(deepCopy(_record));
                                        isFind = true;//告诉记录x，已经匹配了，就不需要
                                        break;//param1='北京,上海’,如果北京符合，就不需要再判断上海了，如果不是北京，还需要判断上海，如果上海符合，可以返回
                                    }
                                }
                                if (isFind) break;
                            } else {//参数param_x.value的值，是一个  ---------------------#
                                if(isPrintinfo)
                                console.log("....2...参数名._includeparam:", _includeparam.name, "，只有1个值，当前参数值：", _includeparam.value, '，当前记录值：', _record[_includeparam.name]);

                                if (_record[_includeparam.name] === _includeparam.value) {
                                    _final_rtn.push(deepCopy(_record));
                                    isFind = true;//告诉记录x，已经匹配了，就不需要
                                    break;//param1,param2,只要有一个符合条件，就返回，没有必要再对param3做判断了
                                }
                            }//------------------------------------------------------#
                            if (isFind) break;
                        }

                    })
                    _table_withRecores.response_data = Array.from(_final_rtn);
                    if(isPrintinfo)
                    console.log("...............本表结构，有查询参数，替换response_data,", _table_withRecores);

                } else {
                    throw "条件太复杂，不支持";
                }
            } else {//本表结构，没有查询参数
                if(isPrintinfo)
                console.log("...............本表结构，没有查询参数,", _dataset.name, _table_withRecores);
            }
            _ok_datasets.push(_table_withRecores);

        }
    )
    if(isPrintinfo)
    console.log("....有查询参数，有外部数据集，查询结果集记录:", _ok_datasets);
    return _ok_datasets;
})

export const filterDatasetsFrom_params_reportDesigner = ((datasets, params, datasetList, get_onUpdateDataMaps, _condition = '_or_') => {
    let _ok_datasets = [];
    console.log("@@@@@@@@@@@@@@@@@@,get_onUpdateDataMaps:",get_onUpdateDataMaps);
    datasets.forEach((_dataset, _index) => {
            let _includeParams = getParamsFrom_DataSet(datasetList, _dataset.name, params);

            console.log("_dataset.name:", _dataset.name);
            let _table_withRecores = get_onUpdateDataMaps.get(_dataset.name);
            console.log("...._table_withRecores:", _table_withRecores);
            if (_includeParams.length > 0) {//如果当前dataSets对应的表结构中，此时，有参数传入
                let _final_rtn = [];
                let _records = _table_withRecores.response_data;//相当于从后台取到dataset对应的表结构的全部记录，再对记录做参数过滤
                //下面只对 _condition ==  and ,or,not;三种场景，满足条件就返回该记录
                if (_condition === '_or_') {
                    _records.forEach((_record, index) => {
                        console.log("....1...._record.index:", index);
                        let isFind = false;
                        for (let _i = 0; _i < _includeParams.length; _i++) {//记录1的多个参数循环
                            //举例：产品销售表；商品名称，区域名称，销售额三个字段；  查询参数1：区域选择：北京/上海，查询参数2：商品名称：笔记本电脑/台式机电脑
                            //记录1，  如果区域名称是上海，那么商品名称就不再查询了，，跳到记录2
                            //记录1，如果区域名称是北京，那么查询参数1的上海就不需要查了，同时，查询参数2都不要再查了，跳到记录2
                            let _includeparam = _includeParams[_i];

                            if (Array.isArray(_includeparam.value)) {//参数x的值，是一个列表---------------#
                                for (let _j = 0; _j < _includeparam.value.length; _j++) {//记录1 的参数1的多值循环
                                    let _paramValue = _includeparam.value[_j];
                                    console.log("....2...参数名._includeparam:", _includeparam.name, "，有多个值，当前值：", _paramValue, '，当前记录值：', _record[_includeparam.name]);
                                    if (_record[_includeparam.name] === _paramValue) {
                                        _final_rtn.push(deepCopy(_record));
                                        isFind = true;//告诉记录x，已经匹配了，就不需要
                                        break;//param1='北京,上海’,如果北京符合，就不需要再判断上海了，如果不是北京，还需要判断上海，如果上海符合，可以返回
                                    }
                                }
                                if (isFind) break;
                            } else {//参数param_x.value的值，是一个  ---------------------#
                                console.log("....2...参数名._includeparam:", _includeparam.name, "，只有1个值，当前参数值：", _includeparam.value, '，当前记录值：', _record[_includeparam.name]);

                                if (_record[_includeparam.name] === _includeparam.value) {
                                    _final_rtn.push(deepCopy(_record));
                                    isFind = true;//告诉记录x，已经匹配了，就不需要
                                    break;//param1,param2,只要有一个符合条件，就返回，没有必要再对param3做判断了
                                }
                            }//------------------------------------------------------#
                            if (isFind) break;
                        }

                    })
                    _table_withRecores.response_data = Array.from(_final_rtn);
                    console.log("...............本表结构，有查询参数，替换response_data,", _table_withRecores);

                } else {
                    throw "条件太复杂，不支持";
                }
            } else {//本表结构，没有查询参数

                console.log("...............本表结构，没有查询参数,", _dataset.name, _table_withRecores);
            }
            _ok_datasets.push(_table_withRecores);

        }
    )
    console.log("..................根据params，最终返回的数据集的记录：", _ok_datasets);
    return _ok_datasets;
})


export const union_onUpdateDataMaps = (_sourceMap, _targetMap) => {
    for (const [key, value] of _sourceMap.entries()) {
        _targetMap.set(key, value);
    }
    return _targetMap;
}

export const union_onUpdateDataSetList = (_sourceDataSetList, _targetDataSetList) => {
    _sourceDataSetList.forEach((_dataset) => {
        _targetDataSetList.push(_dataset);
    })
    return _targetDataSetList;
}


