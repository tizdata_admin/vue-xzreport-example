/*const dataConfig=[
    {
        "name": "商品销售",
        "id": "商品销售id",
        "type": 4,
        "response_data": [],
        "fields": [
            {
                "name": "商品名称",
                "data_type": 3
            },
            {
                "name": "区域名称",
                "data_type": 3
            },
            {
                "name": "销售额",
                "data_type": 2
            }
        ],
        "params": [
            {
                "name": "商品名称",
                "data_type": 3
            },
            {
                "name": "区域名称",
                "data_type": 3
            }
        ]
    }
]
*/

 const dataConfig_chart2=[
     {
         "name": "商品销售",
         "id": "商品销售id",
         "type": 4,
         "response_data": [],
         "fields": [
             {
                 "name": "商品名称",
                 "data_type": 3
             },
             {
                 "name": "区域名称",
                 "data_type": 3
             },
             {
                 "name": "销售额",
                 "data_type": 2
             }
         ],
         "params": [
             {
                 "name": "商品名称",
                 "data_type": 3
             },
             {
                 "name": "区域名称",
                 "data_type": 3
             }
         ]
     },
     {
         "name": "商品季度销售",
         "id": "商品季度销售id",
         "type": 4,
         "response_data": [],
         "fields": [
             {
                 "name": "商品",
                 "data_type": 3
             },
             {
                 "name": "季度",
                 "data_type": 2
             },
             {
                 "name": "销售额",
                 "data_type": 2
             }
         ],
         "params": [
             {
                 "name": "商品",
                 "data_type": 3
             }
         ]
     }
 ]
export {dataConfig_chart2}