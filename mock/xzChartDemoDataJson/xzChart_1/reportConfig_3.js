//=====在小智BI设计器中，输出designer.getSaveData()的数据，从中copy下面需要的信息，形成文件”
// let    designer = new XZChartDesigner("#chartDesigner", option);
// let    chartDesignerReportData;
// function  saveChartDesignerData(){
//     chartDesignerReportData= designer.getSaveData();
//     console.log(chartDesignerReportData)
// }
//=====
let config = {
    "child": [
        {
            "id": "9527ab23-bbb6-4352-b669-b038544c9c9d",
            "type": "GroupBar",
            "option": {
                "chartConfig": {
                    "chartBasic": {
                        "grid__top": 35,
                        "grid__bottom": 35,
                        "grid__left": 35,
                        "grid__right": 35,
                        "itemStyle__borderRadius": 0,
                        "barCategoryGap": "30%",
                        "barGap": "120%",
                        "barWidth": 30
                    },
                    "legend": {
                        "show": true,
                        "orient": "horizontal",
                        "OffsetStartX": "left",
                        "OffsetStartY": "top",
                        "leftDistance": 0,
                        "topDistance": 0,
                        "selectedMode": "multiple",
                        "textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        },
                        "itemWidth": 28,
                        "itemHeight": 14
                    },
                    "group": {
                        "splitLine__show": true,
                        "splitLine__lineStyle__width": 1,
                        "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                        "text__show": true,
                        "text__rotation": 0,
                        "text__margin": 20,
                        "text__textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        }
                    },
                    "xAxis": {
                        "axisLine__show": true,
                        "axisLine__lineStyle__width": 1,
                        "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                        "text__show": true,
                        "text__rotation": 0,
                        "text__margin": 10,
                        "text__textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        }
                    },
                    "yAxis": {
                        "axisLine__show": false,
                        "axisLine__lineStyle__width": 1,
                        "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                        "axisTick__show": true,
                        "axisTick__length": 5,
                        "axisTick__lineStyle__width": 1,
                        "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                        "axisLabel__show": true,
                        "axisLabel__rotate": 0,
                        "axisLabel__textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        },
                        "splitLine__show": false,
                        "splitLine__lineStyle__width": 1,
                        "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                        "splitNumber": 0
                    },
                    "customSeries": {
                        "show": false,
                        "position": "top",
                        "align": "center",
                        "textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        },
                        "rotate": 0,
                        "distance": 0
                    },
                    "tooltip": {
                        "showContent": true,
                        "backgroundColor": "rgba(0, 0, 0, 1)",
                        "textStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(155, 155, 155, 1)"
                        }
                    },
                    "customColor": {
                        "newColor": [
                            {
                                "seriesName": "度量一",
                                "color": "#538fe2"
                            },
                            {
                                "seriesName": "度量二",
                                "color": "#89e5f9"
                            },
                            {
                                "seriesName": "度量三",
                                "color": "#9b8bba"
                            },
                            {
                                "seriesName": "度量四",
                                "color": "#69a8ff"
                            },
                            {
                                "seriesName": "度量五",
                                "color": "#9dc6ff"
                            }
                        ]
                    },
                    "markLineEvent": {
                        "markLine": []
                    }
                },
                "dataConfig": {
                    "data_name": "数据表名称",
                    "indexs": [
                        {
                            "name": "商品名称",
                            "sort": 0,
                            "format": {
                                "showData": 1,
                                "quantile": 0,
                                "multiplying": 0,
                                "decimal": 5,
                                "date": "yyyy-MM-dd",
                                "removePrefix": "",
                                "removeSuffix": "",
                                "addPrefix": "",
                                "addSuffix": "",
                                "location": -1,
                                "replaceNull": {
                                    "type": 0,
                                    "value": ""
                                },
                                "newName": "",
                                "region": "none",
                                "valueType": 1,
                                "scaleType": 1,
                                "selectorFormat": {
                                    "show": 1,
                                    "allShow": 0,
                                    "allName": "全部"
                                }
                            },
                            "type": "3",
                            "noSuchField": false
                        },
                        {
                            "name": "区域名称",
                            "sort": 0,
                            "format": {
                                "showData": 1,
                                "quantile": 0,
                                "multiplying": 0,
                                "decimal": 5,
                                "date": "yyyy-MM-dd",
                                "removePrefix": "",
                                "removeSuffix": "",
                                "addPrefix": "",
                                "addSuffix": "",
                                "location": -1,
                                "replaceNull": {
                                    "type": 0,
                                    "value": ""
                                },
                                "newName": "",
                                "region": "none",
                                "valueType": 1,
                                "scaleType": 1,
                                "selectorFormat": {
                                    "show": 1,
                                    "allShow": 0,
                                    "allName": "全部"
                                }
                            },
                            "type": "3",
                            "noSuchField": false
                        }
                    ],
                    "columns": [
                        {
                            "name": "销售额",
                            "sort": 0,
                            "format": {
                                "showData": 1,
                                "quantile": 0,
                                "multiplying": 0,
                                "decimal": 5,
                                "date": "yyyy-MM-dd",
                                "removePrefix": "",
                                "removeSuffix": "",
                                "addPrefix": "",
                                "addSuffix": "",
                                "location": -1,
                                "replaceNull": {
                                    "type": 0,
                                    "value": ""
                                },
                                "newName": "",
                                "region": "none",
                                "valueType": 1,
                                "scaleType": 1,
                                "selectorFormat": {
                                    "show": 1,
                                    "allShow": 0,
                                    "allName": "全部"
                                }
                            },
                            "aggregate": 1,
                            "type": "2",
                            "noSuchField": false,
                            "uuid": "80f940c4-017d-4a01-bbef-2d66e7ba5e5f"
                        }
                    ],
                    "selectors": [],
                    "filters": [],
                    "tooltips": [],
                    "type": 1,
                    "data_id": "数据表id"
                },
                "eventConfig": {
                    "responseEvent": {
                        "linkage_open": true,
                        "linkage_errorType": 1,
                        "linkage_promptText": "无此联动值"
                    },
                    "clickEvent": {
                        "events": []
                    },
                    "toolbarEvent": {
                        "toolbar": []
                    }
                },
                "dynamicConfig": {
                    "marquee": {
                        "open": false,
                        "interval": 1000,
                        "triggerLinkage": false
                    },
                    "refresh": {
                        "open": false,
                        "type": 1,
                        "time": [
                            0
                        ],
                        "interval": "10"
                    }
                },
                "generalConfig": {
                    "base": {
                        "backgroundColor": "#00000000",
                        "background": {
                            "show": false,
                            "value": ""
                        },
                        "backgroundImage": "",
                        "backgroundSize": 1,
                        "borderRadius": "0px",
                        "boxShadow": false,
                        "boxShadowColor": "#fff",
                        "boxShadowLevel": "0px",
                        "boxShadowVertical": "0px",
                        "boxShadowLength": "2px",
                        "boxShadowBlur": "5px",
                        "borderWidth": "0px",
                        "borderColor": "#eee",
                        "padding": "0px"
                    },
                    "title": {
                        "show": true,
                        "height": "30px",
                        "backgroundColor": "rgba(0, 0, 0, 0)",
                        "background": {
                            "show": false,
                            "value": ""
                        },
                        "backgroundImage": "",
                        "backgroundStyle": 1,
                        "content": "分组簇状柱图",
                        "hyperlink": "",
                        "titleTextStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "textDecoration": "none",
                            "color": "#000000"
                        },
                        "textAlign": "center"
                    },
                    "select": {
                        "show": true,
                        "height": "30px",
                        "backgroundColor": "rgba(0, 0, 0, 0)",
                        "background": {
                            "show": false,
                            "value": ""
                        },
                        "backgroundImage": "",
                        "backgroundStyle": 1,
                        "borderWidth": "1px",
                        "borderColor": "#eee",
                        "selectorTextStyle": {
                            "fontFamily": "sans-serif",
                            "fontSize": 12,
                            "fontWeight": "normal",
                            "fontStyle": "normal",
                            "color": "rgba(96, 98, 102, 1)"
                        },
                        "method": 1,
                        "proportion": "",
                        "absolute": "",
                        "spacing": "0px"
                    }
                }
            }
        }
    ],
    "layout": {
        "report": {
            "width": "1920px",
            "height": "1080px",
            "padding": "0px",
            "single_height": "1080px",
            "backgroundColor": "#fff",
            "backgroundImage": "",
            "backgroundSize": 1
        },
        "9527ab23-bbb6-4352-b669-b038544c9c9d": {
            "width": "960px",
            "height": "540px",
            "zIndex": "99",
            "transform": "translate(0px)",
            "display": ""
        }
    },
    "controlLayer": [
        {
            "id": "9527ab23-bbb6-4352-b669-b038544c9c9d",
            "type": "GroupBar",
            "name": "分组簇状柱图",
            "img_url": "bar.png",
            "zIndex": 99,
            "c_type": 1,
            "cType": "",
            "chartShow": true,
            "chartLock": false,
            "random": "a079d0"
        }
    ]
}
export {config}  //在纯html嵌入脚本中，必须注释掉