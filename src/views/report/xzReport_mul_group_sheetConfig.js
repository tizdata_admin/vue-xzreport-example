export const reportTemplate_mul_group = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "value": "商品", "column": 0, "styleIndex": 1}, {
            "row": 0,
            "value": "季度",
            "column": 1,
            "styleIndex": 1
        }, {"row": 0, "value": "销售额", "column": 2, "styleIndex": 1}, {
            "row": 0,
            "value": "分组排名",
            "column": 3,
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.商品",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.商品", "axisType": 1, "data_type": "3"},
            "styleIndex": 4
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.季度",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.季度", "axisType": 1, "data_type": "2"},
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.销售额",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"order": 0, "value": "sale.销售额", "axisType": 2, "aggregate": 1, "data_type": "2"},
            "styleIndex": 5
        }, {"row": 1, "type": 5, "value": "=COUNT(C2[!0]{A2 = $A2 && C2 > $C2}) + 1", "column": 3, "styleIndex": 4}],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Width": 159, "Column": 3}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 1,
            "sci": 0,
            "sri": 0,
            "label": "A1-D2",
            "noagg": false,
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(255, 123, 88,0.5)",
            "borderColor": "rgba(255, 123, 88)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#93D051"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#01B0F1"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#BFBFBF"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#FFFFFF"
        }],
        "pageFreezeConfig": "A1",
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}];

export const sheetConfig_mul_group = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}, {"Row": 2}, {"Row": 3}, {"Row": 4}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "type": 6, "value": "商品", "column": 0, "styleIndex": 3}, {
            "row": 0,
            "type": 6,
            "value": "年份",
            "column": 1,
            "styleIndex": 3
        }, {"row": 0, "type": 6, "value": "季度", "column": 2, "styleIndex": 3}, {
            "row": 0,
            "type": 6,
            "value": "销售额",
            "column": 3,
            "styleIndex": 3
        }, {
            "row": 1,
            "type": 2,
            "value": "test.商品",
            "column": 0,
            "rowSpan": 3,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "columnSpan": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.商品", "axisType": 1, "data_type": "3"},
            "styleIndex": 2
        }, {
            "row": 1,
            "type": 2,
            "value": "test.年份",
            "column": 1,
            "rowSpan": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "columnSpan": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.年份", "axisType": 1, "data_type": "2"},
            "styleIndex": 2
        }, {
            "row": 1,
            "type": 2,
            "value": "test.季度",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "test.季度", "axisType": 1, "data_type": "2"},
            "styleIndex": 3
        }, {
            "row": 1,
            "type": 2,
            "value": "test.销售额",
            "column": 3,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"order": 0, "value": "test.销售额", "axisType": 2, "aggregate": 1, "data_type": "2"},
            "styleIndex": 3
        }, {"row": 2, "column": 0, "styleIndex": 4}, {"row": 2, "column": 1, "styleIndex": 3}, {
            "row": 2,
            "type": 6,
            "value": "计数",
            "column": 2,
            "styleIndex": 5
        }, {"row": 2, "type": 5, "value": "=count(D2)", "column": 3, "styleIndex": 5}, {
            "row": 3,
            "column": 0,
            "styleIndex": 4
        }, {
            "row": 3,
            "type": 6,
            "value": "计数",
            "column": 1,
            "rowSpan": 1,
            "columnSpan": 2,
            "styleIndex": 6
        }, {"row": 3, "column": 2, "styleIndex": 7}, {
            "row": 3,
            "type": 5,
            "value": "=count(D2)",
            "column": 3,
            "styleIndex": 5
        }, {
            "row": 4,
            "type": 6,
            "value": "计数",
            "column": 0,
            "rowSpan": 1,
            "columnSpan": 3,
            "styleIndex": 6
        }, {"row": 4, "column": 1, "styleIndex": 7}, {"row": 4, "column": 2, "styleIndex": 7}, {
            "row": 4,
            "type": 5,
            "value": "=count(D2)",
            "column": 3,
            "styleIndex": 5
        }],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Column": 3}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 4,
            "sci": 0,
            "sri": 0,
            "label": "A1-D5",
            "noagg": false,
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(115, 234, 255,0.5)",
            "children": [{
                "eci": 3,
                "eri": 3,
                "sci": 1,
                "sri": 0,
                "label": "B1-D4",
                "noagg": false,
                "no_agg": false,
                "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
                "bgColor": "rgba(224, 115, 255,0.5)",
                "children": [{
                    "eci": 3,
                    "eri": 2,
                    "sci": 2,
                    "sri": 0,
                    "label": "C1-D3",
                    "noagg": false,
                    "no_agg": false,
                    "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
                    "bgColor": "rgba(255, 115, 191,0.5)",
                    "borderColor": "rgba(255, 115, 191)"
                }],
                "borderColor": "rgba(224, 115, 255)"
            }],
            "borderColor": "rgba(115, 234, 255)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center", "valign": "middle"}, {
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "valign": "middle"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {"align": "center"}, {
            "font": {"bold": true},
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {
            "font": {"bold": true},
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "valign": "middle"
        }, {"font": {"bold": true}, "align": "center"}],
        "pageFreezeConfig": "A1",
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}]

export const response_data_mul_group = [{"商品": " A产品", "季度": 1, "年份": 2020, "销售额": 1}, {
    "商品": " A产品",
    "季度": 2,
    "年份": 2020,
    "销售额": 2
}, {"商品": " A产品", "季度": 3, "年份": 2020, "销售额": 1}, {
    "商品": " A产品",
    "季度": 4,
    "年份": 2020,
    "销售额": 1
}, {"商品": " A产品", "季度": 1, "年份": 2021, "销售额": 2}, {
    "商品": " A产品",
    "季度": 2,
    "年份": 2021,
    "销售额": 2
}, {"商品": " A产品", "季度": 3, "年份": 2021, "销售额": 1}, {
    "商品": " A产品",
    "季度": 4,
    "年份": 2021,
    "销售额": 2
}, {"商品": " B产品", "季度": 1, "年份": 2020, "销售额": 3}, {
    "商品": " B产品",
    "季度": 2,
    "年份": 2020,
    "销售额": 1
}, {"商品": " B产品", "季度": 3, "年份": 2020, "销售额": 2}, {
    "商品": " B产品",
    "季度": 4,
    "年份": 2020,
    "销售额": 1
}, {"商品": " B产品", "季度": 1, "年份": 2021, "销售额": 2}, {
    "商品": " B产品",
    "季度": 2,
    "年份": 2021,
    "销售额": 1
}, {"商品": " B产品", "季度": 3, "年份": 2021, "销售额": 3}, {"商品": " B产品", "季度": 4, "年份": 2021, "销售额": 2}]
export const sortInGroup_formula_mul_group = "=COUNT(C2[!0]{A2 = $A2 && C2 > $C2}) + 1";
export const data_mul_group = [
    {
        "type": 4,
        "name": "test",
        "response_data": response_data_mul_group
    }
]
export const option_mul_group = {
    mode: 'read',  //  报表模式  read | edit  只读模式|编辑模式
    view: {
        width: () => document.documentElement.clientWidth,
        height: () => document.documentElement.clientHeight
    },   // 设置报表的宽高
    renderArrow: false,  // 是否显式 扩展方向图标
    showFreeze: false,  // 是否显式冻结线
    showGrid: false   // 是否显式网格线
};

export const reportPrintOptions_mul_group = [
    {
        name: "多级分组",
        //print: sheetConfig
    }
]

