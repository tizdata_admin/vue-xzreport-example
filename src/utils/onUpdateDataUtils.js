import _ from 'lodash'


function queryResponseDataOnParam(response_data, param) {

    if(Array.isArray(param.value)){
        let found = _.filter(response_data, function (item) {
                let _rtn = false;

                for (let i = 0; i < param.value.length; i++) {
                    if (item[param.name] === param.value[i]) {
                        _rtn = true;
                      //  console.log(`ok,参数值是多值数组类型  param.name:${param.name},param.value[${i}]:${param.value[i]};item[param.name]:${item[param.name]}`);
                    }else{
                      //  console.log(`no,,参数值是多值数组类型  param.name:${param.name},param.value[${i}]:${param.value[i]};item[param.name]:${item[param.name]}`);

                    }
                }
                return _rtn
            }
        )
        //console.log(".....参数多值，found:",found)
        return found;
    }else{

        let found = _.filter(response_data, function (item) {
            if(item[param.name] === param.value) {
               // console.log(`ok   item[param.name]:${item[param.name]},param.value:${param.value}`);
                return true;
            }else{
               // console.log(`no   item[param.name]:${item[param.name]},param.value:${param.value}`);

                return false
            }

        })
        //console.log(".....参数单值，found:",found)
        return found;
    }
}

function queryResponseDataOnparams(response_data, params) {

    let _rtn = response_data;
    for (let i = 0; i < params.length; i++) {
            _rtn=queryResponseDataOnParam(_rtn, params[i])
    }
    return _rtn
}

function queryResponseDataOnDataset(onUpdateData, params) {
    let {name, id, type} = {...onUpdateData};
    if (id == null) id =name;
    //console.log('........数据集名称：', name, type)
    return {name, id, type, response_data: queryResponseDataOnparams(onUpdateData.response_data, params)}

}

function queryResponseDataOnDatasets(onUpdateDatas, params) {
    //如果参数为空，则返回全部原始数据集
    if (params.length == 0) {
        console.log("没有查询参数，全部返回记录：",onUpdateDatas)
        return onUpdateDatas;}

    let _finalrtn = [];
    for (let i = 0; i < onUpdateDatas.length; i++) {
        let onUpdateData = onUpdateDatas[i];
        _finalrtn.push(queryResponseDataOnDataset(onUpdateData, params))
    }
    console.log("查询参数不为空，返回的过滤后的记录：",_finalrtn)

    return _finalrtn

}


function queryonUpdateDataFrom(datasets, params, onUpdateDatas, printinfo = false) {
        return queryResponseDataOnDatasets(onUpdateDatas, params);
}

// // onUpdateDatas_all 10个数据集的记录；onUpdateDatas_finalrtn 7个没有动态查询参数的数据集的记录；
// // 返回 onUpdateDatas_all中3个发生了参数查询的数据集记录
// const _getOnupdateDatasWithoutParams = (onUpdateDatas_all, datasets) => {
//     return _.filter(onUpdateDatas_all, function (onupdateData) {
//         let filted = true;
//         for (let i = 0; i < datasets.length; i++) {
//             if (onupdateData.name === datasets[i].name) {
//                 filted = false;
//                 break
//             }
//         }
//         return filted
//     })
// }
// const _getOnupdateDatasWithParams = (onUpdateDatas_all, datasets) => {
//     return _.filter(onUpdateDatas_all, function (onupdateData) {
//         let filted = false;
//         for (let i = 0; i < datasets.length; i++) {
//             if (onupdateData.name === datasets[i].name) {
//                 filted = true;
//                 break
//             }
//         }
//         return filted
//     })
// }
export {queryResponseDataOnDatasets, queryonUpdateDataFrom}


