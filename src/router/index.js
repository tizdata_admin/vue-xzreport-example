import {createRouter, createWebHashHistory} from 'vue-router'
import XzReportDesignerEdit5 from "@/views/reportDesigner/XzReportDesignerEdit5.vue";
import XzReport1 from "@/views/report/XzReport1.vue";
import Chart_1 from "@/views/chart/Chart_1/Chart_1.vue";
import ChartDesigner_2 from "@/views/chartDesigner/ChartDesigner_2.vue";
import ChartDesigner_2_2 from "@/views/chartDesigner/ChartDesigner_2_2.vue";
import index404 from "@/views/404/index404.vue"
import Chart_2 from "@/views/chart/Chart_2/Chart_2.vue";

export const constantRoutes = [
    {        // 1
        path: '/login',
        component: () => import('@/views/login/index.vue'),
        name: 'login',
        meta: {
            title: '登录',
            order: "1",
            hidden: true,
            icon: "Promotion"
        }

    },

    {        // 2
        path: '/',
        component: () => import('@/layout/index.vue'),
        name: 'layout',
        meta: {
            title: 'layout',//面包屑不显示
            order: "2",
            hidden: false,
            icon: "Promotion"
        },
        redirect: "/home",
        children: [
            {        // 3
                path: '/home',
                component: () => import('@/views/home/index.vue'),
                name: 'home',
                meta: {
                    title: '首页',
                    order: "2-3",
                    hidden: false,
                    icon: "HomeFilled"
                },
            }
        ]
    },
    {
        path: "/xzchartdesigner",
        component: () => import("@/layout/index.vue"),
        name: "XzchartDesigner",
        redirect: "/xzChartDesigner_2",

        meta: {
            hidden: false,
            title: "小智BI设计器",
            icon: "Platform"
        },
        children: [
            // {        // 10
            //     path: '/xzChartDesigner_2',
            //     component: ChartDesigner_2,
            //     name: 'xzChartDesigner_2',
            //     meta: {
            //         title: '编辑模板',
            //         order: "10",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            {        // 10
                path: '/xzChartDesigner_2',
                component: () => import("@/views/chartDesigner/ChartDesigner_2_2.vue"),
                name: 'xzChartDesigner_2',
                meta: {
                    title: '修改BI模板',
                    order: "10",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 10
                path: '/xzChartDesigner_8',
                component: () => import("@/views/chartDesigner/chartDesigner8/cd8_index.vue"),
                name: 'xzChartDesigner_8',
                meta: {
                    title: '新建模板和测试',
                    order: "10",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            //
            // {        // 11
            //     path: '/xzChartDesigner_2_2',
            //     component: ChartDesigner_2_2,
            //     name: 'xzChartDesigner_2_2',
            //     meta: {
            //         title: '小智BI设计器2-2',
            //         order: "11",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            {        // 11
                path: '/xzChartDesignertabs3',
                component: () => import("@/views/chartDesigner/chartDesignerEltabs3/index.vue"),
                name: 'xzChartDesignertabs3',
                meta: {
                    title: '修改模板和预览',
                    order: "11",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 11
                path: '/datadrilling',
                component: () => import("@/views/chartDesigner/DataDrilling/DataDrilling.vue"),
                name: 'datadrilling',
                meta: {
                    title: '数据钻取',
                    order: "11",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 11
                path: '/penetrate',
                component: () => import("@/views/chartDesigner/DataDrilling_Penetrate/DataDrillingPenetrate.vue"),
                name: 'penetrate',
                meta: {
                    title: '数据钻取加穿透',
                    order: "11",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            // {        // 11
            //     path: '/customKeyFrames',
            //     component: () => import("@/views/customSample/echartsBar/CustomEchartBar.vue"),
            //     name: 'customKeyFrames',
            //     meta: {
            //         title: '自定义BI控件',
            //         order: "11",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // }
        ]

    },
    {
        path: "/xzchart",
        component: () => import("@/layout/index.vue"),
        name: "Xzchart",
        meta: {
            hidden: false,
            title: "小智BI展现",
            icon: "Platform"
        },
        children: [
            {        // 9
                path: '/xzchart/xzChart_1',
                component: Chart_1,
                name: 'xzChart_1',
                meta: {
                    title: '小智BI-1',
                    order: "9",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },

            // {        // 9
            //     path: '/xzchart/xzChart_2',
            //     component: Chart_2,
            //     name: 'xzChart_2',
            //     meta: {
            //         title: '小智BI-2',
            //         order: "9",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            {        // 9
                path: '/xzchart/chatdatadrilling',
                component: ()=>import("@/views/chart/datadrilling/ChatDatadrilling.vue"),
                name: 'chatdatadrilling',
                meta: {
                    title: '数据钻取和控件联动',
                    order: "9",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },

        ]

    },

    {
        path: "/xzreportdesigner",
        component: () => import("@/layout/index.vue"),
        name: "Xzreportdesigner",
        meta: {
            hidden: false,
            title: "报表设计器",
            icon: "Platform"
        },
        redirect: '/xzreportdesigner/xzreportdesigner5',
        children: [
            {        // 7
                path: '/xzreportdesigner/xzreportdesigner5',
                component: XzReportDesignerEdit5,
                name: "xzreportdesigner5",
                meta: {
                    title: '编辑',
                    order: "7",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 7
                path: '/xzreportdesigner/xzreportdesignertabs6',
                component: () => import("@/views/reportDesigner/xzreportdesignertabs/index.vue"),
                name: "xzreportdesignertabs6",
                meta: {
                    title: '模板加载-数据保存-回显',
                    order: "7",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 7
                path: '/xzreportdesigner/xzreportdesignerIndentor',
                component: () => import("@/views/reportDesigner/indentor/indentorIndex.vue"),
                name: "xzreportdesignerIndentor",
                meta: {
                    title: '设计回显',
                    order: "7",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 7
                path: '/xzreportdesigner/fineReportTable',
                component: () => import("@/views/reportDesigner/fineReport/fineReportTable.vue"),
                name: "fineReportTable",
                meta: {
                    title: '层级坐标',
                    order: "7",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            // {        // 7
            //     path: '/xzreportdesigner/updateservice',
            //     component: () => import("@/views/reportDesigner/updateService/updateServiceIndex.vue"),
            //     name: "updateService",
            //     meta: {
            //         title: 'updateService',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_abs_v',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_abs_v/FineReport3801_abs_V.vue"),
            //     name: "finereport3801_abs_v",
            //     meta: {
            //         title: '绝对坐标-纵向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_abs_h',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_abs_h/FineReport3801_abs_H.vue"),
            //     name: "finereport3801_abs_hori",
            //     meta: {
            //         title: '绝对坐标-横向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_rel_v',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_rel_v/FineReport3801_rel_V.vue"),
            //     name: "finereport3801_rel_v",
            //     meta: {
            //         title: '相对坐标-纵向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_rel_h',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_rel_h/FineReport3801_rel_H.vue"),
            //     name: "finereport3801_rel_hori",
            //     meta: {
            //         title: '相对坐标-横向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_two_abs',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_two_abs/FineReport3801_two_abs.vue"),
            //     name: "finereport3801_two_abs",
            //     meta: {
            //         title: '绝对坐标-双向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_two_rel',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_two_rel/FineReport3801_two_rel.vue"),
            //     name: "finereport3801_two_rel",
            //     meta: {
            //         title: '相对坐标-双向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 7
            //     path: '/xzreportdesigner/finereport3801_two_other',
            //     component: () => import("@/views/reportDesigner/fineReport/fr3801_two_other/FineReport3801_two_other.vue"),
            //     name: "finereport3801_two_other",
            //     meta: {
            //         title: '单行单列-双向扩展',
            //         order: "7",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },

        ]

    },
    {
        path: "/xzreport",
        component: () => import("@/layout/index.vue"),
        name: "Xzreport",
        meta: {
            hidden: false,
            title: "报表展现",
            icon: "Platform"
        },
        redirect: '/xzreport/xzreport_1',
        children: [
            {        // 8
                path: '/xzreport_1',
                component: XzReport1,
                name: 'xzreport_1',
                meta: {
                    title: '报表1',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 8
                path: '/xzreport_test',
                component: () => import("@/views/report/testReport/index.vue"),
                name: 'xzreport_test',
                meta: {
                    title: '报表_test',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 8
                path: '/xzreport_sortInGroup',
                component: () => import("@/views/report/XzReport_sortInGroup.vue"),
                name: 'sortInGroup',
                meta: {
                    title: '组内排序',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            // {        // 8
            //     path: '/xzreport_acc_lyl',
            //     component: ()=>import("@/views/report/xzReport_accumulate_LayerByLayer.vue"),
            //     name: 'acc_lyl',
            //     meta: {
            //         title: '逐层累计',
            //         order: "8",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            // {        // 8
            //     path: '/xzreport_acc_cross',
            //     component: ()=>import("@/views/report/xzReport_accumulate_cross.vue"),
            //     name: 'acc_cross',
            //     meta: {
            //         title: '交叉表累计报表',
            //         order: "8",
            //         hidden: false,
            //         icon: "InfoFilled"
            //     },
            // },
            {        // 8
                path: '/xzreport_mainSub_1',
                component: () => import("@/views/report/xzReport_mainSub_1.vue"),
                name: 'xzreport_mainSub',
                meta: {
                    title: '基础主子报表',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 8
                path: '/xzreport_cross_mul_column',
                component: () => import("@/views/report/xzReport_cross_mul_column.vue"),
                name: 'xzreport_cross_mul_column',
                meta: {
                    title: '交叉表多数据列',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 8
                path: '/xzreport_mul_group',
                component: () => import("@/views/report/xzReport_mul_group.vue"),
                name: 'xzreport_mul_group',
                meta: {
                    title: '多级分组',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
            {        // 8
                path: '/queryPanel_1',
                component: () => import("@/views/report/queryPanel/queryPanel.vue"),
                name: 'queryPanel_1',
                meta: {
                    title: '查询面板',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            }, {        // 8
                path: '/customQueryPanel_1',
                component: () => import("@/views/report/queryPanelCustom/customQueryPanel.vue"),
                name: 'customQueryPanel_1',
                meta: {
                    title: '定制查询面板',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            }, {        // 8
                path: '/previewTemplatePrint',
                component: () => import("@/views/report/printTemplate/previewTemplatePrint.vue"),
                name: 'previewTemplatePrint',
                meta: {
                    title: '预览打印',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            }, {        // 8
                path: '/batchTemplatePrint',
                component: () => import("@/views/report/printTemplate/batchTemplatePrint_simple.vue"),
                name: 'batchTemplatePrint',
                meta: {
                    title: '批量打印',
                    order: "8",
                    hidden: false,
                    icon: "InfoFilled"
                },
            },
        ]

    },
    {
        path: "/xzreportInAction",
        component: () => import("@/layout/index.vue"),
        name: "xzreportInAction",
        meta: {
            title: "小智报表实战-玩转系列一",
            icon: "InfoFilled"
        },
        children: [
            {
                path: "/action_1_zcljys",
                component: () => import("@/views/inAction/action_1/action_1_zcljys.vue"),
                name: "action_1_zcljys",
                meta: {
                    title: "bug 逐层累计",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_1",
                component: () => import("@/views/inAction/action_1/action_1.vue"),
                name: "action_1",
                meta: {
                    title: "bug 逐层累计 action_1",
                    icon: "InfoFilled"
                }
            },
            // {
            //     path:"/action_2",
            //     component: () => import("@/views/inAction/action_2/action_2.vue"),
            //     name: "action_2",
            //     meta: {
            //         title: "action_2组内占比合计近期环比",
            //         icon: "InfoFilled"
            //     }
            // },
            {
                path: "/action_2_8",
                component: () => import("@/views/inAction/action_2/action_2_8.vue"),
                name: "action_2_8",
                meta: {
                    title: "组内占比合计近期环比",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_2_9",
                component: () => import("@/views/inAction/action_2/action_2_9.vue"),
                name: "action_2_9",
                meta: {
                    title: "组内占比card",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_3",
                component: () => import("@/views/inAction/action_3/action_3.vue"),
                name: "action_3",
                meta: {
                    title: "交叉累计-简易",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_3_2",
                component: () => import("@/views/inAction/action_3/action_3_2.vue"),
                name: "action_3_2",
                meta: {
                    title: "多维度交叉累计冻结1-3行1-2列",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_3_3.vue",
                component: () => import("@/views/inAction/action_3/action_3_3.vue"),
                name: "action_3_3.vue",
                meta: {
                    title: "多维度交叉累计冻结1-3行",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_4",
                component: () => import("@/views/inAction/action_4/action_4.vue"),
                name: "action_4",
                meta: {
                    title: "跨层累计总计小计组内排名",
                    icon: "InfoFilled"
                }
            },
            {
                path:"/action_5",
                component: () => import("@/views/inAction/action_5/action_5.vue"),
                name: "action_5",
                meta: {
                    title: "逐层累计",
                    icon: "InfoFilled"
                }
            },
            {
                path:"/action_7",
                component: () => import("@/views/inAction/action_7/action_7.vue"),
                name: "action_7",
                meta: {
                    title: "跨层累计",
                    icon: "InfoFilled"
                }
            },
            {
                path: "/action_8_1",
                component: () => import("@/views/inAction/action_8/action_8_1.vue"),
                name: "action_8_1",
                meta: {
                    title: "2分栏-检验报告单",
                    icon: "InfoFilled"
                }
            },
            // {
            //     path: "/custom_xinyi",
            //     component: () => import("@/views/customSample/jiangsuxinyi/xinyi.vue"),
            //     name: "custom_xingyi",
            //     meta: {
            //         title: "江苏鑫亿",
            //         icon: "InfoFilled"
            //     }
            // },

        ]
    },

    {
        path: "/screen",
        component: () => import("@/views/screen/index.vue"),
        name: "Screen",
        meta: {
            hidden: false,
            title: "数据大屏",
            icon: "Platform"
        }

    },
    {
        path: "/testdefmodel",
        component: () => import("@/views/testdeepcomponent/definemodel/DemoDefineModelMain.vue"),
        name: "testdefmodel",
        meta: {
            hidden: false,
            title: "testdefmodel",
            icon: "Platform"
        }

    },
    {
        path: "/acl",
        component: () => import("@/layout/index.vue"),
        name: "Acl",
        meta: {
            title: "权限管理",
            icon: 'Lock'
        },
        redirect: '/acl/user',
        children: [
            {
                path: '/acl/user',
                component: () => import("@/views/acl/user/index.vue" ),
                name: 'User',
                meta: {
                    title: "用户管理",
                    icon: 'User',
                }
            }, {
                path: '/acl/role',
                component: () => import("@/views/acl/role/index.vue" ),
                name: 'Role',
                meta: {
                    title: "角色管理",
                    icon: 'UserFilled',
                }
            }, {
                path: '/acl/permission',
                component: () => import("@/views/acl/permission/index.vue" ),
                name: 'Permission',
                meta: {
                    title: "菜单管理",
                    icon: 'Monitor',
                }
            }
        ]
    },
    // {
    //     path: '/product',
    //     component: () => import('@/layout/index.vue'),
    //     name: 'Product',
    //     meta: {
    //         title: "商品管理",
    //         icon: "Goods",
    //     },
    //     redirect: '/product/trademark',
    //     children: [
    //         {
    //             path: '/product/trademark',
    //             component: () => import("@/views/product/trademark/index.vue"),
    //             name: 'Trademark',
    //             meta: {
    //                 title: "品牌管理",
    //                 //hidden: false,
    //                 icon: 'ShoppingCartFull',
    //
    //             }
    //         }, {
    //             path: '/product/attr',
    //             component: () => import("@/views/product/attr/index.vue"),
    //             name: 'Attr',
    //             meta: {
    //                 title: "属性管理",
    //                 //hidden: false,
    //                 icon: 'ChromeFilled',
    //
    //             }
    //         }, {
    //             path: '/product/spu',
    //             component: () => import("@/views/product/spu/index.vue"),
    //             name: 'Spu',
    //             meta: {
    //                 title: "SPU管理",
    //                 //hidden: false,
    //                 icon: 'Calendar',
    //
    //             }
    //         }, {
    //             path: '/product/sku',
    //             component: () => import("@/views/product/sku/index.vue"),
    //             name: 'Sku',
    //             meta: {
    //                 title: "SKU管理",
    //                 //hidden: false,
    //                 icon: 'Orange',
    //
    //             }
    //         }
    //     ]
    // },
    {        // 12
        path: '/:pathMatch(.*)*',
        redirect: '/404',
        component: index404,
        name: 'any',
        meta: {
            title: '任意路由',
            order: "12",
            hidden: true,
            icon: "InfoFilled"
        },
    },
    {//13
        path: '/404',
        component: () => import('@/views/404/index404.vue'),
        name: '404',
        meta: {
            title: '404',
            order: "13",
            hidden: true,
            icon: "InfoFilled"
        },
    },
]
const router = createRouter({
    history: createWebHashHistory(),
    routes: constantRoutes,
    scrollBehavior() {
        return {
            left: 0,
            top: 0
        }
    }
})
export default router
