let templateIns = [{
    "name": "sheet1", "SheetData": {
        "freeze": "A1",
        "rowHead": [{"Row": 0}, {"Row": 1}],
        "collapse": [],
        "apiConfig": {"key": "", "url": "", "secret": ""},
        "waterMask": null,
        "cellConfig": [{"row": 0, "value": "商品", "column": 0, "styleIndex": 1}, {
            "row": 0,
            "value": "季度",
            "column": 1,
            "styleIndex": 1
        }, {"row": 0, "value": "销售额", "column": 2, "styleIndex": 1}, {
            "row": 0,
            "value": "分组排名",
            "column": 3,
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.商品",
            "column": 0,
            "dataType": "3",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.商品", "axisType": 1, "data_type": "3"},
            "styleIndex": 4
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.季度",
            "column": 1,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"merge": 1, "order": 0, "value": "sale.季度", "axisType": 1, "data_type": "2"},
            "styleIndex": 1
        }, {
            "row": 1,
            "type": 2,
            "value": "sale.销售额",
            "column": 2,
            "dataType": "2",
            "editable": false,
            "expansion": 1,
            "scaleType": 1,
            "valueType": 1,
            "dataConfig": {"order": 0, "value": "sale.销售额", "axisType": 2, "aggregate": 1, "data_type": "2"},
            "styleIndex": 5
        }, {"row": 1, "type": 5, "value": "=COUNT(C2[!0]{A2 = $A2 && C2 > $C2}) + 1", "column": 3, "styleIndex": 4}],
        "columnHead": [{"Column": 0}, {"Column": 1}, {"Column": 2}, {"Width": 159, "Column": 3}],
        "dataConfig": [],
        "groupConfig": [{
            "eci": 3,
            "eri": 1,
            "sci": 0,
            "sri": 0,
            "label": "A1-D2",
            "noagg": false,
            "filter": null,
            "no_agg": false,
            "repeat": {"isRepeat": false, "repeatKey": "", "repeatValue": ""},
            "bgColor": "rgba(255, 123, 88,0.5)",
            "borderColor": "rgba(255, 123, 88)"
        }],
        "paramsConfig": [],
        "stylesConfig": [{"align": "center"}, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            }
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#93D051"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#01B0F1"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#BFBFBF"
        }, {
            "align": "center",
            "border": {
                "top": ["thin", "#666"],
                "left": ["thin", "#666"],
                "right": ["thin", "#666"],
                "bottom": ["thin", "#666"]
            },
            "bgcolor": "#FFFFFF"
        }],
        "pageFreezeConfig": "A1",
        "toolbarButtonList": [{
            "icon": "fa fa-step-backward",
            "show": true,
            "text": "首页",
            "type": 1,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-left-filling",
            "show": true,
            "text": "上一页",
            "type": 2,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-file-text-o",
            "show": true,
            "text": "当前页/总页数",
            "type": 3,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "xzreport-icon-arrow-right-filling",
            "show": true,
            "text": "下一页",
            "type": 4,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-step-forward",
            "show": true,
            "text": "末页",
            "type": 5,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-print",
            "show": true,
            "text": "打印",
            "type": 6,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }, {
            "icon": "fa fa-download",
            "show": true,
            "text": "导出",
            "type": 7,
            "color": "#2c3e50",
            "showIcon": true,
            "showText": true
        }]
    }
}]
