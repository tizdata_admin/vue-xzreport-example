export const get_onUpdateDatasetlist= ()=>{
    let onUpdateDatasetlist = [
        {
            name: "组内排序",
            id: "组内排序",
            type:4,
            columns:
                [
                    {
                        name: "商品",                           //这些name：可以是中文？
                        data_type: 3
                    },
                    {
                        name: "季度",
                        data_type: 2
                    },
                    {
                        name: "销售额",
                        data_type: 2
                    },
                ],

            params:
                [
                    {
                        name: "商品",                           //这些name：可以是中文？
                        data_type: 3
                    },
                    {
                        name: "季度",
                        data_type: 2
                    },

                ],

        },

    ]
    return onUpdateDatasetlist;//深拷贝
}