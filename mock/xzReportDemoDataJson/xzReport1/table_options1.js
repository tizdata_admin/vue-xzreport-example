const options = {

  mode:'read',  //  报表模式  read | edit  只读模式|编辑模式
  view: {
    width: () => document.documentElement.clientWidth,
    height: () => document.documentElement.clientHeight
  },   // 设置报表的宽高
  renderArrow:false,  // 是否显式 扩展方向图标
  showFreeze:false,  // 是否显式冻结线
  showGrid:false   // 是否显式网格线
};
export {options}