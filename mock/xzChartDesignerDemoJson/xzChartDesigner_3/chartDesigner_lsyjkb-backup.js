let report = {
    "type": 2,
    "charts": [],
    "config": {
        "child": [
            {
                "id": "5190a1a5-a7d4-4b4e-a9bd-d7c687ee839f",
                "type": "standardBar",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "grid__top": 35,
                            "grid__bottom": 35,
                            "grid__left": 35,
                            "grid__right": 35
                        },
                        "xAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__interval": 0,
                            "axisLabel__margin": 10,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 15,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)"
                        },
                        "yAxis": {
                            "FormatObj__dataFormat__rateValue": -1,
                            "FormatObj__dataFormat__decimalValue": 0,
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "splitNumber": 0,
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "useYAxisMinMax": false,
                            "yAxisMin": 0,
                            "yAxisMax": 100
                        },
                        "customSeries": {
                            "show": false,
                            "labelFormatter": "默认",
                            "position": "top",
                            "align": "center",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            },
                            "rotate": 0,
                            "distance": 0
                        },
                        "tooltip": {
                            "showContent": true,
                            "formatterContent": "默认",
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "barStyle": {
                            "showBackground": false,
                            "backgroundColor": "rgba(180, 180, 180, 0.2)",
                            "barBorRadius": 1,
                            "barWidth": "38%"
                        },
                        "customColor": {
                            "color": [
                                "#538FE2",
                                "#89E5F9",
                                "#9DC6FF",
                                "#69A8FF",
                                "#9b8bba",
                                "#e098c7",
                                "#8fd3e8",
                                "#71669e",
                                "#cc70af",
                                "#7cb4cc"
                            ],
                            "use": true,
                            "colorStops": [
                                {
                                    "name": "度量一",
                                    "startColor": "rgba(136, 238, 221, 1)",
                                    "endColor": "rgba(0, 0, 0, 1)"
                                },
                                {
                                    "name": "度量二",
                                    "startColor": "rgba(136, 238, 221, 1)",
                                    "endColor": "rgba(0, 0, 0, 1)"
                                },
                                {
                                    "name": "度量三",
                                    "startColor": "rgba(136, 238, 221, 1)",
                                    "endColor": "rgba(0, 0, 0, 1)"
                                },
                                {
                                    "name": "度量四",
                                    "startColor": "rgba(136, 238, 221, 1)",
                                    "endColor": "rgba(0, 0, 0, 1)"
                                },
                                {
                                    "name": "金额",
                                    "startColor": "#ba69c8",
                                    "endColor": "#4a148c"
                                }
                            ]
                        },
                        "legend": {
                            "show": true,
                            "orient": "horizontal",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        }
                    },
                    "dataConfig": {
                        "data_name": "活动期间累计交易额",
                        "indexs": [
                            {
                                "name": "类型",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "type": "3",
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "金额",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 4,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "c81c4c31-b08b-428f-9fda-dc7cd1642a41"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": true,
                            "height": "67px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "活动期间累计交易额",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "textAlign": "left"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "1a84c20f-23ad-4a35-a601-5a8a8ee01232",
                "type": "BaseTable",
                "option": {
                    "chartConfig": {
                        "basicStyle": {
                            "style": 1,
                            "theme": "#70a5ef"
                        },
                        "head": {
                            "show": true,
                            "mulHeads": [],
                            "height": 25,
                            "wrapType": 0,
                            "textAlign": "center",
                            "textStyle": {
                                "fontFamily": "Arial",
                                "fontSize": "15px",
                                "fontWeight": "bold",
                                "fontStyle": "none",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "bgcolor": "#00000000",
                            "borderStyle": "thin",
                            "borderColor": "#00000000"
                        },
                        "body": {
                            "height": 50,
                            "wrapType": 0,
                            "textAlign": "center",
                            "textStyle": {
                                "fontFamily": "Arial",
                                "fontSize": "14px",
                                "fontWeight": "normal",
                                "fontStyle": "none",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "oddRowsBgcolor": "#00000000",
                            "evenRowsBgcolor": "#00000000",
                            "borderStyle": "thin",
                            "borderColor": "#00000000"
                        },
                        "basic": {
                            "widthType": 2,
                            "pagination__show": false,
                            "pagination__valign": "bottom",
                            "pagination__align": "right",
                            "pagination__showSize": true,
                            "pagination__size": 100,
                            "pagination__showPageCount": true,
                            "freeze": false,
                            "freezeCols": 1,
                            "numberColumns": false,
                            "numberColumnsWidth": 50,
                            "numberColumnsName": "序号",
                            "widthCols": [
                                "25%",
                                "25%",
                                "25%",
                                "25%"
                            ]
                        }
                    },
                    "dataConfig": {
                        "data_name": "表格",
                        "indexs": [],
                        "columns": [
                            {
                                "name": "商品编号",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "46b3ebe4-84bd-46a5-aa10-f82df992f20c"
                            },
                            {
                                "name": "库存",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "79a5bea3-e569-4f05-b212-0615adf9f7d3"
                            },
                            {
                                "name": "调价情况",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "0dc7df7d-833d-46ae-aeae-9c1f1138e843"
                            },
                            {
                                "name": "发卷情况",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "cde52fad-1c84-430a-b4dd-9dd327eea69d"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "26df61e4-5ffc-4d79-a8c6-f5ae4eaa0051",
                "type": "Area",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "grid__top": 40,
                            "grid__bottom": 40,
                            "grid__left": 40,
                            "grid__right": 40
                        },
                        "xAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__interval": 0,
                            "axisLabel__margin": 10,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)"
                        },
                        "yAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "splitNumber": 0,
                            "axisLabel__show": true,
                            "FormatObj__dataFormat__rateValue": -1,
                            "FormatObj__dataFormat__decimalValue": 0,
                            "axisLabel__rotate": 0,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "useYAxisMinMax": false,
                            "yAxisMin": 0,
                            "yAxisMax": 100
                        },
                        "tooltip": {
                            "showContent": true,
                            "formatterContent": "默认",
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            },
                            "axisPointer__lineStyle__color": "rgba(0, 0, 0, 0.6)"
                        },
                        "Linear": {
                            "colums_label": [
                                {
                                    "name": "度量一",
                                    "lineType": "solid",
                                    "lineWidth": 2,
                                    "showSymbol": true,
                                    "lineSymbolSize": 6,
                                    "borderWidth": 1,
                                    "borderColor": "#00000000",
                                    "useShadow": false,
                                    "shadowOffsetX": 2,
                                    "shadowOffsetY": 2,
                                    "shadowBlur": 0,
                                    "shadowColor": "rgba(0, 0, 0, 0.5)"
                                },
                                {
                                    "name": "度量二",
                                    "lineType": "solid",
                                    "lineWidth": 2,
                                    "showSymbol": true,
                                    "lineSymbolSize": 6,
                                    "borderWidth": 1,
                                    "borderColor": "#00000000",
                                    "useShadow": false,
                                    "shadowOffsetX": 2,
                                    "shadowOffsetY": 2,
                                    "shadowBlur": 0,
                                    "shadowColor": "rgba(0, 0, 0, 0.5)"
                                }
                            ]
                        },
                        "customSeries": {
                            "colums_label": [
                                {
                                    "name": "度量一",
                                    "show": true,
                                    "position": "top",
                                    "align": "center",
                                    "color": "rgba(0, 0, 0, 1)",
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "度量二",
                                    "show": true,
                                    "position": "top",
                                    "align": "center",
                                    "color": "rgba(0, 0, 0, 1)",
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "已下单",
                                    "show": false,
                                    "labelFormatter": "默认",
                                    "position": "top",
                                    "align": "center",
                                    "textStyle": {
                                        "fontFamily": "sans-serif",
                                        "fontSize": 12,
                                        "fontWeight": "normal",
                                        "fontStyle": "normal",
                                        "color": "rgba(96, 98, 102, 1)"
                                    },
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "已出库",
                                    "show": false,
                                    "labelFormatter": "默认",
                                    "position": "top",
                                    "align": "center",
                                    "textStyle": {
                                        "fontFamily": "sans-serif",
                                        "fontSize": 12,
                                        "fontWeight": "normal",
                                        "fontStyle": "normal",
                                        "color": "rgba(96, 98, 102, 1)"
                                    },
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "已完成",
                                    "show": false,
                                    "labelFormatter": "默认",
                                    "position": "top",
                                    "align": "center",
                                    "textStyle": {
                                        "fontFamily": "sans-serif",
                                        "fontSize": 12,
                                        "fontWeight": "normal",
                                        "fontStyle": "normal",
                                        "color": "rgba(96, 98, 102, 1)"
                                    },
                                    "rotate": 0,
                                    "distance": 0
                                }
                            ]
                        },
                        "customColor": {
                            "color": [
                                "#538FE2",
                                "#89E5F9",
                                "#9DC6FF",
                                "#69A8FF",
                                "#9b8bba",
                                "#e098c7",
                                "#8fd3e8",
                                "#71669e",
                                "#cc70af",
                                "#7cb4cc"
                            ]
                        },
                        "legend": {
                            "show": false,
                            "orient": "horizontal",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        }
                    },
                    "dataConfig": {
                        "data_name": "物流信息",
                        "indexs": [
                            {
                                "name": "星期",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "type": 3,
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "已下单",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 4,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "c9930fbd-b451-46e9-a159-7a0003de6cc2"
                            },
                            {
                                "name": "已出库 ",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 4,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "fb6d0db2-80ab-4ede-95aa-4b58d26a47f5"
                            },
                            {
                                "name": "已完成",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 4,
                                "type": "3",
                                "noSuchField": false,
                                "uuid": "077af2ca-bea0-4fbd-800d-0525f0e3b7bb"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": true,
                            "height": "47px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "活动期间物流信息",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "textAlign": "left"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "869db07c-2761-41a2-bc5f-ce17661636b4",
                "type": "Radar",
                "option": {
                    "chartConfig": {
                        "chartBase": {
                            "centerX": "50%",
                            "centerY": "50%"
                        },
                        "radar": {
                            "radius": "80%",
                            "shape": "polygon",
                            "name__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "#ffffff",
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "#ffffff",
                            "areaStyleColor": "rgba(48, 48, 48, 0)"
                        },
                        "tooltip": {
                            "showContent": true,
                            "formatterContent": "默认",
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "customColor": {
                            "color": [
                                "#538FE2",
                                "#89E5F9",
                                "#9DC6FF",
                                "#69A8FF",
                                "#9b8bba",
                                "#e098c7",
                                "#8fd3e8",
                                "#71669e",
                                "#cc70af",
                                "#7cb4cc"
                            ]
                        },
                        "legend": {
                            "show": true,
                            "orient": "vertical",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        }
                    },
                    "dataConfig": {
                        "data_name": "活动期间累计交易额  雷达图",
                        "indexs": [
                            {
                                "name": "类型",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "type": "3",
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "Mon",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "999f6f88-f72c-46de-9020-9f459add90bf"
                            },
                            {
                                "name": "Tues",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "e4f50ba5-df72-457e-a085-ab39c55160af"
                            },
                            {
                                "name": "Wed",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "da75b062-d4ae-4206-bf03-a18b828208ad"
                            },
                            {
                                "name": "Thur",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "a0e05a80-f1e9-4272-a8fd-2ddac14dc2b9"
                            },
                            {
                                "name": "Fr",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "e9242476-9128-487e-9b0e-cdaea10a8ee7"
                            },
                            {
                                "name": "Sat",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "87e4c3e9-02ec-43b2-bf01-68ac36ed4e9b"
                            },
                            {
                                "name": "Sun",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "ebc200e8-8f86-47bf-a5bd-73814be35d22"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "雷达图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "6c628138-7537-498b-84b5-a2cc3dc4cc8d",
                "type": "YBar",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "grid__top": 35,
                            "grid__bottom": 35,
                            "grid__left": 35,
                            "grid__right": 35
                        },
                        "xAxis": {
                            "FormatObj__dataFormat__rateValue": -1,
                            "FormatObj__dataFormat__decimalValue": 0,
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "splitNumber": 0,
                            "axisLabel__show": false,
                            "axisLabel__rotate": 0,
                            "axisLabel__interval": 0,
                            "axisLabel__margin": 10,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "useYAxisMinMax": false,
                            "yAxisMin": 0,
                            "yAxisMax": 100
                        },
                        "yAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)"
                        },
                        "customSeries": {
                            "show": true,
                            "labelFormatter": "默认",
                            "position": "inside",
                            "align": "center",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 19,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "rotate": 0,
                            "distance": 0
                        },
                        "tooltip": {
                            "showContent": true,
                            "formatterContent": "自定义**/*undefined",
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "barStyle": {
                            "showBackground": false,
                            "backgroundColor": "rgba(180, 180, 180, 0.2)",
                            "barBorRadius": 2,
                            "barWidth": "39%"
                        },
                        "customColor": {
                            "color": [
                                "#8f2cdc",
                                "#89E5F9",
                                "#9DC6FF",
                                "#69A8FF",
                                "#9b8bba",
                                "#e098c7",
                                "#8fd3e8",
                                "#71669e",
                                "#cc70af",
                                "#7cb4cc"
                            ]
                        },
                        "legend": {
                            "show": false,
                            "orient": "horizontal",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        }
                    },
                    "dataConfig": {
                        "data_name": "商品",
                        "indexs": [
                            {
                                "name": "商品",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "type": "3",
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "好评",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 4,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "c3c82c4a-608b-415b-8369-6ff6dbd386fc"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "标准条形图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "e0b5b64f-7f17-4456-a54c-6665c958657e",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/005.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": true,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "e661e6c9-a800-4b0e-a496-3e26000e8fc9",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "零售业绩看板",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "56px",
                                "fontWeight": "bold",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "526acf5d-09c0-49a6-8b1e-450831e0ab89",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "￥6,9602万元",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "50px",
                                "fontWeight": "bold",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#0c54e7"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": true,
                            "height": "50px",
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "活动期间交易金额",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "textAlign": "left"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "c4e7e79e-0a49-4f66-b985-960b5ca0fd80",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "￥6,9602万元",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "none",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "60px",
                                "fontWeight": "bold",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#0f215c",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": true,
                            "height": "50px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "活动期间交易额",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "textAlign": "left"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "fed9435b-4953-4999-a614-e99a9a1d3015",
                "type": "Ring",
                "option": {
                    "chartConfig": {
                        "custom": {
                            "centerX": "50%",
                            "centerY": "50%",
                            "radiusOut": "90%",
                            "rZeroWidth": 14,
                            "rZeroColor": "rgba(238, 238, 238, 1)",
                            "rOneWidth": 18,
                            "rOneColor": "#538FE2",
                            "rBgWidht": 3,
                            "rBgColor": "#9DC6FF",
                            "rBgInsideWidht": 5,
                            "rBgInsideColor": "#89E5F9",
                            "titleShow": false,
                            "titlePosition": false,
                            "textY": 20,
                            "titleSpacing": 0,
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "rgba(0, 193, 222, 1)"
                            },
                            "valShow": true,
                            "valueTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "rgba(0, 193, 222, 1)"
                            }
                        },
                        "customValue": {
                            "isAuto": true,
                            "maxValue": 100
                        },
                        "tooltip": {
                            "showContent": true,
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "customDivisor": 100
                    },
                    "dataConfig": {
                        "data_name": "活动期间累计交易额 增长率",
                        "indexs": [],
                        "columns": [
                            {
                                "name": "实际值",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "e8d69430-60e0-4e86-a573-19acbd17629f"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "环形图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "0fa716bf-e71d-42fc-be95-fe70d864e77a",
                "type": "Ring",
                "option": {
                    "chartConfig": {
                        "custom": {
                            "centerX": "50%",
                            "centerY": "50%",
                            "radiusOut": "90%",
                            "rZeroWidth": 14,
                            "rZeroColor": "rgba(238, 238, 238, 1)",
                            "rOneWidth": 18,
                            "rOneColor": "#538FE2",
                            "rBgWidht": 3,
                            "rBgColor": "#9DC6FF",
                            "rBgInsideWidht": 5,
                            "rBgInsideColor": "#89E5F9",
                            "titleShow": false,
                            "titlePosition": false,
                            "textY": 20,
                            "titleSpacing": 0,
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "rgba(0, 193, 222, 1)"
                            },
                            "valShow": true,
                            "valueTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "rgba(0, 193, 222, 1)"
                            }
                        },
                        "customValue": {
                            "isAuto": true,
                            "maxValue": 100
                        },
                        "tooltip": {
                            "showContent": true,
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "customDivisor": 100
                    },
                    "dataConfig": {
                        "data_name": "活动期间累计交易额 增长率",
                        "indexs": [],
                        "columns": [
                            {
                                "name": "实际值",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 0,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "e8d69430-60e0-4e86-a573-19acbd17629f"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "环形图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "b2300dca-2f82-4f8d-ba2a-a65d85ed30ba",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/004.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": true,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "08da9adf-c226-4a6c-a522-bedf5af86506",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/001.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": true,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "0efdd62a-0d2d-4b27-85dd-fe36fd1447b2",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/001.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": true,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "756df7af-e102-44f3-9ffd-23cc29c88677",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/001.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": true,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "d76def95-88f6-4e1e-9f09-38a897648c27",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "类目一",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "1cf79c99-0b80-4c2c-879c-061bfdd32b8d",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/007.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": false,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "af39451c-02bd-4d0f-a787-f0f3d12b0ebf",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/007.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": false,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "85debf3f-4fd6-4444-9cd7-76af7a8fee0e",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/007.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": false,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "e8036782-5201-423a-8d2b-28f0426a4bdb",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/007.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": false,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "9aa71f6f-29c1-4984-bd12-40f3010f9232",
                "type": "ImageData",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "src": "https://cdn.tizdata.com/muban/daping/6/007.png",
                            "paddingTop": 10,
                            "paddingBottom": 10,
                            "paddingLeft": 10,
                            "paddingRight": 10
                        },
                        "imageText": {
                            "titleText": "图片文字",
                            "textIsShow": false,
                            "textPosition": false,
                            "textBackground": "rgb(255, 255, 255)",
                            "textHeight": 30,
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 20,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            }
                        },
                        "image": {
                            "imgStyle__backgroundStyle": 1,
                            "imageBorder": 0,
                            "imgStyle__borderColor": "rgba(0, 0, 0, 1)"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通图片",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "9f485e13-a6b7-4ffc-b939-b298d39c21c0",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "类目二",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "539c8147-b0c8-4cca-9e99-366fc2f57541",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "类目三",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "dd99bbdc-75a0-4cd9-b670-3624c28acc7b",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "类目四",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "586c9ac2-49f5-4e24-91e3-a9629ac7fd7f",
                "type": "Label",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "txt": "类目五",
                            "linkText": ""
                        },
                        "textStyle": {
                            "display": "flex",
                            "backgroundColor": "rgba(27, 28, 28, 0)",
                            "bgGradient": false,
                            "startColor": "rgba(0, 157, 255, 1)",
                            "endColor": "rgba(11, 239, 239, 1)",
                            "deg": "right",
                            "txtStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": "20px",
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#ffffff"
                            },
                            "justifyContent": "center",
                            "alignItems": "center"
                        }
                    },
                    "dataConfig": {
                        "data_name": null,
                        "indexs": [],
                        "columns": [],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 3
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "普通文本",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            },
            {
                "id": "345b73d7-4ae2-4679-a03d-e375c64f788a",
                "type": "Line",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "grid__top": 40,
                            "grid__bottom": 40,
                            "grid__left": 40,
                            "grid__right": 40
                        },
                        "xAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__interval": 0,
                            "axisLabel__margin": 10,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)"
                        },
                        "yAxis": {
                            "FormatObj__dataFormat__rateValue": -1,
                            "FormatObj__dataFormat__decimalValue": 0,
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": false,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "splitNumber": 0,
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "#ffffff"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "useYAxisMinMax": false,
                            "yAxisMin": 0,
                            "yAxisMax": 100
                        },
                        "tooltip": {
                            "showContent": true,
                            "formatterContent": "默认",
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "Linear": {
                            "colums_label": [
                                {
                                    "name": "度量一",
                                    "lineType": "solid",
                                    "lineWidth": 2,
                                    "showSymbol": true,
                                    "lineSymbolSize": 6,
                                    "borderWidth": 1,
                                    "borderColor": "#00000000",
                                    "useShadow": false,
                                    "shadowOffsetX": 2,
                                    "shadowOffsetY": 2,
                                    "shadowBlur": 0,
                                    "shadowColor": "rgba(0, 0, 0, 0.5)"
                                },
                                {
                                    "name": "度量二",
                                    "lineType": "solid",
                                    "lineWidth": 2,
                                    "showSymbol": true,
                                    "lineSymbolSize": 6,
                                    "borderWidth": 1,
                                    "borderColor": "#00000000",
                                    "useShadow": false,
                                    "shadowOffsetX": 2,
                                    "shadowOffsetY": 2,
                                    "shadowBlur": 0,
                                    "shadowColor": "rgba(0, 0, 0, 0.5)"
                                }
                            ]
                        },
                        "customSeries": {
                            "colums_label": [
                                {
                                    "name": "度量一",
                                    "show": true,
                                    "position": "top",
                                    "align": "center",
                                    "color": "rgba(0, 0, 0, 1)",
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "度量二",
                                    "show": true,
                                    "position": "top",
                                    "align": "center",
                                    "color": "rgba(0, 0, 0, 1)",
                                    "rotate": 0,
                                    "distance": 0
                                },
                                {
                                    "name": "好评",
                                    "show": true,
                                    "labelFormatter": "默认",
                                    "position": "top",
                                    "align": "center",
                                    "textStyle": {
                                        "fontFamily": "sans-serif",
                                        "fontSize": 20,
                                        "fontWeight": "normal",
                                        "fontStyle": "normal",
                                        "color": "#ffffff"
                                    },
                                    "rotate": 0,
                                    "distance": 6
                                }
                            ]
                        },
                        "customColor": {
                            "color": [
                                "#538FE2",
                                "#89E5F9",
                                "#9DC6FF",
                                "#69A8FF",
                                "#9b8bba",
                                "#e098c7",
                                "#8fd3e8",
                                "#71669e",
                                "#cc70af",
                                "#7cb4cc"
                            ]
                        },
                        "legend": {
                            "show": false,
                            "orient": "horizontal",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        }
                    },
                    "dataConfig": {
                        "data_name": "商品",
                        "indexs": [
                            {
                                "name": "商品",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "type": "3",
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "好评",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none"
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "eaf22266-4972-4a14-8a08-356557577999"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 1
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": false,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "content": "标准折线图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            }
        ],
        "layout": {
            "report": {
                "width": "1920px",
                "height": "1080px",
                "padding": "0px",
                "single_height": "1080px",
                "backgroundColor": "#04082e",
                "backgroundImage": "",
                "backgroundSize": 1
            },
            "5190a1a5-a7d4-4b4e-a9bd-d7c687ee839f": {
                "width": "552px",
                "height": "370px",
                "zIndex": "969",
                "transform": "translate(16px, 340px)",
                "display": ""
            },
            "1a84c20f-23ad-4a35-a601-5a8a8ee01232": {
                "width": "555px",
                "height": "327px",
                "zIndex": "970",
                "transform": "translate(15px, 733px)",
                "display": ""
            },
            "26df61e4-5ffc-4d79-a8c6-f5ae4eaa0051": {
                "width": "452px",
                "height": "332px",
                "zIndex": "975",
                "transform": "translate(576px, 732px)",
                "display": ""
            },
            "869db07c-2761-41a2-bc5f-ce17661636b4": {
                "width": "422px",
                "height": "358px",
                "zIndex": "981",
                "transform": "translate(1248px, 410px)",
                "display": ""
            },
            "6c628138-7537-498b-84b5-a2cc3dc4cc8d": {
                "width": "682px",
                "height": "310px",
                "zIndex": "977",
                "transform": "translate(1240px, 764px)",
                "display": ""
            },
            "e0b5b64f-7f17-4456-a54c-6665c958657e": {
                "width": "1934px",
                "height": "116px",
                "zIndex": "984",
                "transform": "translate(-8px, -10px)",
                "display": ""
            },
            "e661e6c9-a800-4b0e-a496-3e26000e8fc9": {
                "width": "394px",
                "height": "162px",
                "zIndex": "985",
                "transform": "translate(752px, -30px)",
                "display": ""
            },
            "526acf5d-09c0-49a6-8b1e-450831e0ab89": {
                "width": "556px",
                "height": "210px",
                "zIndex": "971",
                "transform": "translate(20px, 112px)",
                "display": ""
            },
            "c4e7e79e-0a49-4f66-b985-960b5ca0fd80": {
                "width": "696px",
                "height": "658px",
                "zIndex": "976",
                "transform": "translate(1226px, 112px)",
                "display": ""
            },
            "fed9435b-4953-4999-a614-e99a9a1d3015": {
                "width": "250px",
                "height": "152px",
                "zIndex": "978",
                "transform": "translate(1668px, 574px)",
                "display": ""
            },
            "0fa716bf-e71d-42fc-be95-fe70d864e77a": {
                "width": "250px",
                "height": "152px",
                "zIndex": "979",
                "transform": "translate(1668px, 400px)",
                "display": ""
            },
            "b2300dca-2f82-4f8d-ba2a-a65d85ed30ba": {
                "width": "1818px",
                "height": "1060px",
                "zIndex": "968",
                "transform": "translate(10px, -10px)",
                "display": ""
            },
            "08da9adf-c226-4a6c-a522-bedf5af86506": {
                "width": "322px",
                "height": "47px",
                "zIndex": "972",
                "transform": "translate(-1.33334px, 133px) rotate(719.944deg)",
                "display": ""
            },
            "0efdd62a-0d2d-4b27-85dd-fe36fd1447b2": {
                "width": "322px",
                "height": "47px",
                "zIndex": "973",
                "transform": "translate(-2.44444px, 364.778px)",
                "display": ""
            },
            "756df7af-e102-44f3-9ffd-23cc29c88677": {
                "width": "322px",
                "height": "47px",
                "zIndex": "980",
                "transform": "translate(1215.11px, 130.111px)",
                "display": ""
            },
            "d76def95-88f6-4e1e-9f09-38a897648c27": {
                "width": "96px",
                "height": "68px",
                "zIndex": "988",
                "transform": "translate(578px, 104px)",
                "display": ""
            },
            "1cf79c99-0b80-4c2c-879c-061bfdd32b8d": {
                "width": "92px",
                "height": "92px",
                "zIndex": "987",
                "transform": "translate(580px, 92px)",
                "display": ""
            },
            "af39451c-02bd-4d0f-a787-f0f3d12b0ebf": {
                "width": "92px",
                "height": "92px",
                "zIndex": "989",
                "transform": "translate(580px, 176px)",
                "display": ""
            },
            "85debf3f-4fd6-4444-9cd7-76af7a8fee0e": {
                "width": "92px",
                "height": "92px",
                "zIndex": "990",
                "transform": "translate(582px, 274px)",
                "display": ""
            },
            "e8036782-5201-423a-8d2b-28f0426a4bdb": {
                "width": "92px",
                "height": "92px",
                "zIndex": "991",
                "transform": "translate(1142px, 846px)",
                "display": ""
            },
            "9aa71f6f-29c1-4984-bd12-40f3010f9232": {
                "width": "92px",
                "height": "92px",
                "zIndex": "992",
                "transform": "translate(1146px, 950px)",
                "display": ""
            },
            "9f485e13-a6b7-4ffc-b939-b298d39c21c0": {
                "width": "96px",
                "height": "68px",
                "zIndex": "993",
                "transform": "translate(582px, 188px)",
                "display": ""
            },
            "539c8147-b0c8-4cca-9e99-366fc2f57541": {
                "width": "96px",
                "height": "68px",
                "zIndex": "994",
                "transform": "translate(578px, 286px)",
                "display": ""
            },
            "dd99bbdc-75a0-4cd9-b670-3624c28acc7b": {
                "width": "96px",
                "height": "68px",
                "zIndex": "995",
                "transform": "translate(1138px, 862px)",
                "display": ""
            },
            "586c9ac2-49f5-4e24-91e3-a9629ac7fd7f": {
                "width": "96px",
                "height": "68px",
                "zIndex": "996",
                "transform": "translate(1144px, 962px)",
                "display": ""
            },
            "345b73d7-4ae2-4679-a03d-e375c64f788a": {
                "width": "656px",
                "height": "218px",
                "zIndex": "982",
                "transform": "translate(1238px, 156px)",
                "display": ""
            }
        },
        "controlLayer": [
            {
                "id": "46b71d",
                "c_type": 2,
                "zIndex": 997,
                "name": "类目",
                "chartShow": true,
                "chartLock": false,
                "chartOpen": false,
                "random": "",
                "children": [
                    {
                        "id": "586c9ac2-49f5-4e24-91e3-a9629ac7fd7f",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 996,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "d872eb"
                    },
                    {
                        "id": "dd99bbdc-75a0-4cd9-b670-3624c28acc7b",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 995,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "43b0eb"
                    },
                    {
                        "id": "539c8147-b0c8-4cca-9e99-366fc2f57541",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 994,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "1a2c35"
                    },
                    {
                        "id": "9f485e13-a6b7-4ffc-b939-b298d39c21c0",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 993,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "86773b"
                    },
                    {
                        "id": "9aa71f6f-29c1-4984-bd12-40f3010f9232",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 992,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "09c716"
                    },
                    {
                        "id": "e8036782-5201-423a-8d2b-28f0426a4bdb",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 991,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "876ad3"
                    },
                    {
                        "id": "85debf3f-4fd6-4444-9cd7-76af7a8fee0e",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 990,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "08355c"
                    },
                    {
                        "id": "af39451c-02bd-4d0f-a787-f0f3d12b0ebf",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 989,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "835a27"
                    },
                    {
                        "id": "d76def95-88f6-4e1e-9f09-38a897648c27",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 988,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "fe75be"
                    },
                    {
                        "id": "1cf79c99-0b80-4c2c-879c-061bfdd32b8d",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 987,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "e61861"
                    }
                ]
            },
            {
                "id": "774044",
                "c_type": 2,
                "zIndex": 986,
                "name": "头部",
                "chartShow": true,
                "chartLock": false,
                "chartOpen": false,
                "random": "",
                "children": [
                    {
                        "id": "e661e6c9-a800-4b0e-a496-3e26000e8fc9",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 985,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "33f7fd"
                    },
                    {
                        "id": "e0b5b64f-7f17-4456-a54c-6665c958657e",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 984,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "afd8f7"
                    }
                ]
            },
            {
                "id": "c445c5",
                "c_type": 2,
                "zIndex": 983,
                "name": "右侧",
                "chartShow": true,
                "chartLock": false,
                "chartOpen": false,
                "random": "",
                "children": [
                    {
                        "id": "345b73d7-4ae2-4679-a03d-e375c64f788a",
                        "type": "Line",
                        "name": "标准折线图",
                        "img_url": "Line.png",
                        "zIndex": 982,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "87187d"
                    },
                    {
                        "id": "869db07c-2761-41a2-bc5f-ce17661636b4",
                        "type": "Radar",
                        "name": "雷达图",
                        "img_url": "c_22.png",
                        "zIndex": 981,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "20ff56"
                    },
                    {
                        "id": "756df7af-e102-44f3-9ffd-23cc29c88677",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 980,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "0ec867"
                    },
                    {
                        "id": "0fa716bf-e71d-42fc-be95-fe70d864e77a",
                        "type": "Ring",
                        "name": "环形图",
                        "img_url": "Ring.png",
                        "zIndex": 979,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "2c610e"
                    },
                    {
                        "id": "fed9435b-4953-4999-a614-e99a9a1d3015",
                        "type": "Ring",
                        "name": "环形图",
                        "img_url": "Ring.png",
                        "zIndex": 978,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "f3732b"
                    },
                    {
                        "id": "6c628138-7537-498b-84b5-a2cc3dc4cc8d",
                        "type": "YBar",
                        "name": "标准条形图",
                        "img_url": "YBar.png",
                        "zIndex": 977,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "b6b1bb"
                    },
                    {
                        "id": "c4e7e79e-0a49-4f66-b985-960b5ca0fd80",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 976,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "d7f4a4"
                    }
                ]
            },
            {
                "id": "26df61e4-5ffc-4d79-a8c6-f5ae4eaa0051",
                "type": "Area",
                "name": "标准区域图",
                "img_url": "Area.png",
                "zIndex": 975,
                "c_type": 1,
                "chartShow": true,
                "chartLock": false,
                "random": "632643"
            },
            {
                "id": "a6353d",
                "c_type": 2,
                "zIndex": 974,
                "name": "左侧",
                "chartShow": true,
                "chartLock": false,
                "chartOpen": false,
                "random": "",
                "children": [
                    {
                        "id": "0efdd62a-0d2d-4b27-85dd-fe36fd1447b2",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 973,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "79c8fa"
                    },
                    {
                        "id": "08da9adf-c226-4a6c-a522-bedf5af86506",
                        "type": "ImageData",
                        "name": "普通图片",
                        "img_url": "ImageData.png",
                        "zIndex": 972,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "ed56ed"
                    },
                    {
                        "id": "526acf5d-09c0-49a6-8b1e-450831e0ab89",
                        "type": "Label",
                        "name": "普通文本",
                        "img_url": "Label.png",
                        "zIndex": 971,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "01c8fa"
                    },
                    {
                        "id": "1a84c20f-23ad-4a35-a601-5a8a8ee01232",
                        "type": "BaseTable",
                        "name": "明细表格",
                        "img_url": "BaseTable.png",
                        "zIndex": 970,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "5c70e4"
                    },
                    {
                        "id": "5190a1a5-a7d4-4b4e-a9bd-d7c687ee839f",
                        "type": "standardBar",
                        "name": "标准柱状图",
                        "img_url": "bar.png",
                        "zIndex": 969,
                        "c_type": 1,
                        "chartShow": true,
                        "chartLock": false,
                        "random": "263f0a"
                    }
                ]
            },
            {
                "id": "b2300dca-2f82-4f8d-ba2a-a65d85ed30ba",
                "type": "ImageData",
                "name": "地球背景",
                "img_url": "ImageData.png",
                "zIndex": 968,
                "c_type": 1,
                "chartShow": true,
                "chartLock": false,
                "random": ""
            }
        ]
    },
    "data": {
        "dataConfig": [
            {
                "name": "活动期间累计交易额",
                "response_data": [
                    {
                        "金额": "600",
                        "类型": "类型1"
                    },
                    {
                        "金额": "800",
                        "类型": "类型2"
                    },
                    {
                        "金额": "994",
                        "类型": "类型3"
                    },
                    {
                        "金额": "995",
                        "类型": "类型4"
                    },
                    {
                        "金额": "400",
                        "类型": "类型5"
                    },
                    {
                        "金额": "500",
                        "类型": "类型6"
                    },
                    {
                        "金额": "998",
                        "类型": "类型7"
                    },
                    {
                        "金额": "300",
                        "类型": "类型8"
                    },
                    {
                        "金额": "1000",
                        "类型": "类型9"
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "金额",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "类型",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "活动期间累计交易额 增长率",
                "response_data": [
                    {
                        "实际值": 22.8
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "实际值",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "表格",
                "response_data": [
                    {
                        "商品编号": "商品357",
                        "库存": "410",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品712",
                        "库存": "100",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品454",
                        "库存": "316",
                        "调价情况": "已调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品741",
                        "库存": "739",
                        "调价情况": "未调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品455",
                        "库存": "954",
                        "调价情况": "未调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品694",
                        "库存": "383",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品639",
                        "库存": "602",
                        "调价情况": "已调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品608",
                        "库存": "377",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品357",
                        "库存": "411",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品712",
                        "库存": "101",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品454",
                        "库存": "311",
                        "调价情况": "已调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品741",
                        "库存": "731",
                        "调价情况": "未调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品455",
                        "库存": "951",
                        "调价情况": "未调价",
                        "发卷情况": "未发卷"
                    },
                    {
                        "商品编号": "商品694",
                        "库存": "381",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品639",
                        "库存": "601",
                        "调价情况": "已调价",
                        "发卷情况": "已发卷"
                    },
                    {
                        "商品编号": "商品608",
                        "库存": "371",
                        "调价情况": "未调价",
                        "发卷情况": "已发卷"
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "商品编号",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "库存",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "调价情况",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "发卷情况",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "物流信息",
                "response_data": [
                    {
                        "星期": "周一",
                        "已下单": "992",
                        "已出库 ": "660",
                        "已完成": "600"
                    },
                    {
                        "星期": "周二",
                        "已下单": "910",
                        "已出库 ": "710",
                        "已完成": "700"
                    },
                    {
                        "星期": "周三",
                        "已下单": "920",
                        "已出库 ": "900",
                        "已完成": "800"
                    },
                    {
                        "星期": "周四",
                        "已下单": "660",
                        "已出库 ": "600",
                        "已完成": "550"
                    },
                    {
                        "星期": "周五",
                        "已下单": "303",
                        "已出库 ": "300",
                        "已完成": "150"
                    },
                    {
                        "星期": "周六",
                        "已下单": "550",
                        "已出库 ": "500",
                        "已完成": "250"
                    },
                    {
                        "星期": "周日",
                        "已下单": "551",
                        "已出库 ": "400",
                        "已完成": "200"
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "星期",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "已下单",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "已出库 ",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "已完成",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "活动期间累计交易额 累计销售金额",
                "response_data": [
                    {
                        "累计销售金额": "582",
                        "星期": "Mon"
                    },
                    {
                        "累计销售金额": "828",
                        "星期": "Tues"
                    },
                    {
                        "累计销售金额": "525",
                        "星期": "Wed"
                    },
                    {
                        "累计销售金额": "400",
                        "星期": "Thur"
                    },
                    {
                        "累计销售金额": "500",
                        "星期": "Fr"
                    },
                    {
                        "累计销售金额": "800",
                        "星期": "Sat"
                    },
                    {
                        "累计销售金额": "700",
                        "星期": "Sun"
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "累计销售金额",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "星期",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "活动期间累计交易额  雷达图",
                "response_data": [
                    {
                        "类型": "已下单",
                        "Mon": 992,
                        "Tues": 910,
                        "Wed": 920,
                        "Thur": 660,
                        "Fr": 303,
                        "Sat": 550,
                        "Sun": 551
                    },
                    {
                        "类型": "已完成",
                        "Mon": 600,
                        "Tues": 700,
                        "Wed": 800,
                        "Thur": 550,
                        "Fr": 150,
                        "Sat": 250,
                        "Sun": 200
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "类型",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Mon",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Tues",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Wed",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Thur",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Fr",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Sat",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "Sun",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            },
            {
                "name": "商品",
                "response_data": [
                    {
                        "商品": "商品678",
                        "好评": 99.64
                    },
                    {
                        "商品": "商品366",
                        "好评": 100.64
                    },
                    {
                        "商品": "商品876",
                        "好评": 101.64
                    },
                    {
                        "商品": "商品478",
                        "好评": 102.64
                    },
                    {
                        "商品": "商品980",
                        "好评": 103.64
                    }
                ],
                "type": 1,
                "fields": [
                    {
                        "name": "商品",
                        "data_type": 3,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    },
                    {
                        "name": "好评",
                        "data_type": 2,
                        "axis_type": 1,
                        "format": null,
                        "sort": 0
                    }
                ],
                "params": []
            }
        ],
        "apiConfig": {}
    },
    "title": "小智BI_Demo展示"
}
export {report}