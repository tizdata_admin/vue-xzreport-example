const dataset= [ //  添加数据集按钮
    { //Json数据
        id: 1,
        value: "新建Json数据集"      //缺点是：新建的数据结构只能在页面-》选择新建json数据集，然后粘贴；而是不可以动态的从后台获取特定的几个数据集
    },
    { //http请求数据
        id: 2,
        value: "新建Http接口数据集"
    },
    { //API服务数据
        id: 3,
        value: "API服务数据集"
    },
    {
        id: 4,
        value: "外部数据集"  //  新建一个新的报表实例，如果希望可以预先动态从后台提供几个数据集供其使用，这个配置必须有；
        // 然后只要动态的从后台拿到n个数据集，全部用 results格式装配，onUpdateDatasetList方法来触发
    }
];
const option = {
    row: 100,
    col: 25,
    // width:()=>width,
    // height:()=>height
    dataset
}
export {option}