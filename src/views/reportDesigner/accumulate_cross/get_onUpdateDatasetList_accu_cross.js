let onUpdateDatasetlist = [
    {
        name: "交叉累计报表",
        id: "交叉累计报表",
        type:4,
        columns:
            [
                {
                    name: "商品",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "年份",
                    data_type: 2
                },
                {
                    name: "季度",
                    data_type: 2
                },
                {
                    name: "销售额",
                    data_type: 2
                },
            ],

        params:
            [
                {
                    name: "商品",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "年份",
                    data_type: 2
                },

            ],

    },
]
import {deepCopy} from "@/utils/xzToolsUtil.js";
export const get_onUpdateDatasetlist= ()=>{

    return Array.from(onUpdateDatasetlist);//深拷贝
}