let report_lsyjkb = {
    "type": 2,
    "charts": [],
    "config": {
        "child": [
            {
                "id": "d6aa3b9d-ff08-4b21-85b8-842b0da308b1",
                "type": "GroupBar",
                "option": {
                    "chartConfig": {
                        "chartBasic": {
                            "grid__top": 35,
                            "grid__bottom": 35,
                            "grid__left": 35,
                            "grid__right": 35,
                            "itemStyle__borderRadius": 0,
                            "itemStyle__customRadius": {
                                "enable": {
                                    "topLeft": 0,
                                    "topRight": 0,
                                    "bottomRight": 0,
                                    "bottomLeft": 0
                                },
                                "useVal": true
                            },
                            "barCategoryGap": "30%",
                            "barGap": "120%",
                            "barWidth": 30
                        },
                        "legend": {
                            "show": true,
                            "orient": "horizontal",
                            "OffsetStartX": "left",
                            "OffsetStartY": "top",
                            "leftDistance": 0,
                            "topDistance": 0,
                            "selectedMode": "multiple",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "itemWidth": 28,
                            "itemHeight": 14
                        },
                        "group": {
                            "splitLine__show": true,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "text__show": true,
                            "text__rotation": 0,
                            "text__margin": 20,
                            "text__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            }
                        },
                        "xAxis": {
                            "axisLine__show": true,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "text__show": true,
                            "text__rotation": 0,
                            "text__margin": 10,
                            "text__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            }
                        },
                        "yAxis": {
                            "axisLine__show": false,
                            "axisLine__lineStyle__width": 1,
                            "axisLine__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisTick__show": true,
                            "axisTick__length": 5,
                            "axisTick__lineStyle__width": 1,
                            "axisTick__lineStyle__color": "rgba(0, 0, 0, 1)",
                            "axisLabel__show": true,
                            "axisLabel__rotate": 0,
                            "axisLabel__textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "splitLine__show": false,
                            "splitLine__lineStyle__width": 1,
                            "splitLine__lineStyle__color": "rgba(80, 80, 80, 1)",
                            "splitNumber": 0
                        },
                        "customSeries": {
                            "show": false,
                            "position": "top",
                            "align": "center",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "rotate": 0,
                            "distance": 0
                        },
                        "tooltip": {
                            "showContent": true,
                            "backgroundColor": "rgba(0, 0, 0, 1)",
                            "textStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(155, 155, 155, 1)"
                            }
                        },
                        "customColor": {
                            "newColor": [
                                {
                                    "seriesName": "度量一",
                                    "color": "#538fe2"
                                },
                                {
                                    "seriesName": "度量二",
                                    "color": "#89e5f9"
                                },
                                {
                                    "seriesName": "度量三",
                                    "color": "#9b8bba"
                                },
                                {
                                    "seriesName": "度量四",
                                    "color": "#69a8ff"
                                },
                                {
                                    "seriesName": "度量五",
                                    "color": "#9dc6ff"
                                }
                            ]
                        },
                        "markLineEvent": {
                            "markLine": []
                        }
                    },
                    "dataConfig": {
                        "data_name": "商品销售",
                        "indexs": [
                            {
                                "name": "商品名称",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none",
                                    "valueType": 1,
                                    "scaleType": 1,
                                    "selectorFormat": {
                                        "show": 1,
                                        "allShow": 0,
                                        "allName": "全部"
                                    }
                                },
                                "type": "3",
                                "noSuchField": false
                            },
                            {
                                "name": "区域名称",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none",
                                    "valueType": 1,
                                    "scaleType": 1,
                                    "selectorFormat": {
                                        "show": 1,
                                        "allShow": 0,
                                        "allName": "全部"
                                    }
                                },
                                "type": "3",
                                "noSuchField": false
                            }
                        ],
                        "columns": [
                            {
                                "name": "销售额",
                                "sort": 0,
                                "format": {
                                    "showData": 1,
                                    "quantile": 0,
                                    "multiplying": 0,
                                    "decimal": 5,
                                    "date": "yyyy-MM-dd",
                                    "removePrefix": "",
                                    "removeSuffix": "",
                                    "addPrefix": "",
                                    "addSuffix": "",
                                    "location": -1,
                                    "replaceNull": {
                                        "type": 0,
                                        "value": ""
                                    },
                                    "newName": "",
                                    "region": "none",
                                    "valueType": 1,
                                    "scaleType": 1,
                                    "selectorFormat": {
                                        "show": 1,
                                        "allShow": 0,
                                        "allName": "全部"
                                    }
                                },
                                "aggregate": 1,
                                "type": "2",
                                "noSuchField": false,
                                "uuid": "9bb712a8-512d-4401-a382-d10c126b3eef"
                            }
                        ],
                        "selectors": [],
                        "filters": [],
                        "tooltips": [],
                        "type": 4,
                        "data_id": "商品销售id"
                    },
                    "eventConfig": {
                        "responseEvent": {
                            "linkage_open": true,
                            "linkage_errorType": 1,
                            "linkage_promptText": "无此联动值"
                        },
                        "clickEvent": {
                            "events": []
                        },
                        "toolbarEvent": {
                            "toolbar": []
                        }
                    },
                    "dynamicConfig": {
                        "marquee": {
                            "open": false,
                            "interval": 1000,
                            "triggerLinkage": false
                        },
                        "refresh": {
                            "open": false,
                            "type": 1,
                            "time": [
                                0
                            ],
                            "interval": "10"
                        }
                    },
                    "generalConfig": {
                        "base": {
                            "backgroundColor": "#00000000",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundSize": 1,
                            "borderRadius": "0px",
                            "customRadius": {
                                "enable": {
                                    "topLeft": 0,
                                    "topRight": 0,
                                    "bottomRight": 0,
                                    "bottomLeft": 0
                                },
                                "useVal": true
                            },
                            "boxShadow": false,
                            "boxShadowColor": "#fff",
                            "boxShadowLevel": "0px",
                            "boxShadowVertical": "0px",
                            "boxShadowLength": "2px",
                            "boxShadowBlur": "5px",
                            "borderWidth": "0px",
                            "borderColor": "#eee",
                            "padding": "0px"
                        },
                        "title": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundStyle": 1,
                            "content": "分组簇状柱图",
                            "hyperlink": "",
                            "titleTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "textDecoration": "none",
                                "color": "#000000"
                            },
                            "textAlign": "center"
                        },
                        "select": {
                            "show": true,
                            "height": "30px",
                            "backgroundColor": "rgba(0, 0, 0, 0)",
                            "background": {
                                "show": false,
                                "value": ""
                            },
                            "backgroundImage": "",
                            "backgroundStyle": 1,
                            "borderWidth": "1px",
                            "borderColor": "#eee",
                            "selectorTextStyle": {
                                "fontFamily": "sans-serif",
                                "fontSize": 12,
                                "fontWeight": "normal",
                                "fontStyle": "normal",
                                "color": "rgba(96, 98, 102, 1)"
                            },
                            "method": 1,
                            "proportion": "",
                            "absolute": "",
                            "spacing": "0px"
                        }
                    }
                }
            }
        ],
        "layout": {
            "report": {
                "width": "1920px",
                "height": "1080px",
                "padding": "0px",
                "single_height": "1080px",
                "backgroundColor": "#fff",
                "backgroundImage": "",
                "backgroundSize": 1
            },
            "d6aa3b9d-ff08-4b21-85b8-842b0da308b1": {
                "width": "960px",
                "height": "540px",
                "zIndex": "99",
                "transform": "translate(400px, 292px)",
                "display": ""
            }
        },
        "controlLayer": [
            {
                "id": "d6aa3b9d-ff08-4b21-85b8-842b0da308b1",
                "type": "GroupBar",
                "name": "分组簇状柱图",
                "img_url": "bar.png",
                "zIndex": 99,
                "c_type": 1,
                "cType": "",
                "chartShow": true,
                "chartLock": false,
                "random": "9b6daa"
            }
        ],
        "toolbarConfig": {
            "toolbar": {
                "color": "#f2f4f7",
                "refresh__open": false,
                "refresh__name": "刷新",
                "refresh__icon": {
                    "open": true,
                    "icon": "<svg width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"#53597B\" xmlns=\"http://www.w3.org/2000/svg\">\n              <g clip-path=\"url(#clip0_33_3341)\">\n              <path d=\"M12.6377 10.813H12.6388V10.8066L12.6377 10.813Z\"></path>\n              <path d=\"M13.4106 6.43335C13.2467 6.26574 12.9583 6.26436 12.792 6.42763L12.505 6.7098C12.353 3.48385 9.73188 0.972412 6.49075 0.972412C3.16721 0.972412 0.463379 3.67624 0.463379 6.9998C0.463379 10.3236 3.16721 13.0274 6.49075 13.0274C8.26192 13.0274 9.93808 12.2435 11.0833 10.8843C11.1343 10.8453 11.1756 10.7984 11.2069 10.7456C11.211 10.7405 11.2156 10.7355 11.2202 10.7309L11.2496 10.684L11.2399 10.679C11.2597 10.6271 11.2689 10.5771 11.2689 10.5256C11.2689 10.2744 11.065 10.0701 10.8138 10.0701C10.6627 10.0701 10.5213 10.1504 10.4391 10.2809C9.45771 11.4597 8.01858 12.1361 6.49076 12.1361C3.65881 12.1361 1.35402 9.83221 1.35402 6.9998C1.35402 4.16805 3.65881 1.8635 6.49076 1.8635C9.30436 1.8635 11.5967 4.13708 11.6266 6.94355L11.1155 6.43084C10.9506 6.26484 10.6618 6.26438 10.4969 6.42948C10.3252 6.60031 10.3252 6.87812 10.496 7.04917L11.5751 8.13084C11.6574 8.21397 11.7676 8.25942 11.8842 8.25942L11.8892 8.25898L11.9007 8.26265C11.9255 8.2686 11.9526 8.27596 11.9825 8.27596C12.0977 8.27596 12.2066 8.23143 12.2892 8.1506L13.4056 7.05216C13.4891 6.97019 13.536 6.85973 13.5369 6.74266C13.5378 6.626 13.4928 6.51646 13.4106 6.43335Z\"></path>\n              </g>\n              <defs>\n              <clipPath id=\"clip0_33_3341\">\n              <rect width=\"14\" height=\"14\" fill=\"white\"></rect>\n              </clipPath>\n              </defs>\n              </svg>",
                    "color": "#409eff",
                    "type": 1
                },
                "full__open": false,
                "full__name": "全屏",
                "full__icon": {
                    "open": true,
                    "icon": "<svg width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" stroke=\"#53597B\" xmlns=\"http://www.w3.org/2000/svg\">\n              <g clip-path=\"url(#clip0_33_3344)\">\n              <mask id=\"mask0_33_3344\" style=\"mask-type:luminance\" maskUnits=\"userSpaceOnUse\" x=\"0\" y=\"0\" width=\"14\" height=\"14\">\n              <path d=\"M14 0H0V14H14V0Z\" fill=\"white\"></path>\n              </mask>\n              <g mask=\"url(#mask0_33_3344)\">\n              <path d=\"M6.70834 1.74988H2.33333C2.01117 1.74988 1.75 2.01104 1.75 2.33321V11.6667C1.75 11.9888 2.01117 12.25 2.33333 12.25H11.6667C11.9888 12.25 12.25 11.9888 12.25 11.6666V7.29163\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M7 4.66663V6.99996\" stroke-width=\"0.8\" stroke-linecap=\"round\"></path>\n              <path d=\"M12.25 1.74988V4.08321\" stroke-width=\"0.8\" stroke-linecap=\"round\"></path>\n              <path d=\"M9.33333 7H7\" stroke-width=\"0.8\" stroke-linecap=\"round\"></path>\n              <path d=\"M12.25 1.74988L7 6.99987\" stroke-width=\"0.8\"></path>\n              <path d=\"M12.2498 1.74988H9.9165\" stroke-width=\"0.8\" stroke-linecap=\"round\"></path>\n              </g>\n              </g>\n              <defs>\n              <clipPath id=\"clip0_33_3344\">\n              <rect width=\"14\" height=\"14\" fill=\"white\"></rect>\n              </clipPath>\n              </defs>\n              </svg>",
                    "color": "#409eff",
                    "type": 1
                },
                "print__open": false,
                "print__name": "打印",
                "print__icon": {
                    "open": true,
                    "icon": "<svg width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" stroke=\"#53597B\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n              <path d=\"M11.6667 9.625V12.25C11.6667 12.5722 11.4055 12.8333 11.0833 12.8333H9.1875\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M11.6668 4.66663V1.74996C11.6668 1.42779 11.4057 1.16663 11.0835 1.16663H2.91683C2.59466 1.16663 2.3335 1.42779 2.3335 1.74996V12.25C2.3335 12.5721 2.59466 12.8333 2.91683 12.8333H4.66683\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M4.6665 4.66663H8.74984\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M6.7085 12.8334L11.6668 6.70837\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M4.6665 7H6.99984\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              </svg>",
                    "color": "#409eff",
                    "type": 1
                },
                "download__open": false,
                "download__name": "导出",
                "download__icon": {
                    "open": true,
                    "icon": "<svg width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" stroke=\"#53597B\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n              <path d=\"M1.4585 2.33333C1.4585 2.01117 1.71966 1.75 2.04183 1.75H5.54183L7.00016 3.5H11.9585C12.2807 3.5 12.5418 3.76116 12.5418 4.08333V11.6667C12.5418 11.9888 12.2807 12.25 11.9585 12.25H2.04183C1.71966 12.25 1.4585 11.9888 1.4585 11.6667V2.33333Z\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M8.75 8.16663L6.99805 9.91663L5.25 8.17053\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              <path d=\"M7 5.83337V9.91671\" stroke-width=\"0.8\" stroke-linecap=\"round\" stroke-linejoin=\"round\"></path>\n              </svg>",
                    "color": "#409eff",
                    "type": 1
                },
                "custom": true,
                "customButton": []
            }
        },
        "paramsConfig": {
            "panelStyles": {
                "isOpen": false,
                "defaultOpen": false,
                "buttonName": "查询",
                "color": "#fff",
                "bgColor": "#4E89FB"
            },
            "paramsManagement": {
                "paramsConfig": []
            }
        }
    },
    "data": {
        "dataConfig": [
            {
                "name": "商品销售",
                "id": "商品销售id",
                "type": 4,
                "response_data": [],
                "fields": [
                    {
                        "name": "商品名称",
                        "data_type": 3
                    },
                    {
                        "name": "区域名称",
                        "data_type": 3
                    },
                    {
                        "name": "销售额",
                        "data_type": 2
                    }
                ],
                "params": [
                    {
                        "name": "商品名称",
                        "data_type": 3
                    },
                    {
                        "name": "区域名称",
                        "data_type": 3
                    }
                ]
            }
        ]
    }
}
export const get_Report=()=>{
    return report_lsyjkb;
}