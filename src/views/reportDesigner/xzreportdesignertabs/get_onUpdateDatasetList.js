let onUpdateDatasetlist = [
    {
        name: "商品销售",
        id: "商品销售id",
        columns:
            [
                {
                    name: "商品名称",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "区域名称",
                    data_type: 3
                },
                {
                    name: "销售额",
                    data_type: 2
                },
            ],

        params:
            [
                {
                    name: "商品名称",                           //这些name：可以是中文？
                    data_type: 3
                },
                {
                    name: "区域名称",
                    data_type: 3
                },

            ],

    },
    {
        name: "商品季度销售",
        id: "商品季度销售id",
        columns: [
            {
                name: "商品",
                data_type: 3
            }, {
                name: "季度",
                data_type: 2

            }, {
                name: "销售额",
                data_type: 2
            }
        ],
        params:
            [
                {
                    name: "商品",
                    data_type: 3
                },
            ],
    }
]
import {deepCopy} from "@/utils/xzToolsUtil.js";
export const get_onUpdateDatasetlist= ()=>{

    return deepCopy(onUpdateDatasetlist);//深拷贝
}

