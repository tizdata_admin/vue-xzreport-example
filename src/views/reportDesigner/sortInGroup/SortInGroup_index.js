const dataset = [ //  添加数据集按钮
    { //Json数据
        id: 1,
        value: "新建Json数据集"
    },
    { //http请求数据
        id: 2,
        value: "新建Http接口数据集"
    },
    { //API服务数据
        id: 3,
        value: "API服务数据集"
    },
    {
        id: 4,
        value: "外部数据集"
    }
];
const options = {
    row: 100,
    col: 25,
    width:()=>1600,
    // width:()=>width
    // height:()=>height
    renderArrow: true,  // 是否显式 扩展方向图标
    showFreeze: true,  // 是否显式冻结线
    showGrid: true,   // 是否显式网格线
    showPreviewToolbar:true,
    showBottombar:true,
    showSelect:true,
    dataset
}
let results= [];
const onUpdateDatasetList = () => {
    return new Promise((resolve, reject) => {

        // resolve的数据格式，这里可以写副作用的‘远程获取数据’
        console.log(results);
        resolve(results)  //这里暂时引用前面的    results
    })
}


options.onUpdateDatasetList = onUpdateDatasetList

import {get_onUpdateDatasetlist } from "./sortInGroup_get_onUpdateDatasetList.js";
import {get_onupdatedatamaps} from "./sortInGroup_get_onUpdateDataMaps_.js";
