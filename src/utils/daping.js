const getScale=(w=1920,h=1080)=>{
    const scale = window.devicePixelRatio
    const ww = window.innerWidth /w
    const wh = window.innerHeight /h

    // w,h一般是不会变化的。宽度方向的
    return  ww < wh ? ww : wh;
}

// selectorClassName='.box'
const getScaleForSelector=(selectClassName)=>{
    let box = document.querySelector(selectClassName)
    box.style.transform = `scale(${getScale()}) translate(-50%,-50%)`
}
