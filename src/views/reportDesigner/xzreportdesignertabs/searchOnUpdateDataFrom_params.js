import {getParamsFrom_DataSet} from "@/utils/xzToolsUtil.js";
export const searchOnUpdateDatasFrom_params = (get_onupdatedatamaps, datasets, params) => {
    //作为测试样例，为了简化，这里只是模拟一个手动参数场景
    let onupdatedatamaps = get_onupdatedatamaps();
    let __rtn = [];
    datasets.forEach((dataset) => {
        if (onupdatedatamaps.get(dataset.name)) {//后台有这个表结构的数据记录
            if (params.length > 0) {//如果有查询参数，从全部表名数据记录中过滤
                let tmp_ls = [];//满足条件的记录返回，测试，目前仅仅用于 参数=单值
                onupdatedatamaps.get(dataset.name).response_data.forEach((tmp_item) => {//每一个数据集，循环过滤
                    for (let param of params) {
                        if (param.value.length == 1)//参数取值一个
                        {
                            if (tmp_item[param.name] == param.value) {//判断参数值和记录字段值是否相等
                                tmp_ls.push(tmp_item);
                            }
                        } else {//当前参数的值是多个，数组
                            for (let __valueIt of param.value) {
                                if (tmp_item[param.name] == __valueIt) {//判断参数值和记录字段值是否相等
                                    tmp_ls.push(tmp_item);   //循环，只要有一个相等，就可以返回
                                    break;//没有必要再循环，否则会重复放入
                                }
                            }
                        }
                    }
                })
                onupdatedatamaps.get(dataset.name).response_data = tmp_ls;
                __rtn.push(onupdatedatamaps.get(dataset.name));
            } else {//如果没有查询参数，该表结构记录全部返回
                __rtn.push(onupdatedatamaps.get(dataset.name));
            }
        }
    });
    return __rtn;
}

export const searchOnUpdateDatasFrom_params2 = (get_onupdatedatamaps, datasets, params) => {
    //作为测试样例，为了简化，这里只是模拟一个手动参数场景

    let onupdatedatamaps = get_onupdatedatamaps();
    let __rtn = [];
    datasets.forEach((dataset) => {
        if (onupdatedatamaps.get(dataset.name)) {//后台有这个表结构的数据记录
            if (params.length > 0) {//如果有查询参数，从全部表名数据记录中过滤
                let tmp_ls = [];//满足条件的记录返回，测试，目前仅仅用于 参数=单值
                onupdatedatamaps.get(dataset.name).response_data.forEach((tmp_item) => {//每一个数据集，循环过滤
                    for (let param of params) {
                        if (param.value.length == 1)//参数取值一个
                        {
                            if (tmp_item[param.name] == param.value) {//判断参数值和记录字段值是否相等
                                tmp_ls.push(tmp_item);
                            }
                        } else {//当前参数的值是多个，数组
                            for (let __valueIt of param.value) {
                                if (tmp_item[param.name] == __valueIt) {//判断参数值和记录字段值是否相等
                                    tmp_ls.push(tmp_item);   //循环，只要有一个相等，就可以返回
                                    break;//没有必要再循环，否则会重复放入
                                }
                            }
                        }
                    }
                })
                onupdatedatamaps.get(dataset.name).response_data = tmp_ls;
                __rtn.push(onupdatedatamaps.get(dataset.name));
            } else {//如果没有查询参数，该表结构记录全部返回
                __rtn.push(onupdatedatamaps.get(dataset.name));
            }
        }
    });
    return __rtn;
}

